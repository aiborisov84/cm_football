from PySide6.QtGui import QAction, QKeySequence, QFont
from PySide6.QtWidgets import QApplication, QMainWindow, QMessageBox, QTextEdit
    
import test


# ==============================
# noinspection PyArgumentList,PyAttributeOutsideInit
class MainWindow(QMainWindow):
    def __init__(self, i_app):
        super().__init__()

        self._app = i_app
        self._Test = test.CTest(test.G_Cases[0])
        self.setMinimumWidth(500)
        self.setMinimumHeight(500)

        self.textEdit = QTextEdit()
        self.textEdit.setReadOnly(True)
        v_font = self.textEdit.currentFont()
        v_font.setPointSize(7)
        self.textEdit.setFont(v_font)
        self.setCentralWidget(self.textEdit)

        self.createActions()
        self.createToolBars()
        self.createStatusBar()

    def closeEvent(self, event):
        event.accept()

    def createActions(self):
        self.newAct = QAction(
            "&Clear",
            self,
            shortcut=QKeySequence.New,
            statusTip="Clear log",
            triggered=self.newFile
        )

        self.stepAct = QAction(
            "&Step",
            self,
            shortcut="F7",
            statusTip="Step action",
            triggered=self.DoStep
        )

        self.exitAct = QAction(
            "E&xit",
            self,
            shortcut="Ctrl+Q",
            statusTip="Exit the application",
            triggered=self.close
        )

        self.aboutAct = QAction(
            "&About",
            self,
            statusTip="Show the application's About box",
            triggered=self.about
        )

        self.aboutQtAct = QAction(
            "About &Qt",
            self,
            statusTip="Show the Qt library's About box",
            triggered=self._app.instance().aboutQt
        )

    def createToolBars(self):
        self.fileToolBar = self.addToolBar("Common")
        self.fileToolBar.addAction(self.newAct)
        self.fileToolBar.addAction(self.stepAct)
        self.fileToolBar.addAction(self.aboutAct)
        self.fileToolBar.addAction(self.aboutQtAct)
        self.fileToolBar.addAction(self.exitAct)

    def createStatusBar(self):
        self.statusBar().showMessage("Ready")

    def newFile(self):
        self.textEdit.clear()

    def DoStep(self):
        v_state = self._Test.do()
        v_add = False
        if v_add:
            v_cursor = self.textEdit.textCursor()
            v_cursor.movePosition(11)  # End
            for it in v_state:
                v_cursor.insertText(it+'\r\n')
        else:
            self.textEdit.setPlainText('\r\n'.join(v_state))

    def about(self):
        QMessageBox().about(
            self,
            "About Application",
            "The <b>Application</b> example demonstrates how to write "
            "modern GUI applications using Qt, with a menu bar, "
            "toolbars, and a status bar."
        )


# ==============================
if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    app.setFont(QFont('Courier New', 8))
    mainWin = MainWindow(app)
    mainWin.show()
    sys.exit(app.exec_())
