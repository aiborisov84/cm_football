from PySide6.QtGui import QFont, QGuiApplication
from PySide6.QtQml import QQmlApplicationEngine
from PySide6.QtQuick import QQuickWindow
from PySide6.QtCore import QObject, Slot

import test
import model.emb
import typing


# ==============================
class PyConnect(QObject):
    _Test: typing.Optional[test.CTest]

    def __init__(self, i_env_selector: str = None):
        QObject.__init__(self)

        self._Root = None
        self._Test = None

        for it_idx, it_path in enumerate(test.G_Cases):
            if i_env_selector:
                if 0 <= it_path.find(i_env_selector):
                    self._ModelIdx = it_idx
                    break
        else:
            self._ModelIdx = len(test.G_Cases) - 1

        self._restart()

    def free_test(self):
        self._Test = None

        if self._Root:
            self._Root.setMainEnv(None)
            self._Root.setMainLayer(None)
            self._Root.setComponentMas([])
            self._Root = None

    def _refresh_env_component(self):
        if not self._Root:
            return

        if self._Test:
            v_component_pack = self._Test.get_components()
            if v_component_pack:
                v_main_env, v_main_layer, v_components = v_component_pack

                self._Root.setMainEnv(v_main_env)
                self._Root.setMainLayer(v_main_layer)
                self._Root.setComponentMas(v_components)

    def _restart(self):
        c_file = test.G_Cases[self._ModelIdx]
        try:
            self._Test = None
            vl_new_test = test.CTest(c_file)
            self._Test = vl_new_test
            self._refresh_env_component()
        except Exception as e:
            msg = f'Exception "{e}" in load file {c_file}'
            if self._Root:
                self._Root.addApplException(msg)
            else:
                print(msg)

    @Slot(QQuickWindow)
    def setRoot(self, i_root):
        self._Root = i_root
        if not i_root:
            self._Test = None
        self._refresh_env_component()

    @Slot(int, result=str)
    def step(self, i_step_count):
        if not self._Test:
            return

        v_state = []

        def do_on_exception(i_exception):
            if self._Root:
                self._Root.addApplException(str(i_exception))

        for _ in range(i_step_count):
            v_state = self._Test.do(do_on_exception)

        return '\r\n'.join(v_state)

    @Slot(result=int)
    def modelCount(self):
        return len(test.G_Cases)

    @Slot(int, result=str)
    def modelStr(self, i_idx):
        return os.path.relpath(test.G_Cases[i_idx])

    @Slot(result=int)
    def modelIdx(self):
        return self._ModelIdx

    @Slot(int)
    def restart(self, i_idx):
        if i_idx >= 0:
            self._ModelIdx = i_idx
        self._restart()

    @Slot(result=QFont)
    def getMainFont(self):
        return model.emb.get_appl_font()

    @Slot(result=QFont)
    def getLogFont(self):
        return model.emb.get_log_font()

    @Slot(result=bool)
    def isAndroid(self):
        return model.emb.GIsDroid


# ==============================
if __name__ == "__main__":
    import sys
    import os

    os.environ['QT_QUICK_CONTROLS_STYLE'] = ["Fusion", "Material", "Universal", "Imagine"][-1 if model.emb.GIsDroid else 1]
    os.environ['QT_QUICK_CONTROLS_MATERIAL_THEME'] = ["Dark", "Light"][1]
    os.environ['QT_QUICK_CONTROLS_MATERIAL_VARIANT'] = ["Normal", "Dense"][1]  #
    os.environ['QT_QUICK_CONTROLS_MATERIAL_ACCENT'] = ["Pink", "Indigo"][0]
    os.environ['QT_QUICK_CONTROLS_MATERIAL_PRIMARY'] = ["Indigo", "Grey"][0]
    # os.environ['QT_FORCE_STDERR_LOGGING'] = '1'

    app = QGuiApplication(sys.argv)
    app.setOrganizationName("A.Borisov")
    app.setOrganizationDomain('https://gitlab.com/aiborisov84/ai_borisov_CIT')
    py_connect = PyConnect(sys.argv[1] if 1 < len(sys.argv) else None)
    app.setFont(py_connect.getMainFont())
    engine = QQmlApplicationEngine()

    engine.rootContext().setContextProperty("pyconnect", py_connect)
    engine.load(os.path.join(os.path.dirname(__file__), "Appl1.qml"))

    # engine.quit.connect(app.quit)
    def free():
        py_connect.free_test()
        engine.deleteLater()

    app.aboutToQuit.connect(free)

    res = app.exec()

    print(res)
    sys.exit(res)
