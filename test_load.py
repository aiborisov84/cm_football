import contextlib
import glob
import os
import unittest
import os.path as op
import sys
# import PyQt5.QtQuick # for android so load

# ==============================
if __name__ == "__main__":
    start_dir = op.dirname(__file__)

    if start_dir not in sys.path:
        sys.path.insert(0, start_dir)

    v_filter = input("Input filter (sample: Env4*). Empty for all tests: ")

    if v_filter:
        with contextlib.ExitStack() as stack:
            v_dir_fd = os.open(start_dir, os.O_DIRECTORY)
            stack.callback(lambda: os.close(v_dir_fd))
            v_test_mas = glob.glob('!data/' + v_filter, dir_fd=v_dir_fd)
            v_test_mas.sort()
            print("Do first from: ", v_test_mas)

        if v_test_mas:
            it = v_test_mas[0]
            loader = unittest.TestLoader()
            v_current_test = op.join(start_dir, it)
            print(v_current_test)
            suite = loader.discover(
                v_current_test,
                pattern="test*.py"
            )

            runner = unittest.TextTestRunner()
            runner.run(suite)
    else:
        loader = unittest.TestLoader()
        suite = loader.discover(
            start_dir + '/model',
            pattern="*.py"
        )

        runner = unittest.TextTestRunner()
        runner.run(suite)

        loader = unittest.TestLoader()
        suite = loader.discover(
            start_dir + '/!data',
            pattern="test*.py"
        )

        runner = unittest.TextTestRunner()
        runner.run(suite)
