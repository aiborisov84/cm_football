# Общая теория алгоритмов. Моделирование процесса анализа визуального поля и управления вниманием
## Лицензия
```
 GNU AFFERO GENERAL PUBLIC LICENSE
 Version 3, 19 November 2007
```

Полный текст лицензии ([LICENSE](https://gitlab.com/aiborisov84/ai_borisov_CIT/-/blob/master/LICENSE.txt))

## Контактная информация
Авторы: 

- Борисов Алексей Игоревич (aiborisov84@gmail.com).
([Доска публикаций Хабр](https://habr.com/ru/users/ai_borisov/))

## Вики

- [Описание](https://gitlab.com/aiborisov84/ai_borisov_CIT/-/wikis/home)
