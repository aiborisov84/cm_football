import os.path as op
import traceback
import unittest

import model.layercore.state
import model.layercore.cell
import model.layercore.analyse
import model.net

import logging
import logging.handlers
import queue

import glob
import os

G_Cases = glob.glob(os.path.join(os.path.dirname(__file__), '!data/Env*/*.work_net'))
G_Cases.sort()
print("\n".join(G_Cases))


# ==============================
class CTest:
    def __init__(self, i_env_file):
        self.File = op.abspath(i_env_file)
        self.Net = model.net.CNet.create_from_file(self.File)
        self.q = queue.Queue(-1)  # 100 no limit on size (-1)
        self._qh = logging.handlers.QueueHandler(self.q)
        v_root = logging.getLogger()
        v_root.setLevel(logging.DEBUG)
        v_root.addHandler(self._qh)

    def __del__(self):
        v_root = logging.getLogger()
        try:
            v_root.removeHandler(self._qh)
        except AttributeError:
            pass

    def do(self, i_action_on_exception=None):
        v_result = []
        try:
            self.Net.do_tick()
        except Exception as e:
            logging.exception('---------------------- exception')
            # logging.error('---------------------- ', exc_info=e)
            logging.debug('----------------------')
            if i_action_on_exception:
                i_action_on_exception(''.join(traceback.format_exception(e)))
            # raise e
        
        while not self.q.empty():
            record = self.q.get()
            if record is None:
                break
            v_result.append(record.getMessage())
        
        return v_result

    def get_components(self):
        v_component_pack = None

        try:
            v_component_pack = self.Net.get_components()
        except Exception as e:
            logging.debug('---------------------- exception')
            logging.error('---------------------- ', exc_info=e)

        return v_component_pack
        

# ==============================
class TestNet(unittest.TestCase):
    def test(self):
        logging.basicConfig(level=logging.DEBUG, format='%(message)s')

        v_prefix = ''.join(['=']*50)

        def header(i_caption):
            print(v_prefix+' Test '+i_caption)

        header('Begin')

        v_test = CTest(G_Cases[1])
        for _ in range(120):
            v_state = v_test.do()

        header('End')
