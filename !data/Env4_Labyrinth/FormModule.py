import typing

from PySide6.QtGui import QFont
from PySide6.QtQml import QQmlEngine, QQmlComponent
from PySide6.QtCore import QObject, QCoreApplication, Slot, Property, Signal, QUrl  # ,QTimer

import os.path as op

import model.emb as emb

from . import Labyrinth
from . import Environment


# ==============================
class PyBoolItem(QObject):
    onChanged = Signal()

    def __init__(self, i_is_on, parent):
        QObject.__init__(self, parent)
        self._isOn = i_is_on

    @Property(bool, notify=onChanged)
    def isOn(self):
        return self._isOn

    def update(self, i_is_on):
        if self._isOn == i_is_on:
            return False
        self._isOn = i_is_on
        # noinspection PyUnresolvedReferences
        self.onChanged.emit()
        return True


# ==============================
class PyFormData(QObject):
    onCaptionChanged = Signal()
    onRowsChanged = Signal()
    onColsChanged = Signal()
    onRobotChanged = Signal()
    onWorkChanged = Signal()
    onStatusChanged = Signal()
    onSizeChanged = Signal()

    # workList: ListProperty
    # wallRowList: ListProperty
    # wallColList: ListProperty
    _work: typing.List[PyBoolItem]
    _wall_row: typing.List[PyBoolItem]
    _wall_col: typing.List[PyBoolItem]
    _component: typing.Optional[QQmlComponent] = None
    _lab: Labyrinth.State
    _env_state: Environment.State
    _lab_idx: int

    def __init__(self, i_env_state: Environment.State):
        QObject.__init__(self)

        if QCoreApplication.instance():
            self._QmlEngine = QQmlEngine()
            self._QmlEngine.rootContext().setContextProperty(
                "formData", self)

            v_dir = op.dirname(__file__)
            self._QmlEngine.setBaseUrl(QUrl("file:///" + v_dir + "/"))

            self._component = QQmlComponent(self._QmlEngine)
            self._component.loadUrl(QUrl("FormModule.qml"))
            if self._component.status() != QQmlComponent.Status.Ready:
                self._component = None

        self._status_text = ""
        self._work = []
        self._wall_row = []
        self._wall_col = []
        # self.workList = ListProperty(PyBoolItem, self, self._work)
        # self.wallRowList = ListProperty(PyBoolItem, self, self._wall_row)
        # self.wallColList = ListProperty(PyBoolItem, self, self._wall_col)

        self._env_state = i_env_state
        self._select_lab(0)
        self._caption = 'Labirinth'

        # self._timer = QTimer()
        # self._timer.timeout.connect(self._update_robot)
        # self._timer.start(1000)   # ms

    def __del__(self):
        self._component = None
        self._QmlEngine = None

    def get_component(self):
        return self._component

    def get_lab_idx(self):
        return self._lab_idx

    def _select_lab(self, idx):
        self._lab_idx = idx
        self._lab = self._env_state.LabMas[self._lab_idx]

        # print(self._lab)

        self._col_count = self._lab.Size.Col
        self._row_count = self._lab.Size.Row

        v_full_size = self._row_count * self._col_count

        # self._wall_row = [PyBoolItem(i % 2 == 0) for i in range(v_full_size)]
        # self._wall_col = [PyBoolItem(i % 3 == 0) for i in range(v_full_size)]

        if self._work:
            self._work.clear()
        self._work.extend([PyBoolItem(it, self) for it in self._lab.CurrentDynamic.WorkMas])

        v_wall_row = [False for _ in range(v_full_size)]
        v_wall_col = [False for _ in range(v_full_size)]

        def con_col_to_idx(col, row):
            return (col - 1) + row * self._col_count

        for itCol, itRowRangeMas in self._lab.ColMap.items():
            for itRowRange in itRowRangeMas:
                for itRow in range(itRowRange.Min, itRowRange.Max):
                    idx = con_col_to_idx(itCol, itRow)
                    v_wall_col[idx] = True

        def con_raw_to_idx(col, row):
            return col + (row - 1) * self._col_count

        for itRow, itColRangeMas in self._lab.RowMap.items():
            for itColRange in itColRangeMas:
                for itCol in range(itColRange.Min, itColRange.Max):
                    idx = con_raw_to_idx(itCol, itRow)
                    v_wall_row[idx] = True

        if self._wall_row:
            self._wall_row.clear()
        self._wall_row.extend([PyBoolItem(i, self) for i in v_wall_row])

        if self._wall_col:
            self._wall_col.clear()
        self._wall_col.extend([PyBoolItem(i, self) for i in v_wall_col])

        self._energy_idx = self._lab.coord_to_idx(self._lab.EnergyPos)

        # noinspection PyUnresolvedReferences
        self.onRobotChanged.emit()
        # noinspection PyUnresolvedReferences
        self.onWorkChanged.emit()
        # noinspection PyUnresolvedReferences
        self.onRowsChanged.emit()
        # noinspection PyUnresolvedReferences
        self.onColsChanged.emit()
        # noinspection PyUnresolvedReferences
        self.onSizeChanged.emit()

    def _update_robot(self):
        if self._lab.CurrentDynamic.RobotPos.Col < 4:
            self._lab.CurrentDynamic.RobotPos.Col = self._lab.CurrentDynamic.RobotPos.Col + 1
        else:
            self._lab.CurrentDynamic.RobotPos.Col = self._lab.CurrentDynamic.RobotPos.Col - 1

        # noinspection PyUnresolvedReferences
        self.onRobotChanged.emit()

    def update_work(self):
        for it, value in enumerate(self._lab.CurrentDynamic.WorkMas):
            self._work[it].update(value)

    def set_status(self, i_text: str):
        self._status_text = i_text
        # noinspection PyUnresolvedReferences
        self.onStatusChanged.emit()

    @Property(str, notify=onCaptionChanged)
    def textCaption(self):
        return self._caption

    @Slot(result=QFont)
    def getLogFont(self):
        return emb.get_log_font()

    @Property(int, notify=onSizeChanged)
    def colCount(self):
        return self._col_count

    @Property(int, notify=onSizeChanged)
    def rowCount(self):
        return self._row_count

    @Property(str, notify=onStatusChanged)
    def statusText(self):
        return self._status_text

    @Property(list, notify=onRowsChanged)
    def wallRow(self):
        return self._wall_row

    @Property(list, notify=onColsChanged)
    def wallCol(self):
        return self._wall_col

    @Property(list, notify=onWorkChanged)
    def work(self):
        return self._work

    @Property(int, notify=onRobotChanged)
    def robotColIdx(self):
        return self._lab.CurrentDynamic.RobotPos.Col

    @Property(int, notify=onRobotChanged)
    def robotRowIdx(self):
        return self._lab.CurrentDynamic.RobotPos.Row

    @Slot(result=int)
    def getEnergyIdx(self):
        return self._energy_idx

    @Slot(result=int)
    def getLabMasCount(self):
        return len(self._env_state.LabMas)

    @Slot(int, result=str)
    def getLabMasFile(self, idx):
        return op.relpath(self._env_state.LabMas[idx].FileName, op.dirname(__file__))

    @Slot(int)
    def setLabMasIdx(self, idx):
        self._select_lab(idx)

    @Slot()
    def restart(self):
        self._env_state.restart()
        self.update_work()
        # noinspection PyUnresolvedReferences
        self.onRobotChanged.emit()

    @Slot(int)
    def moveRobot(self, i_pos):
        self._lab.robot_set_pos(i_pos)
        # noinspection PyUnresolvedReferences
        self.onRobotChanged.emit()
