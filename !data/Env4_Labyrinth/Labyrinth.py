import typing
from dataclasses import dataclass
import copy


# ==============================
@dataclass
class Coord:
    Col: int
    Row: int

    def set(self, i_col, i_row):
        self.Col = i_col
        self.Row = i_row


@dataclass
class Range:
    Min: int
    Max: int

    def in_range(self, i_value):
        return self.Min <= i_value < self.Max


# ==============================
@dataclass
class CStateResult:
    HearWork: int
    HearEnergy: bool


@dataclass
class CDynamicState:
    RobotPos: Coord
    WorkMas: typing.List[bool]


class State:
    Size: Coord
    EnergyPos: Coord
    CurrentDynamic: CDynamicState
    ColMap: typing.MutableMapping[int, typing.List[Range]]
    RowMap: typing.MutableMapping[int, typing.List[Range]]
    FileName: str

    def __init__(self, i_file_name):
        self.FileName = i_file_name

        def read_coord(i_str):
            v_str_coord = i_str.split(" ")
            return Coord(*list(map(int, v_str_coord)))

        def read_line(i_str):
            str_col, str_row = i_str.split(",")
            return [
                list(map(int, str_col.strip().split(" "))),
                list(map(int, str_row.strip().split(" ")))
            ]

        with open(i_file_name, "r") as f:
            self.Size = read_coord(f.readline().strip())
            self.EnergyPos = read_coord(f.readline().strip())
            v_robot_pos = read_coord(f.readline().strip())
            self.ColMap = {}
            self.RowMap = {}
            for file_line in f:
                line_col, line_row = read_line(file_line.strip())
                if len(line_col) == 1:
                    self.ColMap.setdefault(line_col[0], []).append(Range(*line_row))
                elif len(line_row) == 1:
                    self.RowMap.setdefault(line_row[0], []).append(Range(*line_col))

        v_work_mas = [i % 3 == 0 for i in range(self.Size.Row * self.Size.Col)]

        self.CurrentDynamic = CDynamicState(v_robot_pos, v_work_mas)
        self._FirstState = copy.deepcopy(self.CurrentDynamic)

    def coord_to_idx(self, i_coord):
        return i_coord.Col + i_coord.Row * self.Size.Col

    def is_left_wall(self, i_coord: Coord):
        if i_coord.Col == 0:
            return True

        if i_coord.Col in self.ColMap:
            for it in self.ColMap[i_coord.Col]:
                if it.in_range(i_coord.Row):
                    return True

        return False

    def is_right_wall(self, i_coord: Coord):
        if i_coord.Col+1 == self.Size.Col:
            return True

        if i_coord.Col+1 in self.ColMap:
            for it in self.ColMap[i_coord.Col+1]:
                if it.in_range(i_coord.Row):
                    return True

        return False

    def is_top_wall(self, i_coord: Coord):
        if i_coord.Row == 0:
            return True

        if i_coord.Row in self.RowMap:
            for it in self.RowMap[i_coord.Row]:
                if it.in_range(i_coord.Col):
                    return True

        return False

    def is_bottom_wall(self, i_coord: Coord):
        if i_coord.Row+1 == self.Size.Row:
            return True

        if i_coord.Row+1 in self.RowMap:
            for it in self.RowMap[i_coord.Row+1]:
                if it.in_range(i_coord.Col):
                    return True

        return False

    def get_robot_state(self):
        v_robot_pos = self.CurrentDynamic.RobotPos
        return [
            self.is_top_wall(v_robot_pos),
            self.is_right_wall(v_robot_pos),
            self.is_bottom_wall(v_robot_pos),
            self.is_left_wall(v_robot_pos)
        ]

    def step_top(self):
        if self.CurrentDynamic.RobotPos.Row == 0:
            return False

        self.CurrentDynamic.RobotPos.Row -= 1
        return True

    def step_right(self):
        if self.CurrentDynamic.RobotPos.Col == self.Size.Col-1:
            return False

        self.CurrentDynamic.RobotPos.Col += 1
        return True

    def step_bottom(self):
        if self.CurrentDynamic.RobotPos.Row == self.Size.Row-1:
            return False

        self.CurrentDynamic.RobotPos.Row += 1
        return True

    def step_left(self):
        if self.CurrentDynamic.RobotPos.Col == 0:
            return False

        self.CurrentDynamic.RobotPos.Col -= 1
        return True

    def robot_do_step(self, i_step_direction):
        v_wall_func: typing.List[typing.Callable[[Coord], bool]] = [
            self.is_top_wall, self.is_right_wall, self.is_bottom_wall, self.is_left_wall
        ]
        v_step_func = [self.step_top, self.step_right, self.step_bottom, self.step_left]
        for st, wall, step in zip(i_step_direction, v_wall_func, v_step_func):
            if st == 0:
                continue

            if wall(self.CurrentDynamic.RobotPos):
                return False

            if not step():
                return False

        return True

    def robot_do_work(self):
        v_work_pos = self.robot_get_pos()
        if not self.CurrentDynamic.WorkMas[v_work_pos]:
            return False

        self.CurrentDynamic.WorkMas[v_work_pos] = False
        return True

    def robot_is_work(self):
        v_idx = self.robot_get_pos()
        return self.CurrentDynamic.WorkMas[v_idx]

    def robot_set_pos(self, i_idx):
        v_row = i_idx // self.Size.Col
        v_col = i_idx % self.Size.Col

        self.CurrentDynamic.RobotPos.set(v_col, v_row)

    def robot_get_pos(self):
        return self.CurrentDynamic.RobotPos.Row * self.Size.Col + self.CurrentDynamic.RobotPos.Col

    def robot_is_energy(self):
        return self.CurrentDynamic.RobotPos == self.EnergyPos

    def get_size_count(self):
        return self.Size.Col * self.Size.Row

    def get_state(self):
        return CStateResult(self.robot_is_work(), self.robot_is_energy())

    def __str__(self):
        a = list(map(str, [self.Size, self.EnergyPos, self.ColMap, self.RowMap]))
        return "\n".join(a)

    def restart(self):
        self.CurrentDynamic = copy.deepcopy(self._FirstState)


def test():
    l1 = State("1.labyr")
    print(l1)


# ==============================
if __name__ == '__main__':
    test()
