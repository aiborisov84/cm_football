import QtQuick.Controls
import QtQuick
import QtQuick.Shapes
import QtQuick.Controls.Material 


Page {
    anchors.fill: parent

    function min(i1, i2) {
        return i1 < i2 ? i1 : i2
    }

    function max(i1, i2) {
        return i1 < i2 ? i2 : i1
    }

    property bool isVisibleCellIndex: true
    property bool isVisibleVertical: true
    property bool isVisibleHorisontal: true
    property bool isVisibleWork: true

    property int imgMargin: 10

    property int rowCount: formData.rowCount
    property int colCount: formData.colCount
    property int imgScale: 1
    property int cellSize: imgScale * (min(width, height) - imgMargin*2) / max(rowCount, colCount)//75
    property int labWidth: colCount * cellSize
    property int labHeight: rowCount * cellSize
    property int robotLeft: formData.robotColIdx * cellSize
    property int robotTop: formData.robotRowIdx * cellSize

    function isVertical(iIdx) {
        return formData.wallCol[iIdx].isOn
    }

    function isHorisontal(iIdx) {
        return formData.wallRow[iIdx].isOn
    }

    function isWork(iIdx) {
        return formData.work[iIdx].isOn
    }

    function getColor(iIdx) {
        //return (iNum % 3) == 0;
        return formData.getEnergyIdx() == iIdx ? Material.color(Material.Amber) : "transparent"
    }

    Flickable {
        anchors.fill: parent
        leftMargin: imgMargin
        topMargin: imgMargin
        bottomMargin: imgMargin
        rightMargin: imgMargin
        contentWidth: image.width; contentHeight: image.height
        clip: true

        Rectangle {
            id: image
            //anchors.fill: parent
            width: labWidth
            height: labHeight
            color: "transparent"
            border.color : Material.foreground //"black"

            Grid {
                id: labyrGrid
                columns: colCount
                rows: rowCount
                spacing: 0

                Repeater {
                    model: labyrGrid.columns * labyrGrid.rows
                    Rectangle {
                        //id: rectangle
                        //anchors.fill: parent
                        width: cellSize
                        height: cellSize
                        color: getColor(index)

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                formData.moveRobot(index)
                            }
                        }

                        Label {
                            visible: isVisibleCellIndex
                            text: index + 1
                            font: formData.getLogFont()
                        }
                        Shape {
                            anchors.fill: parent
                            //visible: false
                            visible: isVisibleHorisontal && isHorisontal(index)

                            ShapePath {
                                strokeWidth: 1
                                strokeColor: Material.foreground //"black"
                                startX: 1
                                startY: cellSize// - 1
                                PathLine {
                                    x: cellSize
                                    y: cellSize// - 1
                                }
                            }
                        }
                        Shape {
                            anchors.fill: parent
                            visible: isVisibleVertical && isVertical(index)

                            ShapePath {
                                strokeWidth: 1
                                strokeColor: Material.foreground //"black"
                                startX: cellSize
                                startY: 0
                                PathLine {
                                    x: cellSize
                                    y: cellSize// - 1
                                }
                            }
                        }
                        Shape {
                            anchors.fill: parent
                            visible: isVisibleWork && isWork(index)

                            ShapePath {
                                strokeWidth: 2
                                strokeColor: Material.color(Material.Green) //"black"
                                fillColor: "transparent"
                                startX: 1
                                startY: cellSize / 2
                                PathLine {
                                    x: cellSize / 4
                                    y: 3 * cellSize / 4
                                }
                                PathMove {
                                    x: 1
                                    y: 3 * cellSize / 4
                                }
                                PathLine {
                                    x: cellSize / 4
                                    y: cellSize / 2
                                }
                            }
                        }
                    }
                }
            }

            Shape {
                visible: true

                ShapePath {
                    strokeWidth: 1
                    strokeColor: Material.foreground // "black"
                    startX: robotLeft + cellSize/4
                    startY: robotTop + cellSize/2
                    PathArc {
                        x: robotLeft + 3*cellSize/4;
                        y: robotTop + cellSize/2
                        radiusX: cellSize/4; radiusY: cellSize/4
                        useLargeArc: true
                    }
                    PathLine {
                        x: robotLeft + cellSize/4
                        y: robotTop + cellSize/2
                    }
                }
            }
        }
    }
}
