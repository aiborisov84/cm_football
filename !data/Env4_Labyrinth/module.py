import typing
from dataclasses import dataclass
import glob
import os
import os.path as op

import model.emb as emb

from . import Robot
from . import Labyrinth
from . import Environment
from . import FormModule


# ==============================
@dataclass
class CStateResult:
    Reward: bool
    Punishment: bool


# ==============================
class CModule(emb.IModule):
    _qml_data: FormModule.PyFormData

    def __init__(self, i_xml_param: emb.TFullLineParam = None):
        v_param: emb.TFullLineParam = {
            emb.LineType.Input: {
                "Go": emb.CLineSize(4),  # North, East, South, West
                "DoWork": emb.CLineSize()
            },
            emb.LineType.Inner: {"": emb.CLineSize()},
            emb.LineType.Output: {
                "GCarrot": emb.CLineSize(),
                "GStick": emb.CLineSize(),
                "Hungry": emb.CLineSize(),
                "HearWork": emb.CLineSize(),
                "HearFood": emb.CLineSize(),
                "Wall": emb.CLineSize(4)  # North, East, South, West
            }
        }

        emb.IModule.__init__(self, v_param, i_xml_param)

        v_labyrinth_list = glob.glob(os.path.join(os.path.dirname(__file__), '*.labyr'))
        v_labyrinth_list.sort()
        print('\r\n'.join(v_labyrinth_list))
        self._LabyrinthMas = [Labyrinth.State(op.join(op.dirname(__file__), it)) for it in v_labyrinth_list]

        self.State = Environment.State(
            Robot.State(Robot.CEnergy(i_max_energy=500, i_low_energy=40)),
            self._LabyrinthMas
        )

        self._qml_data = FormModule.PyFormData(self.State)

    def get_param(self, i_net_module_param=None):
        return self.Param

    def get_component(self):
        return self._qml_data.get_component()

    def step(self, i_tick_idx, i_input):
        # v_in = [i_input[it][0] for it in self.Param[emb.State.Input]]
        #
        # v_inner = self.State.is_start(i_tick_idx, v_in)
        # v_output = self.State.get_current()
        #

        v_current_state = CStateResult(False, False)

        v_do_work = (i_input["DoWork"][0] == 1)
        v_in_go = i_input["Go"]
        v_sum_go = sum(v_in_go)
        v_do_step = (v_sum_go == 1)

        vl_current_lab = self.State.LabMas[self._qml_data.get_lab_idx()]

        if v_do_work:
            if vl_current_lab.robot_do_work():
                self._qml_data.update_work()
                v_current_state.Reward = True
            else:
                v_current_state.Punishment = True

        if v_sum_go > 1:
            v_current_state.Punishment = True

        self.State.do_step()

        if v_do_step:
            # self.State.Lab.set_robot_pos(i_tick_idx % self.State.Lab.get_size_count())
            if vl_current_lab.robot_do_step(v_in_go):
                # noinspection PyUnresolvedReferences
                self._qml_data.onRobotChanged.emit()
            else:
                v_current_state.Punishment = True

        v_lab_state = vl_current_lab.get_state()

        v_robot_state = self.State.Rob.set_input(i_tick_idx, v_lab_state.HearEnergy, v_do_work, v_do_step)

        v_out = emb.line_param_map_to_line_map(self.Param[emb.LineType.Output])

        emb.line_map_fill(v_out, 'GCarrot', v_current_state.Reward)
        emb.line_map_fill(v_out, 'GStick', v_current_state.Punishment)
        emb.line_map_fill(v_out, 'Hungry', v_robot_state.Hungry)
        emb.line_map_fill(v_out, 'HearWork', v_lab_state.HearWork)
        emb.line_map_fill(v_out, 'HearFood', v_lab_state.HearEnergy)
        emb.line_map_fill(v_out, 'Wall', vl_current_lab.get_robot_state())

        v_inner = {"": [v_current_state, v_lab_state, v_robot_state]}

        self._qml_data.set_status('Tick: {}'.format(i_tick_idx))

        return (
            v_inner,
            v_out
        )

    def get_caption(self):
        return "Labyrinth"

    def get_info(self):
        return \
"""### Тренировка работы действий от текущего входа и состояния

Ссылки: [issue64](https://gitlab.com/aiborisov84/ai_borisov_CIT/-/issues/64)

Актор - это робот

Среда - поле со стенами, клетками с работой и клеткой еды
"""
