import unittest

import typing

import model.emb as emb

from . import FormModule


# ==============================
class CModule(emb.IModule):
    _qml_data: typing.Optional[FormModule.PyFormData] = None

    def __init__(self, i_xml_param: emb.TFullLineParam = None):

        v_param: emb.TFullLineParam = {
            emb.LineType.Input: {
                'Action': emb.CLineSize(1)
            },
            emb.LineType.Inner: {"": emb.CLineSize(1)},
            emb.LineType.Output: {
                'Signal': emb.CLineSize(1),
                'Reward': emb.CLineSize(1)
            }
        }

        emb.IModule.__init__(self, v_param, i_xml_param)

        self.State = FormModule.CInnerState(
            i_reward_probability=1.0  # Вознаграждение сразу для лучшего просмотра краевых реакций
        )

        self._qml_data = FormModule.PyFormData(self.State)

    def get_param(self, i_net_module_param=None):
        return self.Param

    def step(self, i_tick_idx, i_input: emb.TLineDict):
        v_in = [i_input['Action'][0][0], 0]

        v_inner_state = self.State.check_and_start(i_tick_idx, v_in)
        v_out_state = self.State.get_current()

        v_out = emb.line_param_map_to_line_map(self.Param[emb.LineType.Output])

        emb.line_map_fill(v_out, 'Signal', v_out_state[0])
        emb.line_map_fill(v_out, 'Reward', v_inner_state['Reward'])

        self.State.do_step()

        v_result = (
            {"": [v_inner_state]},
            v_out
        )

        return v_result

    def get_component(self):
        return self._qml_data.get_component()

    def get_caption(self):
        return "Long same signal"

    def get_info(self):
        # DONE: Реакции на начало и окончание постоянного сигнала
        # TODO: Реакции на начало и окончание повторяющегося узора (1,0...) (1,0,0...) (1,0,1...)
        # TODO: Реакции на смену узора(интенсивности) сигнала
        return ("Тренировка трансформации постоянного входного сигнала \n"
                "с порождением внутренних сигналов на смену состояния \n"
                "И кодировщик смены интенсивности входного сигнала")


# ==============================
class TestNet(unittest.TestCase):
    def test(self):
        v_module = CModule()
        c_step = 0

        for i in range(30):
            v_in_line = emb.line_param_map_to_line_map(v_module.Param[emb.LineType.Input])
            if i == 5:
                emb.line_map_fill(v_in_line, 'Action', [1])

            v_step_result = v_module.step(c_step + i, v_in_line)

            print('Step {}\r\n\tIn: {}\r\n\tOut: {}'.format(
                i+1,
                v_in_line,
                v_step_result
            ))
