import QtQuick.Controls
import QtQuick
import QtQuick.Layouts

Item {
    anchors.fill: parent

    property font textFont: formData.getLogFont()

    function max(i1, i2) {
        return i1 < i2 ? i2 : i1
    }

    Page {
        anchors.fill: parent

        header: Page {
            ColumnLayout {
                id: topComp
                anchors.fill: parent

                Label {
                   id: simpleText
                   text: "Sign Sequence"
                }

                ComboBox {
                    width: parent.width
                    height: simpleText.height + 10
                    id: listKindView
                    model: ListModel {
                        id: cbItems
                    }

                    delegate: ItemDelegate {
                        topPadding: 0
                        bottomPadding: 0
                        width: listKindView.width
                        contentItem: Text {
                            text: modelData
                            color: "Black"
                            font: listKindView.font
                            elide: Text.ElideRight
                            verticalAlignment: Text.AlignVCenter
                        }
                        highlighted: listKindView.highlightedIndex === index
                    }

                    contentItem: Text {
                        leftPadding: 0
                        rightPadding: listKindView.indicator.width + listKindView.spacing

                        text: listKindView.displayText
                        font: listKindView.font
                        color: "Black"//listKindView.pressed ? "#17a81a" : "#21be2b"
                        verticalAlignment: Text.AlignVCenter
                        elide: Text.ElideRight
                    }

                    onActivated: function(index) {
                        formData.setKindMasIdx(index);
                    }
                }

                RowLayout {
                    width: parent.width
                    Button {
                        text: "Restart"
                        onClicked: formData.restart()
                    }
                }

                Flickable {
                    width: parent.width
                    height: max(simpleText.height, 50)
                    clip: true
                    TextArea.flickable: TextArea {
                        font: formData.getLogFont()
                        readOnly: true
                        //selectByMouse: true
                        textFormat: TextEdit.PlainText
                        wrapMode: TextEdit.NoWrap
                        text: formData.statusText
                    }
                    ScrollBar.vertical: ScrollBar {}
                    ScrollBar.horizontal: ScrollBar {}
                }
            }

            Component.onCompleted: {
                var vCount = formData.getKindMasCount();
                for(var i=0; i<vCount; i++)
                {
                    var vItem = formData.getKindMasItem(i);
                    listKindView.model.append({text: vItem});
                }
                listKindView.currentIndex = 0
            }
        }
    }

    Component.onCompleted: {
        console.log("FormModule Env1 Completed!")
    }

    Component.onDestruction: {
        console.log("FormModule Env1 Destruction!")
    }
}
