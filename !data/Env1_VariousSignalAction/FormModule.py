import typing
import random

from PySide6.QtGui import QFont
from PySide6.QtQml import QQmlEngine, QQmlComponent
from PySide6.QtCore import QObject, QCoreApplication, Slot, Property, Signal, QUrl  # ,QTimer

import os.path as op

import model.emb as emb


# ==============================
class CInnerState(emb.ISampleDetector):
    SampleMas: typing.List[emb.CSample]
    StepMas: typing.List[emb.IDynamic]
    DetectMas: typing.List[emb.CDetector]
    _sample_idx: int

    def __init__(self, i_reward_probability: float):
        self.RewardProbability = i_reward_probability

        self.Detector_Act = emb.CDetector(
            i_detect_mas=[
                emb.CDetectItem(1, None, None)
            ]
        )
        self.DetectMas = [self.Detector_Act]

        self.SampleMas = [
            emb.CSample(
                i_out_mas=[[1], ],
                i_repeat_count=20,
                i_silent_steps_before_cycle=10,
                i_caption='Fill [1,]*20'
            ),
            emb.CSample(
                i_out_mas=[[1], [0], ],
                i_repeat_count=20,
                i_silent_steps_before_cycle=10,
                i_caption='Odd [1,0,]*20'
            ),
            emb.CSample(
                i_out_mas=[[1], [0], [0], ],
                i_repeat_count=20,
                i_silent_steps_before_cycle=10,
                i_caption='Third [1,0,0,]*20'
            ),
            emb.CSample(
                i_out_mas=[[1], [0], [1], ],
                i_repeat_count=20,
                i_silent_steps_before_cycle=10,
                i_caption='Pattern [1,0,1,]*20'
            ),
            emb.CSample(
                i_out_mas=[[1], [0], [1], ],
                i_caption='OddOne [1,0,1]'
            ),
        ]
        self._sample_idx = 0

        self.StepMas = []
        self.StepMas.extend(self.DetectMas)
        self.StepMas.extend(self.SampleMas)

        self.ActionHistory = emb.CHistory()
        self.RewardHistory = emb.CHistory()
        self.SampleHistory = emb.CHistory()

    def select_sample(self, i_sample_idx: int):
        if self._sample_idx == i_sample_idx:
            return

        self.SampleMas[self._sample_idx].set_stop()
        self._sample_idx = i_sample_idx
        self.SampleMas[self._sample_idx].set_start()

    def restart(self):
        self.SampleMas[self._sample_idx].set_stop()
        self.SampleMas[self._sample_idx].set_start()

    def check_and_start(self, i_tick_idx, i_input):
        v_result = {
            'SampleState': '',
            'SignalSum': 0,
            'SignalHistory': '',
            'ActionDetect': 0,
            'ActionDetectHistory': '',
            'Reward': 0,
            'RewardHistory': '',
        }

        for it in self.DetectMas:
            it.check_and_start(i_tick_idx, i_input)

        if self.Detector_Act.is_detect_in_last:
            v_result['ActionDetect'] = 1

            # вероятность вознаграждения за действие
            if random.random() <= self.RewardProbability:
                v_result['Reward'] = 1

        v_current_sample = self.SampleMas[self._sample_idx]
        if i_tick_idx == 0:
            v_current_sample.set_start()

        v_result['SampleState'] = v_current_sample.state()
        v_result['SignalSum'] = sum(v_current_sample.get_current())

        self.ActionHistory.push(v_result['ActionDetect'])
        self.SampleHistory.push(v_result['SignalSum'])
        self.RewardHistory.push(v_result['Reward'])

        v_result['ActionDetectHistory'] = self.ActionHistory.state()
        v_result['SignalHistory'] = self.SampleHistory.state()
        v_result['RewardHistory'] = self.RewardHistory.state()

        return v_result

    def get_current(self):
        v_result = [0, 0]
        for itSample in self.SampleMas:
            c_current = itSample.get_current()
            if c_current is None:
                continue
            for it, itRS in enumerate(zip(v_result, c_current)):
                v_result[it] = max(itRS)

        # return list(map(max, *t))
        return v_result

    def do_step(self):
        for it in self.StepMas:
            it.do_step()


# ==============================
class PyFormData(QObject):
    onStatusChanged = Signal()
    _component: typing.Optional[QQmlComponent] = None
    _status_text: str = ''
    _inner_state: CInnerState

    def __init__(self, i_inner_state: CInnerState):
        QObject.__init__(self)

        self._inner_state = i_inner_state

        if QCoreApplication.instance():
            self._QmlEngine = QQmlEngine()
            self._QmlEngine.rootContext().setContextProperty(
                "formData", self)

            v_dir = op.dirname(__file__)
            self._QmlEngine.setBaseUrl(QUrl("file:///" + v_dir + "/"))

            self._component = QQmlComponent(
                self._QmlEngine,
                QUrl("FormModule.qml"),
                QQmlComponent.CompilationMode.PreferSynchronous
            )
            if self._component.status() != QQmlComponent.Status.Ready:
                self._component = None

    def __del__(self):
        self._component = None
        self._QmlEngine = None

    def get_component(self):
        return self._component

    def set_status(self, i_text: str):
        self._status_text = i_text
        # noinspection PyUnresolvedReferences
        self.onStatusChanged.emit()

    @Slot(result=QFont)
    def getLogFont(self):
        return emb.get_log_font()

    @Property(str, notify=onStatusChanged)
    def statusText(self):
        return self._status_text

    @Slot(result=int)
    def getKindMasCount(self):
        return len(self._inner_state.SampleMas)

    @Slot(int, result=str)
    def getKindMasItem(self, idx):
        return self._inner_state.SampleMas[idx].Caption

    @Slot(int)
    def setKindMasIdx(self, idx):
        self._inner_state.select_sample(idx)

    @Slot()
    def restart(self):
        self._inner_state.restart()

