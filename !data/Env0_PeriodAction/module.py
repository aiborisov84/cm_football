import random
import unittest

import model.emb as emb
from model.layercore.state import CRangeValue


# ==============================
class CInnerState(emb.ISampleDetector):
    def __init__(self, i_length: int):
        self.SampleMas = []
        self.StepMas = []
        self.DetectMas = []
        self.Length = CRangeValue(i_length)
        self.IsInit = False

        self.Detector_Action = emb.CDetector(
            i_detect_mas=[
                emb.CDetectItem(1, None, None)
            ]
        )
        self.DetectMas.append(self.Detector_Action)

        # self.Sample_Hungry = emb.CSample(
        #     i_out_mas=[
        #         [0, 1],
        #     ]
        # )
        #
        # self.SampleMas.append(self.Sample_Hungry)

        self.StepMas.append(self.Detector_Action)
        self.StepMas.extend(self.SampleMas)

    def check_and_start(self, i_tick_idx, i_input):
        v_result = {
            'Reward': 0
        }

        for it in self.DetectMas:
            it.check_and_start(i_tick_idx, i_input)

        if self.Detector_Action.is_detect_in_last:
            if self.IsInit:
                if 6 <= self.Length.Value <= 9:
                    v_result['Reward'] = 1
            else:
                self.IsInit = True
                v_result['Reward'] = 1

            self.Length.reset()

        if self.IsInit:
            v_result['StepFromStart'] = self.Length.Value

        return v_result

    def get_current(self):
        v_result = [0, 0]
        for itSample in self.SampleMas:
            c_sample_current = itSample.get_current()
            if c_sample_current is None:
                continue
            for it, itRS in enumerate(zip(v_result, c_sample_current)):
                v_result[it] = max(itRS)

        return v_result

    def do_step(self):
        if self.IsInit:
            self.Length.inc()
            if self.Length.is_max():
                self.IsInit = False

        for it in self.StepMas:
            it.do_step()


# ==============================
class CModule(emb.IModule):
    def __init__(self, i_xml_param: emb.TFullLineParam = None):
        v_param: emb.TFullLineParam = {
            emb.LineType.Input: {
                'Action': emb.CLineSize(1)
            },
            emb.LineType.Inner: {"": emb.CLineSize(1)},
            emb.LineType.Output: {
                'Reward': emb.CLineSize(1)
            }
        }

        emb.IModule.__init__(self, v_param, i_xml_param)

        self.State = CInnerState(i_length=20)

    def get_param(self, i_net_module_param=None):
        return self.Param

    def step(self, i_tick_idx, i_input: emb.TLineDict):
        v_in = [i_input["Action"][0][0]]

        v_inner_state = self.State.check_and_start(i_tick_idx, v_in)
        v_out_state = self.State.get_current()

        v_out = emb.line_param_map_to_line_map(self.Param[emb.LineType.Output])

        emb.line_map_fill(v_out, 'Reward', v_inner_state['Reward'])

        self.State.do_step()

        v_result = (
            {"": [v_inner_state]},
            v_out
        )

        return v_result

    def get_component(self):
        return None

    def get_caption(self):
        return "Action period selection"

    def get_info(self):
        return \
"""### Тренировка работы с подбором периодичности использования действий

Ссылки: [issue78](https://gitlab.com/aiborisov84/ai_borisov_CIT/-/issues/78)

Поощряется:
- Первое действие после простоя более 20 шагов
- Повторение действия после 6-и шагов, но до 9-ого шага от предыдущего действия

Наказывается:
- Действие, которое не поощрено
"""


# ==============================
class TestNet(unittest.TestCase):
    def test(self):
        c_step = 0
        v_module = CModule()

        for i in range(10):
            v_in_line = emb.line_param_map_to_line_map(v_module.Param[emb.LineType.Input])
            if i == 5:
                emb.line_map_fill(v_in_line, 'Action', [1])

            v_step_result = v_module.step(c_step + i, v_in_line)

            print('=== {}\nIn: {}\nOut: {}'.format(
                i,
                v_in_line,
                v_step_result
            ))
