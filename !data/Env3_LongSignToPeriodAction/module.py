import random
import unittest

if __name__ == '__main__':
    import sys
    import os.path as op

    cDir = op.dirname(__file__)
    vRootPath = op.join(cDir, '../../')
    vRootPath = op.abspath(vRootPath)
    sys.path.append(vRootPath)

    import model.emb as emb
else:
    import model.emb as emb


# ==============================
class CEnergy(emb.IDynamic):
    def __init__(self, i_max_energy, i_low_energy=None):
        self._MaxEnergy = i_max_energy

        if i_low_energy is not None:
            self._LowEnergy = i_low_energy
        else:
            self._LowEnergy = 2 * i_max_energy // 3

        self.Value = i_max_energy

    def do_step(self):
        self.sub(1)

    @property
    def is_low(self):
        return self.Value < self._LowEnergy

    def add(self, i_add_value):
        self.Value = self.Value + i_add_value
        if self._MaxEnergy < self.Value:
            self.Value = self._MaxEnergy

    def add_full(self):
        self.Value = self._MaxEnergy

    def sub(self, i_sub_value):
        self.Value = self.Value - i_sub_value
        if self.Value < 0:
            self.Value = 0


# ==============================
class CInnerState(emb.ISampleDetector):
    def __init__(self, i_energy: CEnergy):
        self.SampleMas = []
        self.StepMas = []
        self.DetectMas = []
        self.Energy = i_energy

        self.Detector_ActSearch = emb.CDetector(
            i_detect_mas=[
                emb.CDetectItem(1, None, None)
            ]
        )
        self.DetectMas.append(self.Detector_ActSearch)

        self.Sample_Hungry = emb.CSample(
            i_out_mas=[
                [0, 1],
            ]
        )

        self.SampleMas.append(self.Sample_Hungry)

        self.Sample_Eat = emb.CSample(
            i_out_mas=[
                [1, 0]
            ]
        )

        self.SampleMas.append(self.Sample_Eat)

        self.StepMas.append(self.Detector_ActSearch)
        self.StepMas.append(self.Energy)
        self.StepMas.extend(self.SampleMas)

    @property
    def is_hungry(self):
        return self.Energy.is_low

    def check_and_start(self, i_tick_idx, i_input):
        v_result = {
            'Energy': self.Energy.Value,
            'Hungry': 0,
            'Search': 0,
            'Reward': 0
        }
        list(map(
            lambda s: 1 if s.check_and_start(i_tick_idx, i_input) else 0,
            self.DetectMas
        ))

        if self.Detector_ActSearch.is_detect_in_last:
            v_result['Search'] = 1
            self.Energy.sub(3)

            # добавить вероятность найти еду
            if random.random() > 0.5:
                self.Sample_Eat.set_start()
                self.Energy.add_full()
                v_result['Reward'] = 1

        if self.is_hungry:
            # if iTickIdx % 15 == 3:
            v_result['Hungry'] = 1
            self.Sample_Hungry.set_start()

        return v_result

    def get_current(self):
        t = map(
            lambda s: s.get_current(),
            self.SampleMas
        )

        v_result = [0, 0]
        for itSample in t:
            if itSample is None:
                continue
            for it, itRS in enumerate(zip(v_result, itSample)):
                v_result[it] = max(itRS)

        # return list(map(max, *t))
        return v_result

    def do_step(self):
        list(map(
            lambda s: s.do_step(),
            self.StepMas
        ))


# ==============================
class CModule(emb.IModule):
    def __init__(self, i_xml_param: emb.TFullLineParam = None):
        v_param: emb.TFullLineParam = {
            emb.LineType.Input: {
                "Search": emb.CLineSize(1),
                "Rest": emb.CLineSize(1)
            },
            emb.LineType.Inner: {"": emb.CLineSize(1)},
            emb.LineType.Output: {
                "Eat": emb.CLineSize(1),
                "Hungry": emb.CLineSize(1),
                "Reward": emb.CLineSize(1)
            }
        }

        emb.IModule.__init__(self, v_param, i_xml_param)

        self.State = CInnerState(
            CEnergy(i_max_energy=50, i_low_energy=40)
        )

    def get_param(self):
        return self.Param

    def step(self, i_tick_idx, i_input: emb.TLineDict):
        v_in = [i_input["Search"][0][0], i_input["Rest"][0][0]]

        v_inner_state = self.State.check_and_start(i_tick_idx, v_in)
        v_out_state = self.State.get_current()

        v_out = emb.line_param_map_to_line_map(self.Param[emb.LineType.Output])

        emb.line_map_fill(v_out, 'Eat', v_out_state[0])
        emb.line_map_fill(v_out, 'Hungry', v_out_state[1])
        emb.line_map_fill(v_out, 'Reward', v_inner_state['Reward'])

        self.State.do_step()

        v_result = (
            {"": [v_inner_state]},
            v_out
        )

        return v_result

    def get_component(self):
        return None

    def get_caption(self):
        return "Long same signal"

    def get_info(self):
        return """Тренировка работы с длинной серией одинакового признака (Hungry)
Поиск баланса и периодичности использования действий:
- с удовлетворением подкрепления
- с уменьшением требовательности в еде (малая активность)

Буфер энергии B.max=50
Признак голод B<40
Поисковое действие (B-=3 энергии)
Расход энергии B-=1 в шаг времени. 
Поисковое действие в 50% попыток приводит к обнаружению еды
Поощрение выполняется при обнаружении еды
Получение энергии в обнаруженной еде (B.SetMax())"""


# ==============================
class TestNet(unittest.TestCase):
    def test(self):
        c_step = 0
        v_module = CModule()

        for i in range(10):
            v_in_line = emb.line_param_map_to_line_map(v_module.Param[emb.LineType.Input])
            if i == 5:
                emb.line_map_fill(v_in_line, 'Search', [1])

            v_step_result = v_module.step(c_step + i, v_in_line)

            print('In: {}\r\nOut: {}\r\nEn: {}'.format(
                v_in_line,
                v_step_result,
                v_module.State.Energy.Value
            ))
