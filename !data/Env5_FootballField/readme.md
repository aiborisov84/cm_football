# Описание

Обучение слоя контроля внимания "LayerAttentionControl" на комментируемое сопровождение объекта с использованием "окна", смещаемого по плоскому полю данных

- EnvField генерирует содержимое плоского 56х56 поля и "гладкое" движение объекта по нему
- EnvField обеспечивает интерфейс обучающих подсказок слою "LayerAttentionControl" и канал подкрепления/наказания
- EnvMovableAttention под управлением команд смещения "DoViewShift" слоя "LayerAttentionControl" реализует движение окна и предоставление "части" плоского поля данных с подписью смещения текущего положения окна.
- LayerD1 и LayerD2 - свертки по однородному полю признаков

  - LayerD1[54х54 => win(4x4) step(4x4) => 14x14]
  - LayerD2[14х14 => win(2x2) step(1x1) => 13x13]

Последовательность:

- Обучение алгоритму поиска объекта в поле стимуляцией LearnBoring
- Обучение алгоритму сопровождения объекта стимуляцией LearnObject
- Обучение алгоритму движения по границе стимуляцией LearnBorder
- Обучение комментирования текущей активности

![Схема потока](man/IMG_20230129_115326.jpg)

TODO:

- [ ]  EnvField
- [ ]  EnvMovableAttention
