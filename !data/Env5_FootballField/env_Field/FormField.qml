import QtQuick.Controls
import QtQuick
import QtQuick.Shapes


Page {
    anchors.fill: parent

    function min(i1, i2) {
        return i1 < i2 ? i1 : i2
    }

    function max(i1, i2) {
        return i1 < i2 ? i2 : i1
    }

    property bool isVisibleCellIndex: false
    property bool isVisibleVertical: true
    property bool isVisibleHorisontal: true
    property bool isVisibleWork: true

    property int imgMargin: 10

    property int rowCount: formData.rowCount
    property int colCount: formData.colCount
    property int imgScale: 2
    property int cellSize: imgScale * (min(width, height) - imgMargin*2) / max(rowCount, colCount)//75
    property int labWidth: colCount * cellSize
    property int labHeight: rowCount * cellSize
    property int robotLeft: formData.robotColIdx * cellSize
    property int robotTop: formData.robotRowIdx * cellSize

    function isVertical(iIdx) {
        if(formData)
            return formData.wallCol[iIdx].isOn
        else
            return false
    }

    function isHorisontal(iIdx) {
        if(formData)
            return formData.wallRow[iIdx].isOn
        else
            return false
    }

    function isWork(iIdx) {
        if(formData)
            return formData.work[iIdx].isOn
        else
            return false
    }

    function getColor(iIdx) {
        //return (iNum % 3) == 0;
        if(formData)
            return formData.getEnergyIdx() == iIdx ? "yellow" : "transparent"
        else
            return "transparent"
    }

    Flickable {
        anchors.fill: parent
        leftMargin: imgMargin
        topMargin: imgMargin
        bottomMargin: imgMargin
        rightMargin: imgMargin
        contentWidth: image.width; contentHeight: image.height
        clip: true

        ScrollBar.vertical: ScrollBar {}
        ScrollBar.horizontal: ScrollBar {}

        Rectangle {
            id: image
            //anchors.fill: parent
            width: labWidth
            height: labHeight
            color: "transparent"
            border.color : "black"

            Grid {
                id: labyrGrid
                columns: colCount
                rows: rowCount
                spacing: 0

                Repeater {
                    model: colCount * rowCount
                    Rectangle {
                        //id: rectangle
                        //anchors.fill: parent
                        width: cellSize
                        height: cellSize
                        color: getColor(index)
                        //border.color: "gray"

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                formData.moveRobot(index)
                            }
                        }

                        Shape {
                            anchors.fill: parent
                            visible: true

                            ShapePath {
                                strokeWidth: 1
                                strokeColor: "gray"
                                startX: 0
                                startY: 0
                                PathLine {
                                    x: cellSize
                                    y: 0
                                }
                                PathLine {
                                    x: cellSize
                                    y: cellSize
                                }
                                PathLine {
                                    x: 0
                                    y: cellSize
                                }
                                PathLine {
                                    x: 0
                                    y: 0
                                }
                            }
                        }

                        Text {
                            visible: isVisibleCellIndex
                            text: index + 1
                            anchors.fill: parent
                            font.pixelSize: parent.height / 2
                        }

                        Shape {
                            anchors.fill: parent
                            visible: isVisibleHorisontal && isHorisontal(index)

                            ShapePath {
                                strokeWidth: 1
                                strokeColor: "black"
                                startX: 1
                                startY: cellSize// - 1
                                PathLine {
                                    x: cellSize
                                    y: cellSize// - 1
                                }
                            }
                        }
                        Shape {
                            anchors.fill: parent
                            visible: isVisibleVertical && isVertical(index)

                            ShapePath {
                                strokeWidth: 1
                                strokeColor: "black"
                                startX: cellSize
                                startY: 0
                                PathLine {
                                    x: cellSize
                                    y: cellSize// - 1
                                }
                            }
                        }
                        Shape {
                            anchors.fill: parent
                            visible: isVisibleWork && isWork(index)

                            ShapePath {
                                strokeWidth: 1
                                strokeColor: "black"
                                startX: 1
                                startY: cellSize / 4
                                PathLine {
                                    x: cellSize / 4
                                    y: cellSize / 2
                                }
                                PathMove {
                                    x: 1
                                    y: cellSize / 2
                                }
                                PathLine {
                                    x: cellSize / 4
                                    y: cellSize / 4
                                }
                            }
                        }
                    }
                }
            }

            Shape {
                visible: true

                ShapePath {
                    strokeWidth: 1
                    strokeColor: "black"
                    startX: robotLeft + cellSize/4
                    startY: robotTop + cellSize/2
                    PathArc {
                        x: robotLeft + 3*cellSize/4;
                        y: robotTop + cellSize/2
                        radiusX: cellSize/4; radiusY: cellSize/4
                        useLargeArc: true
                    }
                    PathLine {
                        x: robotLeft + cellSize/4
                        y: robotTop + cellSize/2
                    }
                }
            }

        }
    }
}
