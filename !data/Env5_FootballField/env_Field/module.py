import typing
from dataclasses import dataclass
import glob
import os
import os.path as op

import model.emb as emb

from . import Robot
from . import Field
from . import Environment
from . import FormModule


# ==============================
@dataclass
class CStateResult:
    Reward: bool
    Punishment: bool


# ==============================
class CModule(emb.IModule):
    _qml_data: FormModule.PyFormData

    def __init__(self, i_xml_param: emb.TFullLineParam = None):
        """
        Line Format:
            <Input Name="DoWork" Size="1"/>
            <Input Name="Go" Size="4"/>
            <Output Name="Field" Size="14:14"/>
            <Output Name="L_Boring" Size="1"/>
            <Output Name="L_HearObject" Size="1"/>
            <Output Name="L_HearBorder" Size="1"/>
            <Output Name="L_ShiftMove" Size="4"/>
            <Output Name="GCarrot" Size="1"/>
            <Output Name="GStick" Size="1"/>
        """

        self._caption = "Flat field analyse"

        v_param: emb.TFullLineParam = {
            emb.LineType.Input: {
                "Go": emb.CLineSize(4),  # North, East, South, West
                "DoWork": emb.CLineSize()
            },
            emb.LineType.Inner: {"": emb.CLineSize()},
            emb.LineType.Output: {
                "Field": emb.CLineSize(i_array_shape=[14, 14]),
                "GCarrot": emb.CLineSize(i_user_control=True),
                "GStick": emb.CLineSize(i_user_control=True),
                "L_Boring": emb.CLineSize(i_user_control=True),
                "L_HearObject": emb.CLineSize(i_user_control=True),
                "L_HearBorder": emb.CLineSize(i_user_control=True),
                "L_ShiftMove": emb.CLineSize(4, i_user_control=True)  # North, East, South, West
            }
        }

        emb.IModule.__init__(self, v_param, i_xml_param)

        v_labyrinth_list = glob.glob(os.path.join(os.path.dirname(__file__), '*.field'))
        print('\r\n'.join(v_labyrinth_list))
        self._LabyrinthMas = [Field.State(op.join(op.dirname(__file__), it)) for it in v_labyrinth_list]

        self.State = Environment.State(
            Robot.State(Robot.CEnergy(i_max_energy=500, i_low_energy=40)),
            self._LabyrinthMas
        )

        self._qml_data = FormModule.PyFormData(self.State, self._caption)

    def get_component(self):
        return self._qml_data.get_component()

    def get_caption(self):
        return self._caption

    def get_info(self):
        return """!!! Вставить описание"""

    def step(self, i_tick_idx, i_input):
        v_current_state = CStateResult(False, False)

        c_input_do_work = i_input["DoWork"][0]
        c_input_go = i_input["Go"][0]
        v_do_work = (c_input_do_work[0] == 1)
        v_in_go = c_input_go.tolist()
        v_sum_go = sum(v_in_go)
        v_do_step = (v_sum_go == 1)

        vl_current_lab = self.State.LabMas[self._qml_data.get_lab_idx()]

        if v_do_work:
            if vl_current_lab.robot_do_work():
                self._qml_data.update_work()
                v_current_state.Reward = True
            else:
                v_current_state.Punishment = True

        if v_sum_go > 1:
            v_current_state.Punishment = True

        self.State.do_step()

        if v_do_step:
            if vl_current_lab.robot_do_step(v_in_go):
                # noinspection PyUnresolvedReferences
                self._qml_data.onRobotChanged.emit()
            else:
                v_current_state.Punishment = True

        v_lab_state = vl_current_lab.get_state()

        v_robot_state = self.State.Rob.set_input(i_tick_idx, v_lab_state.HearEnergy, v_do_work, v_do_step)

        v_out = emb.line_param_map_to_line_map(self.Param[emb.LineType.Output])

        emb.line_map_fill(v_out, 'GCarrot', v_current_state.Reward)
        emb.line_map_fill(v_out, 'GStick', v_current_state.Punishment)
        emb.line_map_fill(v_out, 'L_Boring', v_robot_state.Hungry)
        emb.line_map_fill(v_out, 'L_HearObject', v_lab_state.HearWork)
        emb.line_map_fill(v_out, 'L_HearBorder', v_lab_state.HearEnergy)
        emb.line_map_fill(v_out, 'L_ShiftMove', vl_current_lab.get_robot_state())

        vl_field = v_out['Field'][0]
        c_filed_range = vl_field.shape
        for it_col in range(c_filed_range[0]):
            for it_row in range(c_filed_range[1]):
                vl_field[it_col][it_row] = vl_current_lab.is_work_in_coord(Field.Coord(it_col, it_row))

        v_state = {"": [v_current_state, v_lab_state, v_robot_state]}

        self._qml_data.set_status('Tick: {}'.format(i_tick_idx))

        v_result = (
            v_state,
            v_out
        )

        return v_result
