import QtQuick.Controls
import QtQuick
import QtQuick.Layouts

Item {
    id: rootCmpt
    property string blockCaption : applicationData.textCaption
    anchors.fill: parent

    property font textFont: applicationData.getLogFont()

    Page {
        anchors.fill: parent

        header: Label {
           id: simpleText
           text: "View field state"
        }

        Flickable {
            width: parent.width
            anchors.fill: parent
            clip: true
            TextArea.flickable: TextArea {
                font: applicationData.getLogFont()
                readOnly: true
                //selectByMouse: true
                textFormat: TextEdit.PlainText
                wrapMode: TextEdit.NoWrap
                text: applicationData.statusText
            }
            ScrollBar.vertical: ScrollBar {}
            ScrollBar.horizontal: ScrollBar {}
        }
    }

    Component.onCompleted: {
        console.log("EnvModule Completed! ["+blockCaption+"]")
    }

    Component.onDestruction: {
        console.log("EnvModule Destruction! ["+blockCaption+"]")
    }
}
