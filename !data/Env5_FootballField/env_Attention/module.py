from dataclasses import dataclass
import sys
import os.path as op
import numpy

from PySide6.QtGui import QFont
from PySide6.QtQuick import QQuickItem
from PySide6.QtQml import QQmlComponent, QQmlEngine
from PySide6.QtCore import QObject, Slot, Property, Signal, QUrl, QCoreApplication  # , QTimer

if __name__ == '__main__':
    cDir = op.dirname(__file__)
    vRootPath = op.join(cDir, '../../../')
    vRootPath = op.abspath(vRootPath)
    sys.path.append(vRootPath)

import model.emb as emb


# ==============================
class PyApplicationData(QObject):
    onCaptionChanged = Signal()
    onStatusChanged = Signal()

    def __init__(self, i_caption: str):
        QObject.__init__(self)

        self._Root = None
        self._caption = i_caption
        self._status_text = ""

        # self._timer = QTimer()
        # self._timer.timeout.connect(self._update_robot)
        # self._timer.start(1000)   # ms

    @Slot(QQuickItem)
    def setRoot(self, i_root):
        self._Root = i_root
        if self._Root:
            pass

    @Property(str, notify=onCaptionChanged)
    def textCaption(self):
        return self._caption

    @Slot(result=QFont)
    def getLogFont(self):
        return emb.get_log_font()

    @Property(str, notify=onStatusChanged)
    def statusText(self):
        return self._status_text

    def set_status(self, i_status: str):
        self._status_text = i_status
        # noinspection PyUnresolvedReferences
        self.onStatusChanged.emit()


# ==============================
@dataclass
class CStateResult:
    Reward: bool
    Punishment: bool


# ==============================
class CModule(emb.IModule):
    _qml_data: PyApplicationData

    def __init__(self, i_xml_param: emb.TFullLineParam = None):
        """
        Line Format:
            <Input Name="DoViewShift" Size="4" Level="1"/>
            <Input Name="Object" Size="1"/>
            <Input Name="ObjectChange" Size="1"/>
            <Input Name="ObjectGo" Size="4"/>
            <Input Name="LongBorder" Size="4"/>
            <Output Name="Object" Size="1"/>
            <Output Name="ObjectChange" Size="1"/>
            <Output Name="ObjectGo" Size="4"/>
            <Output Name="LongBorder" Size="4"/>
            <Output Name="ViewShift" Size="4" Level="255"/>
        """

        self._caption = "Env Attention"

        v_param: emb.TFullLineParam = {
            emb.LineType.Input: {
                "DoViewShift": emb.CLineSize(4),  # North, East, South, West
                "Object": emb.CLineSize(),
                "ObjectChange": emb.CLineSize(),
                "ObjectGo": emb.CLineSize(4),
                "LongBorder": emb.CLineSize(4)
            },
            emb.LineType.Inner: {"": emb.CLineSize()},
            emb.LineType.Output: {
                "Object": emb.CLineSize(),
                "ObjectChange": emb.CLineSize(),
                "ObjectGo": emb.CLineSize(4),
                "LongBorder": emb.CLineSize(4),
                "ViewShift": emb.CLineSize(4, i_level=255)  # North, East, South, West
            }
        }

        emb.IModule.__init__(self, v_param, i_xml_param)

        if QCoreApplication.instance():
            self._QmlEngine = QQmlEngine()
            self._qml_data = PyApplicationData(self._caption)
            self._QmlEngine.rootContext().setContextProperty(
                "applicationData", self._qml_data)

            v_dir = op.dirname(__file__)
            self._QmlEngine.setBaseUrl(QUrl("file:///" + v_dir+"/"))

            self._component = QQmlComponent(self._QmlEngine)
            self._component.loadUrl(QUrl("module.qml"))
            if self._component.status() != QQmlComponent.Ready:
                self._component = None

    def get_component(self):
        return self._component

    def get_caption(self):
        return self._caption

    def get_info(self):
        return """!!! Вставить описание
Выхватывает зону из общего поля
Положение зоны управляемо линейным смещением
Абсолютное смещение зоны в выходном параметре
"""

    def step(self, i_tick_idx, i_input):
        v_current_state = CStateResult(False, False)

        v_out = emb.line_param_map_to_line_map(self.Param[emb.LineType.Output])

        v_state = {"": [v_current_state, ]}

        self._qml_data.set_status(f'Tick: {i_tick_idx}\r\n' + str(v_current_state))

        v_result = (
            v_state,
            v_out
        )

        return v_result
