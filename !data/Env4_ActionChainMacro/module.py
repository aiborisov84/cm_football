import enum

import model.emb as emb
import typing

# ==============================
# TODO: !!! Необходимо добавить "след" цепочки. Нужна информация не только о предыдущем узле, но и как
#       этот узел был активирован. Возможно, внутренняя цепочка должна быть не обусловленной, а "побуждающей"
# TODO: выбор действия в активной области памяти с частичной активацией обуславливающих признаков
# TODO: циклическое научение (с подкреплением) условного действия (рефлекса) на сигнал
# TODO: калибровка смещения (сближения и удаления) исполнения рефлекса от момента сигнала
# TODO: распределение управления рефлекса между областями памяти с разным времени задержки анализа и обработки
# TODO: лог активности Environment
# TODO: Управление внутренними флагами Environment
# TODO: Постепенное обучение длинной цепочки действий


# ==============================
class StateType(enum.Enum):
    LearnAction1 = 0
    LearnAction2 = 1
    LearnAction3 = 2
    LearnMacroSign = 3
    LearnMarcoActionSeq = 4
    TestMacro = 5


# ==============================
class CInnerState(emb.ISampleDetector):
    _sample_mas: typing.List[emb.ISampleDetector]
    _detector_mas: typing.List[emb.CDetector]

    def __init__(self, i_out_none):
        self._sample_mas = []
        self._detector_mas = []
        self._out_none = i_out_none

    def check_and_start(self, i_tick_idx, i_input):
        list(map(
            lambda s: 1 if s.check_and_start(i_tick_idx, i_input) else 0,
            self._detector_mas
        ))

        r = map(
            lambda s: 1 if s.check_and_start(i_tick_idx, i_input) else 0,
            self._sample_mas
        )
        return list(r)

    def get_current(self):
        t = list(map(
            lambda s: s.get_current(), 
            self._sample_mas
        ))
        return list(map(max, self._out_none, *t))

    def do_step(self):
        for it in self._detector_mas:
            it.do_step()
        for it in self._sample_mas:
            it.do_step()


# ==============================
class CInnerStateLearnAction1(CInnerState):
    def __init__(self, i_out_none, i_on_reward: typing.Callable):
        super().__init__(i_out_none)

        self._sample_mas.append(emb.CConditionalSample(
            i_out_mas=[
                [1, 0, 0, 0, 0]
            ],
            i_out_none=i_out_none,
            i_is_start=lambda i_tick_idx, i_input: True,
            i_length=8
        ))

        self._1_pattern_detector = emb.CDetector(
            i_detect_mas=[
                emb.CDetectItem(1, 0, 0)
            ],
            i_is_work=lambda: True,
            i_on_detect=i_on_reward
        )
        self._detector_mas.append(self._1_pattern_detector)


# ==============================
class CInnerStateLearnAction2(CInnerState):
    def __init__(self, i_out_none, i_on_reward: typing.Callable):
        super().__init__(i_out_none)

        self._sample_mas.append(emb.CConditionalSample(
            i_out_mas=[
                [0, 1, 0, 0, 0]
            ],
            i_out_none=i_out_none,
            i_is_start=lambda i_tick_idx, i_input: True,
            i_length=8
        ))

        self._2_pattern_detector = emb.CDetector(
            i_detect_mas=[
                emb.CDetectItem(0, 1, 0)
            ],
            i_is_work=lambda: True,
            i_on_detect=i_on_reward
        )
        self._detector_mas.append(self._2_pattern_detector)


# ==============================
class CInnerStateLearnAction3(CInnerState):
    def __init__(self, i_out_none, i_on_reward: typing.Callable):
        super().__init__(i_out_none)

        self._sample_mas.append(emb.CConditionalSample(
            i_out_mas=[
                [0, 0, 1, 0, 0]
            ],
            i_out_none=i_out_none,
            i_is_start=lambda i_tick_idx, i_input: True,
            i_length=8
        ))

        self._3_pattern_detector = emb.CDetector(
            i_detect_mas=[
                emb.CDetectItem(0, 0, 1)
            ],
            i_is_work=lambda: True,
            i_on_detect=i_on_reward
        )
        self._detector_mas.append(self._3_pattern_detector)


# ==============================
class CInnerStateLearnMacroSign(CInnerState):
    def __init__(self, i_out_none, i_on_end: typing.Callable):
        super().__init__(i_out_none)

        self._sample_mas.append(emb.CConditionalSample(
            i_out_mas=[
                [0, 0, 0, 1, 0]
            ],
            i_out_none=i_out_none,
            i_is_start=lambda i_tick_idx, i_input: True,
            i_length=10,
            i_on_end=i_on_end
        ))


# ==============================
class CInnerStateLearnMacroAction(CInnerState):
    def __init__(self, i_out_none, i_on_reward: typing.Callable):
        super().__init__(i_out_none)

        self._sample_mas.append(emb.CConditionalSample(
            i_out_mas=[
                [1, 0, 0, 1, 0],
                [0, 1, 0, 0, 0],
                [0, 0, 0, 0, 0],
                [0, 0, 1, 0, 0]
            ],
            i_out_none=i_out_none,
            i_is_start=lambda i_tick_idx, i_input: True,
            i_length=12
        ))

        self._full_pattern_detector1 = emb.CDetector(
            i_detect_mas=[
                emb.CDetectItem(1, 0, 0),
                emb.CDetectItem(0, 1, 0),
                emb.CDetectItem(0, 0, 0),
                emb.CDetectItem(0, 0, 1),
            ],
            i_is_work=lambda: True,
            i_on_detect=i_on_reward
        )
        self._detector_mas.append(self._full_pattern_detector1)


# ==============================
class CInnerStateTestMacroAction(CInnerState):
    def __init__(self, i_out_none, i_on_reward: typing.Callable):
        super().__init__(i_out_none)

        self._sample_mas.append(emb.CConditionalSample(
            i_out_mas=[
                [0, 0, 0, 1, 0]
            ],
            i_out_none=i_out_none,
            i_is_start=lambda i_tick_idx, i_input: True,
            i_length=12
        ))

        self._full_pattern_detector1 = emb.CDetector(
            i_detect_mas=[
                emb.CDetectItem(1, 0, 0),
                emb.CDetectItem(0, 1, 0),
                emb.CDetectItem(0, 0, 0),
                emb.CDetectItem(0, 0, 1),
            ],
            i_is_work=lambda: True,
            i_on_step_detect=i_on_reward
        )
        self._detector_mas.append(self._full_pattern_detector1)


# ==============================
class CInnerStateAll(CInnerState):
    _state: StateType
    _state_map: typing.Dict[StateType, CInnerState]

    def __init__(self):
        super().__init__([0, 0, 0, 0, 0])
        self._state = StateType.LearnAction1
        self._need_reward = False
        self._state_map = {}
        self._auto_next_state = True
        self._reward_counter = 0

        def reward_and_next_state(i_reward_count: int, i_state: StateType):
            def fun():
                if self._reward_counter < i_reward_count:
                    self._need_reward = True
                    self._reward_counter += 1

                if i_reward_count <= self._reward_counter and self._auto_next_state:
                    self._state = i_state
                    self._reward_counter = 0

            return fun

        self._state_map = {
            StateType.LearnAction1: CInnerStateLearnAction1(
                self._out_none,
                reward_and_next_state(2, StateType.LearnAction2)
            ),
            StateType.LearnAction2: CInnerStateLearnAction2(
                self._out_none,
                reward_and_next_state(2, StateType.LearnAction3)
            ),
            StateType.LearnAction3: CInnerStateLearnAction3(
                self._out_none,
                reward_and_next_state(1, StateType.LearnMacroSign)
            ),
            StateType.LearnMacroSign: CInnerStateLearnMacroSign(
                self._out_none,
                reward_and_next_state(0, StateType.LearnMarcoActionSeq)
            ),
            StateType.LearnMarcoActionSeq: CInnerStateLearnMacroAction(
                self._out_none,
                reward_and_next_state(3, StateType.TestMacro)
            ),
            StateType.TestMacro: CInnerStateTestMacroAction(
                self._out_none,
                reward_and_next_state(1, StateType.TestMacro)
            )
        }

        def reward_end():
            self._need_reward = False

        self._reward_sample = emb.CConditionalSample(
            i_out_mas=[
                [0, 0, 0, 0, 1]
            ],
            i_out_none=self._out_none,
            i_is_start=lambda i_tick_idx, i_input: self._need_reward,
            i_on_end=reward_end
        )

        self._sample_mas = [
            self._state_map[self._state],
            self._reward_sample
        ]

    def do_step(self):
        self._sample_mas[0] = self._state_map[self._state]
        super().do_step()

    def get_info(self):
        return {'CurrentState': self._state}


# ==============================
class CModule(emb.IModule):
    def __init__(self, i_xml_param: emb.TFullLineParam = None):
        self.State = CInnerStateAll()

        v_param: emb.TFullLineParam = {
            emb.LineType.Input: {"": emb.CLineSize(8)},
            emb.LineType.Inner: {"": emb.CLineSize()},
            emb.LineType.Output: {"": emb.CLineSize(4), "Reward": emb.CLineSize(1, i_user_control=True)}
        }

        emb.IModule.__init__(self, v_param, i_xml_param)

    def step(self, i_tick_idx, i_input):
        self.State.check_and_start(i_tick_idx, i_input[""])
        v_output_state = self.State.get_current()

        v_inner = {'': [self.State.get_info()]}
        v_out = emb.line_param_map_to_line_map(self.Param[emb.LineType.Output])

        emb.line_map_fill(v_out, '', v_output_state[0:-1])
        emb.line_map_fill(v_out, 'Reward', v_output_state[-1])

        self.State.do_step()

        return (
            v_inner,
            v_out
        )

    def get_component(self):
        return None

    def get_caption(self):
        return "Action Chain"

    def get_info(self):
        return """[LearnAction1 reward] =>
    [1, 0, 0, 0] SignAct1:learn2->Act1 [1, 0, 0]

[LearnAction2 reward] =>
    [0, 1, 0, 0] SignAct2:learn2->Act2 [0, 1, 0]

[LearnAction3 reward] =>
    [0, 0, 1, 0] SignAct3:learn2->Act3 [0, 0, 1]
    
[LearnMacroSign] =>
    [0, 0, 0, 1] SignMacroAction:learn0->{no action} 10_it
    
[LearnMarcoActionSeq reward] SignMacroAction :learn3 => MacroAction:
    [1, 0, 0, 1] SignAct1 + SignMacroAction ->Act1 [1, 0, 0]
    [0, 1, 0, 0] SignAct2 ->Act2 [0, 1, 0]
    [0, 0, 0, 0] 
    [0, 0, 1, 0] SignAct3 ->Act3 [0, 0, 1]
    
[TestMacro] =>
    [0, 0, 0, 1] SignMacroAction"""


# ==============================
def test(i_step=0):
    v_module = CModule()

    for i in range(10):
        if i == 5:
            v_in = [1, 0, 0, 0, 0, 0, 0, 0]
        else:
            v_in = [0, 0, 0, 0, 0, 0, 0, 0]
            
        print(v_module.step(i_step + i, {"": v_in}))


# ==============================
if __name__ == '__main__':
    test()
