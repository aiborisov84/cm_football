import model.emb as emb


# ==============================
class CInnerState(emb.ISampleDetector):
    def __init__(self):
        self.Sample = []
        self.Sample.append(emb.CConditionalSample(
            i_out_mas=[
                [0, 0, 0],
                [1, 1, 0],
                [0, 0, 1],
                [0, 1, 0],
            ],
            i_out_none=[0, 0, 0],
            i_is_start=lambda i_tick_idx, i_input: i_tick_idx % 20 == 0
        ))
        self.Detector = emb.CDetector(
            i_detect_mas=[
                emb.CDetectItem(1)
            ]
        )
        self.Sample.append(emb.CConditionalSample(
            i_out_mas=[
                [1, 1, 1]
            ],
            i_out_none=[0, 0, 0],
            i_is_start=self.Detector.check_and_start
        ))
        
    def check_and_start(self, i_tick_idx, i_input):
        r = map(
            lambda s: 1 if s.check_and_start(i_tick_idx, i_input) else 0,
            self.Sample
        )
        return list(r)

    def get_current(self):
        t = map(
            lambda s: s.get_current(), 
            self.Sample
        )
        return list(map(max, *t))

    def do_step(self):
        list(map(
            lambda s: s.do_step(), 
            self.Sample
        ))
        self.Detector.do_step()


# ==============================
class CModule(emb.IModule):
    def __init__(self, i_xml_param: emb.TFullLineParam = None):
        v_param: emb.TFullLineParam = {
            emb.LineType.Input: {"": emb.CLineSize(8)},
            emb.LineType.Inner: {"Sample": emb.CLineSize(2)},
            emb.LineType.Output: {"": emb.CLineSize(3), "Reward": emb.CLineSize(1)}
        }

        emb.IModule.__init__(self, v_param, i_xml_param)

        self.State = CInnerState()

    def get_param(self, i_net_module_param=None):
        return self.Param

    def step(self, i_tick_idx, i_input):
        v_inner_state = self.State.check_and_start(i_tick_idx, i_input[""])
        v_out_state = self.State.get_current()

        v_inner = emb.line_param_map_to_line_map(self.Param[emb.LineType.Inner])
        v_out = emb.line_param_map_to_line_map(self.Param[emb.LineType.Output])

        emb.line_map_fill(v_inner, 'Sample', v_inner_state)
        emb.line_map_fill(v_out, '', v_out_state)
        emb.line_map_fill(v_out, 'Reward', 1 if v_inner_state[1] > 0 else 0)

        self.State.do_step()

        return (
            v_inner,
            v_out
        )

    def get_component(self):
        return None

    def get_caption(self):
        return "Long pattern detect"

    def get_info(self):
        return """cycled(20) =>
    [0, 0, 0],
    [1, 1, 0],
    [0, 0, 1],
    [0, 1, 0]

detect(_0) =>
    [1, 1, 1] 
    reward"""


# ==============================
def test(i_step=0):
    v_module = CModule()

    for i in range(10):
        if i == 5:
            v_in = [1, 0, 0, 0, 0, 0, 0, 0]
        else:
            v_in = [0, 0, 0, 0, 0, 0, 0, 0]
            
        print(v_module.step(i_step + i, {"": v_in}))


# ==============================
if __name__ == '__main__':
    test()
