import typing
from dataclasses import dataclass
import copy
from enum import Enum


# ==============================
@dataclass
class Coord:
    Col: int
    Row: int

    def set(self, i_col, i_row):
        self.Col = i_col
        self.Row = i_row


@dataclass
class Range:
    Min: int
    Max: int

    def in_range(self, i_value):
        return self.Min <= i_value < self.Max


@dataclass
class CStateResult:
    AwayView: tuple[bool, bool]
    HandView: tuple[bool, bool]
    HearSample: bool
    HearAccept: bool


class DirectionType(Enum):
    right = 0
    bottom = 1
    left = 2
    top = 3


@dataclass
class CDynamicState:
    RobotPos: Coord
    Direction: DirectionType
    CoinsCellMas: list[bool]


# ==============================
class State:
    Size: Coord
    SamplePosMas: list[Coord]
    AcceptPosMas: list[Coord]
    CurrentDynamic: CDynamicState
    ColMap: typing.MutableMapping[int, typing.List[Range]]
    RowMap: typing.MutableMapping[int, typing.List[Range]]
    FileName: str

    def __init__(self, i_file_name):
        self.FileName = i_file_name

        def read_coord(i_str):
            v_str_coord = i_str.split(" ")
            return Coord(*list(map(int, v_str_coord)))

        def read_line(i_str):
            str_col, str_row = i_str.split(",")
            return [
                list(map(int, str_col.strip().split(" "))),
                list(map(int, str_row.strip().split(" ")))
            ]

        with open(i_file_name, "r") as f:
            self.Size = read_coord(f.readline().strip())
            v_sample_mas = f.readline().split(';')
            self.SamplePosMas = [read_coord(it.strip()) for it in v_sample_mas]
            v_accept_mas = f.readline().split(';')
            self.AcceptPosMas = [read_coord(it.strip()) for it in v_accept_mas]
            v_robot_pos = read_coord(f.readline().strip())
            self.ColMap = {}
            self.RowMap = {}
            for file_line in f:
                line_col, line_row = read_line(file_line.strip())
                if len(line_col) == 1:
                    self.ColMap.setdefault(line_col[0], []).append(Range(*line_row))
                elif len(line_row) == 1:
                    self.RowMap.setdefault(line_row[0], []).append(Range(*line_col))

        v_work_mas = [i % 3 == 0 for i in range(self.Size.Row * self.Size.Col)]

        self.CurrentDynamic = CDynamicState(v_robot_pos, DirectionType.right, v_work_mas)
        self._FirstState = copy.deepcopy(self.CurrentDynamic)

    def coord_to_idx(self, i_coord):
        return i_coord.Col + i_coord.Row * self.Size.Col

    def is_work_in_coord(self, i_coord: Coord):
        if i_coord.Col < 0 or self.Size.Col <= i_coord.Col:
            return False
        if i_coord.Row < 0 or self.Size.Row <= i_coord.Row:
            return False
        return self.CurrentDynamic.CoinsCellMas[self.coord_to_idx(i_coord)]

    def is_wall(self, i_coord: Coord, i_direction: DirectionType):
        match i_direction:
            case DirectionType.right:
                if i_coord.Col + 1 == self.Size.Col:
                    return True

                if i_coord.Col + 1 in self.ColMap:
                    for it in self.ColMap[i_coord.Col + 1]:
                        if it.in_range(i_coord.Row):
                            return True

            case DirectionType.left:
                if i_coord.Col == 0:
                    return True

                if i_coord.Col in self.ColMap:
                    for it in self.ColMap[i_coord.Col]:
                        if it.in_range(i_coord.Row):
                            return True

            case DirectionType.bottom:
                if i_coord.Row + 1 == self.Size.Row:
                    return True

                if i_coord.Row + 1 in self.RowMap:
                    for it in self.RowMap[i_coord.Row + 1]:
                        if it.in_range(i_coord.Col):
                            return True

            case DirectionType.top:
                if i_coord.Row == 0:
                    return True

                if i_coord.Row in self.RowMap:
                    for it in self.RowMap[i_coord.Row]:
                        if it.in_range(i_coord.Col):
                            return True

        return False

    def get_robot_state(self):
        v_robot_pos = self.CurrentDynamic.RobotPos
        return [
            self.is_wall(v_robot_pos, DirectionType.top),
            self.is_wall(v_robot_pos, DirectionType.right),
            self.is_wall(v_robot_pos, DirectionType.bottom),
            self.is_wall(v_robot_pos, DirectionType.left)
        ]

    def step(self, io_coord: Coord, i_direction: DirectionType):
        match i_direction:
            case DirectionType.right:
                if io_coord.Col == self.Size.Col - 1:
                    return False

                io_coord.Col += 1

            case DirectionType.left:
                if io_coord.Col == 0:
                    return False

                io_coord.Col -= 1

            case DirectionType.bottom:
                if io_coord.Row == self.Size.Row - 1:
                    return False

                io_coord.Row += 1

            case DirectionType.top:
                if io_coord.Row == 0:
                    return False

                io_coord.Row -= 1

        return True

    def robot_turn(self, i_direction: DirectionType):
        self.CurrentDynamic.Direction = i_direction

    def robot_do_step(self):
        if self.is_wall(self.CurrentDynamic.RobotPos, self.CurrentDynamic.Direction):
            return False

        return self.step(self.CurrentDynamic.RobotPos, self.CurrentDynamic.Direction)

    def robot_do_work(self):
        v_work_pos = self.robot_get_pos()
        if not self.CurrentDynamic.CoinsCellMas[v_work_pos]:
            return False

        self.CurrentDynamic.CoinsCellMas[v_work_pos] = False
        return True

    def robot_is_coins(self):
        return self.is_coins(self.CurrentDynamic.RobotPos, self.CurrentDynamic.Direction)

    def is_coins(self, i_coord: Coord, i_direction: DirectionType):
        v_pos = copy.copy(i_coord)
        if not self.step(v_pos, i_direction):
            return False

        v_idx = self.get_pos_idx(v_pos)
        return self.CurrentDynamic.CoinsCellMas[v_idx]

    def robot_set_pos(self, i_idx):
        v_row = i_idx // self.Size.Col
        v_col = i_idx % self.Size.Col

        if v_col < self.CurrentDynamic.RobotPos.Col:
            self.CurrentDynamic.Direction = DirectionType.left
        else:
            self.CurrentDynamic.Direction = DirectionType.right

        self.CurrentDynamic.RobotPos.set(v_col, v_row)

    def get_pos_idx(self, i_coord: Coord):
        return i_coord.Row * self.Size.Col + i_coord.Col

    def robot_get_pos(self):
        return self.get_pos_idx(self.CurrentDynamic.RobotPos)

    def robot_is_sample_cell(self):
        return self.CurrentDynamic.RobotPos in self.SamplePosMas

    def robot_is_accept_cell(self):
        return self.CurrentDynamic.RobotPos in self.AcceptPosMas

    def get_size_count(self):
        return self.Size.Col * self.Size.Row

    def get_state(self):
        return CStateResult(
            AwayView=(
                self.is_wall(self.CurrentDynamic.RobotPos, self.CurrentDynamic.Direction),
                self.is_coins(self.CurrentDynamic.RobotPos, self.CurrentDynamic.Direction)
            ),
            HandView=(False, False),
            HearSample=self.robot_is_sample_cell(),
            HearAccept=self.robot_is_accept_cell()
        )

    def __str__(self):
        a = list(map(str, [
            self.Size,
            self.SamplePosMas,
            self.AcceptPosMas,
            self.ColMap,
            self.RowMap]
        ))
        return "\n".join(a)

    def restart(self):
        self.CurrentDynamic = copy.deepcopy(self._FirstState)


def test():
    l1 = State("1.field")
    print(l1)


# ==============================
if __name__ == '__main__':
    test()
