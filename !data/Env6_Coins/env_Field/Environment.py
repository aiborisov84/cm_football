import typing

import model.emb as emb

from . import Robot, Field


# ==============================
class State(emb.IDynamic):
    LabMas: typing.List[Field.State]
    Rob: Robot.State
    _step_mas: typing.List[emb.IDynamic]

    def __init__(self, i_robot: Robot.State, i_lab_mas: typing.List[Field.State]):
        self.LabMas = i_lab_mas
        self.Rob = i_robot

        self._step_mas = []
        self._step_mas.append(self.Rob)

    def do_step(self):
        for it in self._step_mas:
            it.do_step()

    def restart(self):
        for it in self.LabMas:
            it.restart()

        self.Rob.restart()
