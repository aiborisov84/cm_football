import QtQuick.Controls
import QtQuick
import QtQuick.Layouts

Item {
    property string blockCaption : formData.textCaption
    anchors.fill: parent

    property font textFont: formData.getLogFont()

    Page {
        anchors.fill: parent

        header: ToolBar {
            id: mainToolBar
            Flow {
                anchors.fill: parent
                spacing: 5

                RowLayout {
                    Label {
                        id: simpleText
                        text: "Coins field"
                    }

                    ComboBox {
                        id: listLabView
                        width: 100
                        model: ListModel {
                            id: cbItems
                        }
                        flat: true

                        delegate: ItemDelegate {
                            topPadding: 0
                            bottomPadding: 0
                            width: listLabView.width
                            contentItem: Label {
                                text: modelData
                                font: listLabView.font
                                elide: Text.ElideRight
                                verticalAlignment: Text.AlignVCenter
                            }
                            highlighted: listLabView.highlightedIndex === index
                        }

                        onActivated: function(index) {
                            formData.setLabMasIdx(index);
                        }

                        Component.onCompleted: {
                            var vCount = formData.getLabMasCount();
                            for(var i=0; i<vCount; i++)
                            {
                                var vItem = formData.getLabMasFile(i);
                                listLabView.model.append({text: vItem});
                            }
                            listLabView.currentIndex = 0
                        }
                    }
                }

                Button {
                    id: btRestart
                    text: "Restart"
                    flat: true
                    onClicked: formData.restart()
                }

                RowLayout {
                    Label {text: "Scale"}
                    SpinBox {
                        id: spinBox
                        from: 1
                        to: 5
                        onValueModified: {
                            fmField.imgScale = value
                        }
                        Component.onCompleted: {
                            value = fmField.imgScale
                        }
                    }
                }

                CheckBox {
                    id: checkBox
                    text: qsTr("ShowIndex")
                    onCheckedChanged: {
                        fmField.isVisibleCellIndex = checked
                    }
                    Component.onCompleted: {
                        checked = fmField.isVisibleCellIndex
                    }
                }

                RowLayout {
                    visible: !hasMultipleLines(formData.statusText)
                    Label {text: "Status"}
                    TextField {
                        text: formData.statusText
                        font: formData.getLogFont()
                        readOnly: true
                    }
                }
            }
        }

        ColumnLayout {
            anchors.fill: parent

            Flickable {
                visible: hasMultipleLines(formData.statusText)
                Layout.fillWidth: true
                Layout.minimumHeight: statusTextArea.startHeight
                //Layout.preferredHeight: Math.min(parent.height/3, statusTextArea.height)

                clip: true
                TextArea.flickable: TextArea {
                    property int startHeight
                    id: statusTextArea
                    font: formData.getLogFont()
                    readOnly: true
                    //selectByMouse: true
                    textFormat: TextEdit.PlainText
                    wrapMode: TextEdit.NoWrap
                    text: formData.statusText

                    Component.onCompleted: {
                        statusTextArea.startHeight = statusTextArea.height
                        //console.log("!! start height ["+statusTextArea.height+"]")
                    }
                }
                ScrollBar.vertical: ScrollBar {}
                ScrollBar.horizontal: ScrollBar {}
            }

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true

                FormField {id: fmField}
            }
        }
    }

    Component.onCompleted: {
        console.log("EnvModule Completed! ["+blockCaption+"]")
    }

    Component.onDestruction: {
        console.log("EnvModule Destruction! ["+blockCaption+"]")
    }

    function hasMultipleLines(text) {
        return text.split("\n").length > 1;
    }
}
