import typing
from dataclasses import dataclass
import glob
import os
import os.path as op

import model.emb as emb

from . import Robot
from . import Field
from . import Environment
from . import FormModule


# ==============================
@dataclass
class CStateResult:
    Reward: bool
    Punishment: bool


# ==============================
class CModule(emb.IModule):
    _qml_data: FormModule.PyFormData

    def __init__(self, i_xml_param: emb.TFullLineParam = None):
        """
        Line Format:
            from CoinField.work_net
        """

        self._caption = "Coins field process"

        v_param: emb.TFullLineParam = {
            emb.LineType.Input: {
                "GetObject": emb.CLineSize(),
                "YieldObject": emb.CLineSize(),
                # [forward(East), backward(West)]
                # [North, East, South, West]
                "Turn": emb.CLineSize(2),
                # [Hand, Away]
                "Step": emb.CLineSize(),
            },
            emb.LineType.Inner: {"": emb.CLineSize()},
            emb.LineType.Output: {
                "GReward": emb.CLineSize(i_user_control=True),
                "GPunish": emb.CLineSize(i_user_control=True),
                "L_HandView": emb.CLineSize(1, [1, 2]),  # Coin class in hand
                "L_AwayView": emb.CLineSize(1, [1, 2]),  # View cell fill
                "L_HearSample": emb.CLineSize(),
                "L_HearAccept": emb.CLineSize(),
                "L_Boring": emb.CLineSize(i_user_control=True),
            }
        }

        emb.IModule.__init__(self, v_param, i_xml_param)

        v_labyrinth_list = glob.glob(os.path.join(os.path.dirname(__file__), '*.field'))
        print('\r\n'.join(v_labyrinth_list))
        self._LabyrinthMas = [Field.State(op.join(op.dirname(__file__), it)) for it in v_labyrinth_list]

        self.State = Environment.State(
            Robot.State(Robot.CEnergy(i_max_energy=500, i_low_energy=40)),
            self._LabyrinthMas
        )

        self._qml_data = FormModule.PyFormData(self.State, self._caption)

    def get_component(self):
        return self._qml_data.get_component()

    def get_caption(self):
        return self._caption

    def get_info(self):
        return """!!! Вставить описание"""

    def step(self, i_tick_idx, i_input):
        v_current_state = CStateResult(False, False)

        # c_input_do_work = i_input["DoWork"][0]
        # c_input_go = i_input["Go"][0]
        # v_do_work = (c_input_do_work[0] == 1)
        # v_in_go = c_input_go.tolist()
        v_do_work = False
        # step_top, step_right, step_bottom, step_left
        v_in_direction = [0, 1, 0, 0]
        v_sum_direction = sum(v_in_direction)
        v_do_step = True

        vl_current_lab = self.State.LabMas[self._qml_data.get_lab_idx()]

        if v_do_work:
            if vl_current_lab.robot_do_work():
                self._qml_data.update_work()
                v_current_state.Reward = True
            else:
                v_current_state.Punishment = True

        if v_sum_direction > 1:
            v_current_state.Punishment = True

        self.State.do_step()

        if v_do_step:
            if vl_current_lab.robot_do_step():
                # noinspection PyUnresolvedReferences
                self._qml_data.onRobotChanged.emit()
            else:
                v_current_state.Punishment = True

        v_lab_state = vl_current_lab.get_state()

        v_robot_state = self.State.Rob.set_input(i_tick_idx, False, v_do_work, v_do_step)

        v_out = emb.line_param_map_to_line_map(self.Param[emb.LineType.Output])

        emb.line_map_fill(v_out, 'GReward', v_current_state.Reward)
        emb.line_map_fill(v_out, 'GPunish', v_current_state.Punishment)
        emb.line_map_fill(v_out, 'L_HandView', v_lab_state.HandView)
        emb.line_map_fill(v_out, 'L_AwayView', v_lab_state.AwayView)
        emb.line_map_fill(v_out, 'L_HearSample', v_lab_state.HearSample)
        emb.line_map_fill(v_out, 'L_HearAccept', v_lab_state.HearAccept)
        emb.line_map_fill(v_out, 'L_Boring', v_robot_state.Hungry)

        # vl_field = v_out['Field'][0]
        # c_filed_range = vl_field.shape
        # for it_col in range(c_filed_range[0]):
        #     for it_row in range(c_filed_range[1]):
        #         vl_field[it_col][it_row] = vl_current_lab.is_work_in_coord(Field.Coord(it_col, it_row))

        v_state = {"": [v_current_state, v_lab_state, v_robot_state]}

        self._qml_data.set_status('Tick: {}'.format(i_tick_idx))

        v_result = (
            v_state,
            v_out
        )

        return v_result
