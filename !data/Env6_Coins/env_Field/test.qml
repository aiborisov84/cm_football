import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

ApplicationWindow {
    property Item envItem: null
    visible: true
    width: 500
    height: 500
    title: qsTr("test Env5")
    id: main

    RowLayout {
        anchors.fill: parent

        Page {
            width: parent.width/2
            height: parent.height

            header: ColumnLayout {
                width: parent.width

                Label {
                   id: simpleText
                   text: "Test"
                }
                Button {
                    text: "Step"
                    onClicked: pyconnect.step()
                }
            }
            Flickable {
                anchors.fill: parent
                clip: true
                TextArea.flickable: TextArea {
                    readOnly: true
                    textFormat: TextEdit.PlainText
                    wrapMode: TextEdit.NoWrap
                    font: pyconnect.getLogFont()
                    text: pyconnect.statusText
                }
                ScrollBar.vertical: ScrollBar {}
                ScrollBar.horizontal: ScrollBar {}
            }
        }

        Item {
            id: moduleMain
            width: parent.width/2
            height: parent.height
        }
    }

    function setCmpt(iComponent)
	{
	    if(envItem)
	    {
	        moduleMain.visible = false
            envItem.destroy()
            envItem = null
            console.log("== Completed EnvComponent Free!")
	    }

	    if(iComponent)
	    {
            console.log("== Completed EnvComponent Insert!")
            envItem = iComponent.createObject(moduleMain, {"x": 0, "y": 0});
            moduleMain.visible = true
	    }
	    else
	    {
            moduleMain.visible = false
	    }
	}
}