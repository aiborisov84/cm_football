import pprint
import unittest
import importlib
import sys
import os.path as op

import numpy
from PySide6.QtGui import QGuiApplication
from PySide6.QtQml import QQmlApplicationEngine, QQmlComponent
from PySide6.QtGui import QFont
from PySide6.QtCore import QObject, Slot, Property, Signal

import model.emb as emb


# ==============================
class TestAnalyse(unittest.TestCase):
    def test(self):
        app = QGuiApplication(sys.argv)

        v_import_dir = op.dirname(__file__)
        c_module_name = op.basename(v_import_dir)
        v_import_dir = op.dirname(v_import_dir)
        if v_import_dir not in sys.path:
            sys.path.insert(0, v_import_dir)
        v_mod = importlib.import_module(
            c_module_name,
            v_import_dir
        )

        v_module = v_mod.CModule()

        class PyConnect(QObject):
            onStatusChanged = Signal()
            _status_text: str

            def __init__(self, i_first_step: int = 0):
                QObject.__init__(self)
                self.current_step = 0
                self.go_vector = [0, 0, 0, 0]
                self.first_step = i_first_step
                self._status_text = ''

            @Property(str, notify=onStatusChanged)
            def statusText(self):
                return self._status_text

            @Slot(result=QFont)
            def getLogFont(self):
                return emb.get_log_font()

            @Slot()
            def step(self):
                if self.current_step % 3 == 0:
                    if sum(self.go_vector) == 0:
                        self.go_vector = [1, 0, 0, 0]
                    else:
                        self.go_vector = self.go_vector[-1:] + self.go_vector[:-1]

                    v_current_go_vector = self.go_vector
                else:
                    v_current_go_vector = [0, 0, 0, 0]

                def to_input_format(i_list):
                    return [numpy.asarray([it]) for it in i_list]

                v_in = {"Go": to_input_format(v_current_go_vector), "DoWork": to_input_format([0])}

                v_step_result = v_module.step(self.first_step + self.current_step, v_in)

                self._status_text = (
                    '== In:\n{}\n== Out:\n{}\n== En:\n{}'.format(
                        pprint.pformat(v_in),
                        pprint.pformat(v_step_result),
                        v_module.State.Rob.Energy.Value
                    )
                )
                self.onStatusChanged.emit()

                self.current_step += 1

        cmp: QQmlComponent = v_module.get_component()

        engine = QQmlApplicationEngine()
        py_connect = PyConnect(0)
        engine.rootContext().setContextProperty("pyconnect", py_connect)
        v_path = op.dirname(__file__)+"/test.qml"
        self.assertTrue(op.exists(v_path), v_path)
        engine.load(v_path)
        self.assertTrue(len(engine.rootObjects()) > 0, 'no root')
        engine.rootObjects()[0].setCmpt(cmp)

        def free():
            engine.rootObjects()[0].setCmpt(None)
            engine.deleteLater()

        app.aboutToQuit.connect(free)

        app.exec()
