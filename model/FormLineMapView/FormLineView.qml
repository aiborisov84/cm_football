import QtQuick.Controls
import QtQuick
import QtQuick.Shapes
import QtQuick.Layouts


Page {
    function min(i1, i2) {
        return i1 < i2 ? i1 : i2
    }

    function max(i1, i2) {
        return i1 < i2 ? i2 : i1
    }

    property bool isVisibleVertical: true
    property bool isVisibleHorisontal: true
    property bool isVisibleWork: true

    property int imgMargin: 10

    property int rowCount: applicationData.rowCount
    property int colCount: applicationData.colCount
    property int cellSize: (min(width, height) - imgMargin*2) / max(rowCount, colCount)//75
    property int labWidth: colCount * cellSize
    property int labHeight: rowCount * cellSize
    property int robotLeft: applicationData.robotColIdx * cellSize
    property int robotTop: applicationData.robotRowIdx * cellSize

    property font textLogFont: applicationData.getLogFont()

    StackLayout {
        anchors.fill: parent
        id: viewKind

        Flickable {
            //anchors.fill: parent
            leftMargin: imgMargin
            topMargin: imgMargin
            bottomMargin: imgMargin
            rightMargin: imgMargin
            contentWidth: image.width; contentHeight: image.height
            clip: true

            Rectangle {
                id: image
                //anchors.fill: parent
                width: labWidth
                height: labHeight
                color: "transparent" // "red"
                border.width: 0
                border.color : "black"

                Grid {
                    id: labyrGrid
                    columns: colCount
                    rows: rowCount
                    spacing: 0

                    Repeater {
                        id: cellMas
                        model: colCount * rowCount
                        Rectangle {
                            width: cellSize
                            height: cellSize
                            color: "transparent"
                            border.width: 0
                            border.color: "transparent"

                            property bool isWork: false
                            property bool isWallRow: false
                            property bool isWallCol: false

                            MouseArea {
                                anchors.fill: parent
                                acceptedButtons: Qt.LeftButton | Qt.RightButton
                                onClicked: {
                                    if (mouse.button == Qt.RightButton)
                                    {
                                        if (applicationData.changeWork(index))
                                            parent.isWork = !parent.isWork
                                    }
                                    else
                                    {
                                        applicationData.robotSetPosition(index)
                                    }
                                }
                            }

                            Shape {
                                anchors.fill: parent
                                visible: true

                                ShapePath {
                                    strokeWidth: 1
                                    strokeColor: "gray"
                                    fillColor: "transparent"
                                    startX: 0
                                    startY: 0
                                    PathLine {
                                        x: cellSize
                                        y: 0
                                    }
                                    PathLine {
                                        x: cellSize
                                        y: cellSize
                                    }
                                    PathLine {
                                        x: 0
                                        y: cellSize
                                    }
                                    PathLine {
                                        x: 0
                                        y: 0
                                    }
                                }
                            }

                            // Text {
                            //     text: index + 1
                            //     font: textLogFont
                            // }
                            Shape {
                                anchors.fill: parent
                                //visible: false
                                visible: isVisibleHorisontal && isWallRow

                                ShapePath {
                                    strokeWidth: 1
                                    strokeColor: "black"
                                    fillColor: "transparent"
                                    startX: 1
                                    startY: cellSize// - 1
                                    PathLine {
                                        x: cellSize
                                        y: cellSize// - 1
                                    }
                                }
                            }
                            Shape {
                                anchors.fill: parent
                                visible: isVisibleVertical && isWallCol

                                ShapePath {
                                    strokeWidth: 1
                                    strokeColor: "black"
                                    fillColor: "transparent"
                                    startX: cellSize
                                    startY: 0
                                    PathLine {
                                        x: cellSize
                                        y: cellSize// - 1
                                    }
                                }
                            }
                            Shape {
                                anchors.fill: parent
                                visible: isVisibleWork && isWork

                                ShapePath {
                                    strokeWidth: 1
                                    strokeColor: "black"
                                    fillColor: "transparent"
                                    startX: 1
                                    startY: cellSize / 4
                                    PathLine {
                                        x: cellSize / 4
                                        y: cellSize / 2
                                    }
                                    PathMove {
                                        x: 1
                                        y: cellSize / 2
                                    }
                                    PathLine {
                                        x: cellSize / 4
                                        y: cellSize / 4
                                    }
                                }
                            }
                        }
                    }
                }

                Shape {
                    visible: true

                    ShapePath {
                        strokeWidth: 1
                        strokeColor: "black"
                        startX: robotLeft + cellSize/4
                        startY: robotTop + cellSize/2
                        PathArc {
                            x: robotLeft + 3*cellSize/4;
                            y: robotTop + cellSize/2
                            radiusX: cellSize/4; radiusY: cellSize/4
                            useLargeArc: true
                        }
                        PathLine {
                            x: robotLeft + cellSize/4
                            y: robotTop + cellSize/2
                        }
                    }
                }
            }
        }

        Flickable {
            //anchors.fill: parent

            id: flicArea
            clip: true
            TextArea.flickable: TextArea {
                id: cmptTxt
                readOnly: true
                //selectByMouse: true
                textFormat: TextEdit.PlainText
                font: textLogFont
                wrapMode: TextEdit.NoWrap
            }

            ScrollBar.vertical: ScrollBar {}
            ScrollBar.horizontal: ScrollBar {}
        }
    }

    function refreshCellMas() {
        if (applicationData.isArray())
        {
            viewKind.currentIndex = 0

            var vCount = cellMas.count;
            for(var i=0; i<vCount; i++)
            {
                var vItem = cellMas.itemAt(i);
                vItem.isWork = applicationData.isWork(i)
                vItem.isWallRow = applicationData.isWallRow(i)
                vItem.isWallCol = applicationData.isWallCol(i)
                vItem.color = applicationData.isEnergy(i) ? "yellow" : "transparent"
            }
        }
        else
        {
            viewKind.currentIndex = 1
        }

        cmptTxt.text = applicationData.getLineStatus()
    }

    Component.onCompleted: {
        console.log("FormLineView Completed!")

        applicationData.setRoot(this)
    }

    Component.onDestruction: {
        console.log("FormLineView Destruction!")

        applicationData.setRoot(null)
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
