import typing
from dataclasses import dataclass
import copy


# ==============================
@dataclass
class Coord:
    Row: int
    Col: int

    def set(self, i_row, i_col):
        self.Row = i_row
        self.Col = i_col


@dataclass
class Range:
    Min: int
    Max: int

    def in_range(self, i_value):
        return self.Min <= i_value < self.Max


@dataclass
class CStateResult:
    HearWork: int
    HearEnergy: bool


# ==============================
class State:
    Size: Coord
    LineSize: int
    RobotPos: Coord
    ColMap: typing.MutableMapping[int, typing.List[Range]]
    RowMap: typing.MutableMapping[int, typing.List[Range]]

    def __init__(self, i_size: Coord):
        self.ColMap = {}
        self.RowMap = {}

        self.set_size(i_size)
        self.RobotPos = Coord(0, 0)
        self._FirstRobotPos = copy.deepcopy(self.RobotPos)

    def set_size(self, i_size: Coord):
        self.Size = i_size
        self.LineSize = self.Size.Row * self.Size.Col

    def coord_to_idx(self, i_coord: Coord) -> typing.Optional[int]:
        if i_coord.Col < 0 or self.Size.Col <= i_coord.Col:
            return None
        if i_coord.Row < 0 or self.Size.Row <= i_coord.Row:
            return None
        return i_coord.Col + i_coord.Row * self.Size.Col

    def idx_to_coord(self, i_idx: int) -> typing.Optional[Coord]:
        if 0 <= i_idx < self.LineSize:
            return Coord(i_idx // self.Size.Col, i_idx % self.Size.Col)
        else:
            return None

    def is_left_wall(self, i_coord: Coord):
        if i_coord.Col == 0:
            return True

        if i_coord.Col in self.ColMap:
            for it in self.ColMap[i_coord.Col]:
                if it.in_range(i_coord.Row):
                    return True

        return False

    def is_right_wall(self, i_coord: Coord):
        if i_coord.Col+1 == self.Size.Col:
            return True

        if i_coord.Col+1 in self.ColMap:
            for it in self.ColMap[i_coord.Col+1]:
                if it.in_range(i_coord.Row):
                    return True

        return False

    def is_top_wall(self, i_coord: Coord):
        if i_coord.Row == 0:
            return True

        if i_coord.Row in self.RowMap:
            for it in self.RowMap[i_coord.Row]:
                if it.in_range(i_coord.Col):
                    return True

        return False

    def is_bottom_wall(self, i_coord: Coord):
        if i_coord.Row+1 == self.Size.Row:
            return True

        if i_coord.Row+1 in self.RowMap:
            for it in self.RowMap[i_coord.Row+1]:
                if it.in_range(i_coord.Col):
                    return True

        return False

    def get_robot_state(self):
        v_robot_pos = self.RobotPos
        return [
            self.is_top_wall(v_robot_pos),
            self.is_right_wall(v_robot_pos),
            self.is_bottom_wall(v_robot_pos),
            self.is_left_wall(v_robot_pos)
        ]

    def step_top(self):
        if self.RobotPos.Row == 0:
            return False

        self.RobotPos.Row -= 1
        return True

    def step_right(self):
        if self.RobotPos.Col == self.Size.Col-1:
            return False

        self.RobotPos.Col += 1
        return True

    def step_bottom(self):
        if self.RobotPos.Row == self.Size.Row-1:
            return False

        self.RobotPos.Row += 1
        return True

    def step_left(self):
        if self.RobotPos.Col == 0:
            return False

        self.RobotPos.Col -= 1
        return True

    def robot_do_step(self, i_step_direction):
        v_wall_func: typing.List[typing.Callable[[Coord], bool]] = [
            self.is_top_wall, self.is_right_wall, self.is_bottom_wall, self.is_left_wall
        ]
        v_step_func = self.step_top, self.step_right, self.step_bottom, self.step_left
        for st, wall, step in zip(i_step_direction, v_wall_func, v_step_func):
            if st == 0:
                continue

            if wall(self.RobotPos):
                return False

            if not step():
                return False

        return True

    def robot_set_pos(self, i_idx):
        v_row = i_idx // self.Size.Col
        v_col = i_idx % self.Size.Col

        self.RobotPos.set(v_row, v_col)

    def robot_get_pos(self):
        return self.RobotPos.Row * self.Size.Col + self.RobotPos.Col

    def get_size_count(self):
        return self.Size.Col * self.Size.Row

    def get_state(self):
        return CStateResult(False, False)

    def __str__(self):
        a = list(map(str, [self.Size, self.ColMap, self.RowMap]))
        return "\n".join(a)

    def restart(self):
        self.RobotPos = copy.deepcopy(self._FirstRobotPos)
