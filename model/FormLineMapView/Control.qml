import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick 2.11

Item {
    id: rootCmpt
    property string blockCaption : applicationData.textCaption
    anchors.fill: parent

    property font textFont: applicationData.getLogFont()

    Page {
        anchors.fill: parent

        header: Item {
            height: cbOrientation.height
            Label {
               id: simpleText
               anchors.verticalCenter: parent.verticalCenter
               text: "Current line view"
               verticalAlignment: Text.AlignVCenter
            }
            CheckBox {
                id: cbOrientation
                 text: qsTr("Vert|Hor")
                 anchors.right: parent.right
                 checked: false
            }
        }

        SplitView {
            id: splitCustom
            orientation: (cbOrientation.checkState === Qt.Checked) ? Qt.Vertical : Qt.Horizontal
            anchors.fill: parent

            handle: Rectangle {
                 implicitWidth: 4
                 implicitHeight: 4

                 color: SplitHandle.pressed ? splitCustom.palette.mid
                     : (SplitHandle.hovered ? splitCustom.palette.light : splitCustom.palette.midlight)

            }

            Item {
                SplitView.preferredWidth: parent.width*0.4
                SplitView.preferredHeight: parent.height*0.4

                GridLayout {
                    id: lineSelector
                    anchors.top: parent.top
                    width: parent.width

                    columns: 2
                    columnSpacing: 2
                    rowSpacing: 2

                    Label {text: "LineName"}
                    ComboBox {
                        id: listLabView
                        Layout.fillWidth: true

                        model: ListModel {
                            id: cbItems
                        }

                        delegate: ItemDelegate {
                            topPadding: 0
                            bottomPadding: 0
                            width: listLabView.width
                            contentItem: Label {
                                text: modelData
                                //color: "Black"
                                font: listLabView.font
                                elide: Text.ElideRight
                                verticalAlignment: Text.AlignVCenter
                            }
                            highlighted: listLabView.highlightedIndex == index
                        }

                        contentItem: Label {
                            leftPadding: 10
                            rightPadding: listLabView.indicator.width + listLabView.spacing

                            text: listLabView.displayText
                            font: listLabView.font
                            //color: "Black" // listLabView.pressed ? "#17a81a" : "#21be2b"
                            verticalAlignment: Text.AlignVCenter
                            elide: Text.ElideRight
                        }

                        onActivated: function(index) {
                            applicationData.setLineIdx(index);
                        }

                        Component.onCompleted: {
                            var vCount = applicationData.getLineCount();
                            for(var i=0; i<vCount; i++)
                            {
                                var vItem = applicationData.getLineName(i);
                                listLabView.model.append({text: vItem});
                            }
                            listLabView.currentIndex = 0
                        }
                    }

                    Label {text: "ThreadIdx["+(applicationData.threadCount).toString()+"]"}
                    SpinBox {
                        value: applicationData.lineThreadIdx
                        editable: true
                        to: applicationData.threadCount-1

                        onValueModified: {
                            applicationData.setLineThreadIdx(value);
                        }
                    }
                    Label {text: "Status"}
                }

                Flickable {
                    //height: simpleText.height
                    anchors.left: parent.left
                    anchors.top: lineSelector.bottom
                    anchors.bottom: parent.bottom

                    width: parent.width

                    clip: true
                    TextArea.flickable: TextArea {
                        font: applicationData.getLogFont()
                        readOnly: true
                        //selectByMouse: true
                        textFormat: TextEdit.PlainText
                        wrapMode: TextEdit.NoWrap
                        text: applicationData.statusText
                    }
                    ScrollBar.vertical: ScrollBar {}
                    ScrollBar.horizontal: ScrollBar {}
                }
            }

            //Rectangle {color: "Red"; height: 100; width: 200}

            FormLineView {
                SplitView.fillWidth: true
                SplitView.fillHeight: true
                //anchors.fill: parent
            }
        }
    }

    Component.onCompleted: {
        console.log("Control_LineView Completed! ["+blockCaption+"]")
    }

    Component.onDestruction: {
        console.log("Control_LineView Destruction! ["+blockCaption+"]")
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
