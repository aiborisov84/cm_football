import copy
import typing
from dataclasses import dataclass
import sys
import os.path as op
import numpy

from PySide6.QtGui import QFont
from PySide6.QtQuick import QQuickItem
from PySide6.QtQml import QQmlComponent, QQmlEngine
from PySide6.QtCore import QObject, Slot, Property, Signal, QUrl, QCoreApplication  # , QTimer

if __name__ == '__main__':
    cDir = op.dirname(__file__)
    vRootPath = op.join(cDir, '../../../')
    vRootPath = op.abspath(vRootPath)
    sys.path.append(vRootPath)

import model.emb as emb

from . import ViewField


# ==============================
@dataclass
class LineWorkParam:
    Type: emb.LineType
    Name: str
    Size: emb.CLineSize
    Data: typing.List[numpy.ndarray]
    UserData: typing.List[numpy.ndarray]

    def get_address_str(self):
        return f'{emb.line_type_to_str(self.Type)}_{self.Name}'


# ==============================
class PyControlData(QObject):
    onCaptionChanged = Signal()
    onSizeChanged = Signal()
    onRobotChanged = Signal()
    onLineParamChanged = Signal()
    onLineThreadIdxChanged = Signal()
    onStatusChanged = Signal()
    _state: ViewField.State

    def __init__(self,
                 i_line_mas: typing.List[LineWorkParam],
                 i_caption: str
                 ):
        QObject.__init__(self)

        self._Root = None
        self._line_mas = i_line_mas
        self._line_idx = 0
        self._line_thread_idx = 0
        self._state = ViewField.State(ViewField.Coord(1, 1))

        self._caption = i_caption
        self._status_text = ''

        # self._timer = QTimer()
        # self._timer.timeout.connect(self._update_robot)
        # self._timer.start(1000)   # ms

    @Slot(QQuickItem)
    def setRoot(self, i_root):
        self._Root = i_root
        if self._Root:
            self._select_line(0)

    def _select_thread(self, i_field_thread_idx):
        if self._line_thread_idx == i_field_thread_idx:
            return

        self._line_thread_idx = i_field_thread_idx
        # noinspection PyUnresolvedReferences
        self.onLineThreadIdxChanged.emit()

        if self._Root:
            self._Root.refreshCellMas()

    def _select_line(self, i_line_idx):
        if self._line_idx == i_line_idx:
            return

        self._line_idx = i_line_idx
        self._refresh_line_info()

    def _refresh_line_info(self):
        c_line = self._line_mas[self._line_idx]

        if c_line.Size.ThreadCount <= self._line_thread_idx:
            self._line_thread_idx = 0
            # noinspection PyUnresolvedReferences
            self.onLineThreadIdxChanged.emit()

        c_shape = c_line.Size.ArrayShape
        if len(c_shape) == 1:
            self._state.set_size(ViewField.Coord(c_shape[0], 1))
        elif len(c_shape) == 2:
            self._state.set_size(ViewField.Coord(c_shape[0], c_shape[1]))
        else:
            self._state.set_size(ViewField.Coord(1, 1))

        # noinspection PyUnresolvedReferences
        self.onLineParamChanged.emit()

        # noinspection PyUnresolvedReferences
        self.onSizeChanged.emit()

        # noinspection PyUnresolvedReferences
        self.onRobotChanged.emit()

        if self._Root:
            self._Root.refreshCellMas()

    def _update_robot(self):
        if self._state.RobotPos.Col < 4:
            self._state.RobotPos.Col = self._state.RobotPos.Col + 1
        else:
            self._state.RobotPos.Col = self._state.RobotPos.Col - 1

        # noinspection PyUnresolvedReferences
        self.onRobotChanged.emit()

    def fill_state(self, i_full_line_state: emb.TFullLineState):
        v_line_status_set = []
        v_line_status_detect = []
        for it, it_line in enumerate(self._line_mas):
            v_type_lines = i_full_line_state[it_line.Type]

            v_line = v_type_lines[it_line.Name]
            if not v_line:
                it_line.Data = '<None>'
            else:
                v_thread = v_line[0]
                if not isinstance(v_thread, numpy.ndarray):
                    it_line.Data = emb.data_to_text(v_line)
                else:
                    if tuple(it_line.Size.ArrayShape) != v_thread.shape:
                        it_line.Size.ArrayShape = list(v_thread.shape)
                        it_line.UserData = []

                    if it_line.UserData:
                        v_detect_count = 0
                        v_set_count = 0
                        for it_thread_num, it_user_thread in enumerate(it_line.UserData):
                            if it_user_thread.any():
                                v_nd_iter = numpy.nditer(it_user_thread, flags=['multi_index'])
                                for x in v_nd_iter:
                                    if x > 0:
                                        if it_line.Size.UserControl and it_line.Type == emb.LineType.Output:
                                            if v_line[it_thread_num][v_nd_iter.multi_index] == 0:
                                                v_line[it_thread_num][v_nd_iter.multi_index] = x
                                                v_set_count += 1
                                        else:
                                            if v_line[it_thread_num][v_nd_iter.multi_index] > 0:
                                                v_detect_count += 1

                        if v_detect_count:
                            v_line_status_detect.append(
                                f'{it_line.get_address_str()}[{v_detect_count}]'
                            )

                        if v_set_count:
                            v_line_status_set.append(f'{it_line.get_address_str()}[{v_set_count}]')
                    else:
                        it_line.UserData = [numpy.zeros(v_thread.shape, dtype=int) for _ in v_line]

                    it_line.Data = copy.deepcopy(v_line)

        if self._Root:
            self._Root.refreshCellMas()

        self._status_text = (
            emb.info_block_to_text('Detect', '\r\n'.join(v_line_status_detect)) +
            '\r\n' +
            emb.info_block_to_text('Set', '\r\n'.join(v_line_status_set))
        )

        # noinspection PyUnresolvedReferences
        self.onStatusChanged.emit()

        self._refresh_line_info()

    @Property(str, notify=onCaptionChanged)
    def textCaption(self):
        return self._caption

    @Slot(result=QFont)
    def getLogFont(self):
        return emb.get_log_font()

    @Property(str, notify=onStatusChanged)
    def statusText(self):
        return self._status_text

    @Property(int, notify=onSizeChanged)
    def colCount(self):
        return self._state.Size.Col

    @Property(int, notify=onSizeChanged)
    def rowCount(self):
        return self._state.Size.Row

    @Slot(int, result=bool)
    def isWallRow(self, i_cell_idx):
        c_coord = self._state.idx_to_coord(i_cell_idx)
        if c_coord is None:
            return False

        return self._state.is_top_wall(c_coord)

    @Slot(int, result=bool)
    def isWallCol(self, i_cell_idx):
        c_coord = self._state.idx_to_coord(i_cell_idx)
        if c_coord is None:
            return False

        return self._state.is_left_wall(c_coord)

    @Slot(result=bool)
    def isArray(self):
        if not self._line_mas:
            return False

        vl_line = self._line_mas[self._line_idx]

        if not vl_line.Data:
            return False

        return isinstance(vl_line.Data[0], numpy.ndarray)

    @Slot(result=str)
    def getLineStatus(self):
        vl_line = self._line_mas[self._line_idx]
        if not vl_line.Data:
            return ''

        if not isinstance(vl_line.Data, str):
            return ''

        return vl_line.Data

    def _cell_data_get(self, i_data, i_cell_idx):
        if not i_data:
            return None

        if len(i_data) <= self._line_thread_idx:
            return None

        vl_thread = i_data[self._line_thread_idx]

        if not isinstance(vl_thread, numpy.ndarray):
            return None

        c_coord = self._state.idx_to_coord(i_cell_idx)
        if c_coord is None:
            return None

        if vl_thread.ndim == 1:
            return vl_thread, c_coord.Col
        elif vl_thread.ndim == 2:
            return vl_thread, (c_coord.Row, c_coord.Col)
        else:
            return None

    @Slot(int, result=bool)
    def isWork(self, i_cell_idx):
        vl_line = self._line_mas[self._line_idx]
        c_data = self._cell_data_get(vl_line.UserData, i_cell_idx)
        if c_data is None:
            return False

        c_array, c_idx = c_data
        return c_array[c_idx] > 0

    @Slot(int, result=int)
    def isEnergy(self, i_cell_idx):
        vl_line = self._line_mas[self._line_idx]
        c_data = self._cell_data_get(vl_line.Data, i_cell_idx)
        if c_data is None:
            return False

        c_array, c_idx = c_data
        return c_array[c_idx] > 0

    @Property(int, notify=onRobotChanged)
    def robotColIdx(self):
        return self._state.RobotPos.Col

    @Property(int, notify=onRobotChanged)
    def robotRowIdx(self):
        return self._state.RobotPos.Row

    @Slot(result=int)
    def getLineCount(self):
        return len(self._line_mas)

    @Slot(int, result=str)
    def getLineName(self, idx):
        c_line = self._line_mas[idx]

        v_pref = f'{emb.line_type_to_str(c_line.Type)}: '
        if c_line.Size.UserControl:
            v_pref = v_pref + '*'

        return v_pref + c_line.Name

    @Property(int, notify=onLineParamChanged)
    def threadCount(self):
        return self._line_mas[self._line_idx].Size.ThreadCount

    @Slot(int)
    def setLineThreadIdx(self, i_thread_idx):
        self._select_thread(i_thread_idx)

    @Property(int, notify=onLineThreadIdxChanged)
    def lineThreadIdx(self):
        return self._line_thread_idx

    @Slot(int)
    def setLineIdx(self, idx):
        self._select_line(idx)

    @Slot()
    def restart(self):
        self._state.restart()
        # noinspection PyUnresolvedReferences
        self.onRobotChanged.emit()

    @Slot(int)
    def robotSetPosition(self, i_cell_idx):
        self._state.robot_set_pos(i_cell_idx)
        # noinspection PyUnresolvedReferences
        self.onRobotChanged.emit()

    @Slot(int, result=bool)
    def changeWork(self, i_cell_idx):
        vl_line = self._line_mas[self._line_idx]
        v_data = self._cell_data_get(vl_line.UserData, i_cell_idx)
        if v_data is None:
            return False

        v_array, c_idx = v_data
        v_array[c_idx] = 1 - v_array[c_idx]
        return True


# ==============================
@dataclass
class CStateResult:
    Reward: bool
    Punishment: bool


# ==============================
class CControl:
    _qml_data: PyControlData

    def __init__(self, i_param, i_caption):
        self._caption = i_caption

        self.Param: emb.TFullLineParam = i_param

        v_line_mas = []
        for it_state in emb.LineType:
            v_typed_line_mas = [
                LineWorkParam(
                    it_state, it_line_name,
                    copy.deepcopy(it_line_param),  # it_line_param is mutable data link
                    it_line_param.data_create(),
                    it_line_param.data_create()
                )
                for it_line_name, it_line_param
                in self.Param[it_state].items()
            ]
            v_line_mas.extend(v_typed_line_mas)

        if QCoreApplication.instance():
            self._QmlEngine = QQmlEngine()
            self._qml_data = PyControlData(v_line_mas, self._caption)
            self._QmlEngine.rootContext().setContextProperty(
                "applicationData", self._qml_data)

            v_dir = op.dirname(__file__)
            self._QmlEngine.setBaseUrl(QUrl("file:///" + v_dir+"/"))

            self._component = QQmlComponent(self._QmlEngine)
            self._component.loadUrl(QUrl("Control.qml"))
            if self._component.status() != QQmlComponent.Ready:
                self._component = None

    def get_component(self):
        return self._component

    def get_caption(self):
        return self._caption

    def fill_state(self, i_full_line_state: emb.TFullLineState):
        self._qml_data.fill_state(i_full_line_state)
