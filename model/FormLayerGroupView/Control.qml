import QtQuick.Controls
import QtQuick.Layouts
import QtQuick

Item {
    id: rootCmpt
    property string blockCaption : "Group "+ applicationData.textCaption
    property bool collapsed: false
    anchors.fill: parent

    property font logTextFont: applicationData.getLogFont()

    ListModel {
        id: cbItems
    }

    Page {
        anchors.fill: parent

        header: ToolBar {
            implicitHeight: cbIsLog.height

            RowLayout
            {
                anchors.fill: parent
                Label {
                    id: simpleText
                    leftPadding: 5
                    Layout.fillWidth: true
                    elide: Label.ElideRight
                    text: blockCaption
                    verticalAlignment: Text.AlignVCenter
                    MouseArea {
                        id: collapseButton
                        anchors.fill: parent
                        onClicked: collapsed = !collapsed
                    }
                }
                CheckBox {
                    id: cbIsLog
                    text: qsTr("L|C")
                    checkState: Qt.Checked
                    checked: true
                }
            }

        }

        StackLayout {
            anchors.fill: parent
            currentIndex: cbIsLog.checked ? 1 : 0

            SplitView {
                id: splitCustom
                orientation: Qt.Vertical

                handle: Rectangle {
                     implicitWidth: 4
                     implicitHeight: 4

                     color: SplitHandle.pressed ? splitCustom.palette.mid
                         : (SplitHandle.hovered ? splitCustom.palette.light : splitCustom.palette.midlight)

                }

                ColumnLayout {
                    width: parent.width
                    SplitView.preferredHeight: parent.height*0.3

                    Label {text: "Status:"}
                    Flickable {
                        //height: simpleText.height
                        height: parent.height*0.2
                        Layout.fillHeight: true
                        Layout.fillWidth: true

                        clip: true
                        TextArea.flickable: TextArea {
                            font: logTextFont
                            readOnly: true
                            //selectByMouse: true
                            textFormat: TextEdit.PlainText
                            wrapMode: TextEdit.NoWrap
                            text: applicationData.statusText
                        }
                        ScrollBar.vertical: ScrollBar {}
                        ScrollBar.horizontal: ScrollBar {}
                    }
                }

                SplitView {
                    id: splitList
                    orientation: Qt.Horizontal
                    SplitView.fillWidth: true

                    handle: Rectangle {
                         implicitWidth: 4
                         implicitHeight: 4

                         color: SplitHandle.pressed ? splitCustom.palette.mid
                             : (SplitHandle.hovered ? splitCustom.palette.light : splitCustom.palette.midlight)

                    }

                    Page {
                        SplitView.fillHeight: true
                        SplitView.preferredWidth: lbCells.width

                        header: Label {id: lbCells; text: "Cells:"}

                        ListView {
                            id: listFullLab
                            anchors.fill: parent
                            model: cbItems
                            keyNavigationEnabled: true
                            focus: true

                            delegate: Label {
                                text: modelData
                                width: ListView.view.width

                                MouseArea {
                                    anchors.fill: parent
                                    onClicked: {
                                        listFullLab.currentIndex = index
                                        listFullLab.forceActiveFocus();
                                    }
                                }
                            }

                            highlight: Rectangle { color: "lightsteelblue"; radius: 5 }

                            onCurrentIndexChanged: {
                                applicationData.setCellIdx(currentIndex);
                                listLabView.currentIndex = currentIndex;
                            }
                        }
                    }

                    Page {
                        SplitView.fillHeight: true

                        header: ColumnLayout
                        {
                            GridLayout {
                                id: lineSelector

                                columns: 2
                                columnSpacing: 2
                                rowSpacing: 2

                                Label {text: "Cell:"}
                                ComboBox {
                                    id: listLabView
                                    Layout.fillWidth: true

                                    model: cbItems

                                    delegate: ItemDelegate {
                                        //topPadding: 0
                                        //bottomPadding: 0
                                        //width: listLabView.width
                                        contentItem: Label {
                                            text: modelData
                                            //color: "Black"
                                            font: listLabView.font
                                            elide: Text.ElideRight
                                            verticalAlignment: Text.AlignVCenter
                                        }
                                        highlighted: listLabView.highlightedIndex === index
                                    }

                                    contentItem: Label {
                                        leftPadding: 10
                                        rightPadding: listLabView.indicator.width + listLabView.spacing

                                        text: listLabView.displayText
                                        font: listLabView.font
                                        //color: "Black"//listLabView.pressed ? "#17a81a" : "#21be2b"
                                        verticalAlignment: Text.AlignVCenter
                                        elide: Text.ElideRight
                                    }

                                    onActivated: function(index) {
                                        applicationData.setCellIdx(index);
                                        listFullLab.currentIndex = index;
                                    }

                                    Component.onCompleted: refreshCellMas()
                                }

                                Label {text: "Actions"}
                                CheckBox {
                                    id: cbActionsVisible
                                    checked: false
                                }
                            }

                            RowLayout
                            {
                                id: plActions
                                //SplitView.preferredHeight: plNameChange.height
                                visible: cbActionsVisible.checked

                                //id: plNameChange
                                width: parent.width
                                Label {text: "New cell name: "}
                                TextEdit {
                                    id: edNewCellName
                                    Layout.fillWidth: true
                                }
                                Button {
                                    text: "Change"
                                    onClicked: {
                                        applicationData.setCellName(listLabView.currentIndex, edNewCellName.text)
                                        edNewCellName.text = ''
                                    }
                                }
                            }
                        }

                        Flickable {
                            id: flicArea
                            anchors.fill: parent

                            clip: true
                            TextArea.flickable: TextArea {
                                font: logTextFont
                                readOnly: true
                                selectByMouse: true
                                textFormat: TextEdit.PlainText
                                text: applicationData.cellText
                                wrapMode: TextEdit.NoWrap
                            }

                            ScrollBar.vertical: ScrollBar {}
                            ScrollBar.horizontal: ScrollBar {}
                        }
                    }
                }
            }

            ItemTextView {
                id: logHistory
                SplitView.preferredWidth: parent.width*0.2
                textFont: logTextFont
                contentText: "" //applicationData.textInput
            }
        }
    }

    Component.onCompleted: {
        console.log("Control_LineView Completed! ["+blockCaption+"]")

        applicationData.setRoot(this)
    }

    Component.onDestruction: {
        console.log("Control_LineView Destruction! ["+blockCaption+"]")

        applicationData.setRoot(null)
    }

    Connections {
        target: applicationData

        function onCellMasChanged()
        {
            refreshCellMas()
        }
    }

    function setLogTextContent(iText)
    {
        logHistory.contentText = iText
    }

    function refreshCellMas()
    {
        cbItems.clear()
        var vCount = applicationData.getCellCount();
        for(var i=0; i<vCount; i++)
        {
            var vItem = applicationData.getCellName(i);
            cbItems.append({text: vItem});
        }
        listLabView.currentIndex = 0
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
