import typing
import sys
import os.path as op

from PySide6.QtGui import QFont
from PySide6.QtQuick import QQuickItem
from PySide6.QtQml import QQmlComponent, QQmlEngine
from PySide6.QtCore import QObject, Slot, Property, Signal, QUrl, QCoreApplication  # , QTimer

if __name__ == '__main__':
    cDir = op.dirname(__file__)
    vRootPath = op.join(cDir, '../../../')
    vRootPath = op.abspath(vRootPath)
    sys.path.append(vRootPath)

import model.emb as emb

from model.layercore import cell
from model.layercore.group import CCellGroup


# ==============================
class PyControlData(QObject):
    onCaptionChanged = Signal()
    onCellTextChanged = Signal()
    onStatusChanged = Signal()
    cellMasChanged = Signal()
    _group: CCellGroup
    _cell_mas: typing.List[cell.CAnalyseCell]

    def __init__(self,
                 i_group: CCellGroup,
                 i_caption: str
                 ):
        QObject.__init__(self)

        self._group = i_group

        self._Root = None

        self._cell_mas = [it for it in self._group.CellMas.values()]
        self._cell_idx = 0
        self._cell_text = ''

        self._caption = i_caption
        self._status_text = ''

        # self._timer = QTimer()
        # self._timer.timeout.connect(self._update_robot)
        # self._timer.start(1000)   # ms

    @Slot(QQuickItem)
    def setRoot(self, i_root):
        self._Root = i_root

    def fill_log(self):
        v_current_group_text = str(self._group)
        self._status_text = self._group.get_status()

        # noinspection PyUnresolvedReferences
        self.onStatusChanged.emit()

        if self._Root:
            self._Root.setLogTextContent(v_current_group_text)

        # noinspection PyUnresolvedReferences
        self.onCellTextChanged.emit()

    @Property(str, notify=onCaptionChanged)
    def textCaption(self):
        return self._caption

    @Slot(result=QFont)
    def getLogFont(self):
        return emb.get_log_font()

    @Property(str, notify=onStatusChanged)
    def statusText(self):
        return self._status_text

    @Slot(result=int)
    def getCellCount(self):
        return len(self._cell_mas)

    @Slot(int, result=str)
    def getCellName(self, idx):
        v_caption = self._cell_mas[idx].get_caption()
        if v_caption:
            return f'{idx} {v_caption}'
        else:
            return f'{idx}'

    @Slot(int, str)
    def setCellName(self, idx, i_caption):
        self._cell_mas[idx].set_caption(i_caption)

        # noinspection PyUnresolvedReferences
        self.cellMasChanged.emit()
        # noinspection PyUnresolvedReferences
        self.onCellTextChanged.emit()

    @Slot(int)
    def setCellIdx(self, idx):
        self._cell_idx = idx

        # noinspection PyUnresolvedReferences
        self.onCellTextChanged.emit()

    @Property(str, notify=onCellTextChanged)
    def cellText(self):
        if self._cell_idx < len(self._group.CellMas):
            return '\n'.join([
                'C{{{}}} {}'.format(
                    self._group.get_cell_info(self._cell_idx),
                    self._group.CellMas[self._cell_idx].header_str()
                ),
                emb.verbose_string_to_text(
                    self._group.CellMas[self._cell_idx].transfer_verbose_string(self._group, [self._cell_idx])
                )
            ])
        else:
            return ''


# ==============================
class CControl:
    _qml_data: PyControlData
    Group: CCellGroup

    def __init__(self, i_group: CCellGroup, i_caption):
        self._caption = i_caption

        if QCoreApplication.instance():
            self._QmlEngine = QQmlEngine()
            self._qml_data = PyControlData(i_group, self._caption)
            self._QmlEngine.rootContext().setContextProperty(
                "applicationData", self._qml_data)

            v_dir = op.dirname(__file__)
            self._QmlEngine.setBaseUrl(QUrl("file:///" + v_dir+"/"))

            self._component = QQmlComponent(self._QmlEngine)
            self._component.loadUrl(QUrl("Control.qml"))
            if self._component.status() != QQmlComponent.Ready:
                self._component = None

    def get_component(self):
        return self._component

    def get_caption(self):
        return self._caption

    def fill_log_text(self):
        if self._qml_data:
            self._qml_data.fill_log()
