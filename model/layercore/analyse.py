from __future__ import annotations

import sys
import glob
import os.path as op
import typing
import unittest
import xml.etree.ElementTree as xmlET
from dataclasses import dataclass

from model.layercore.state import CState
from model.layercore.group import CCellGroup
from model.layercore.cell import CCellParam
from model.layercore.transfer import CTransferParam


# ==============================
class CAnalyse:

    @dataclass
    class SettingT:

        @dataclass
        class StateT:
            Caption: str  # xml.attr 'Caption'
            Dir: str  # xml dir

            def state_list(self):
                return sorted(glob.glob(op.join(self.Dir, self.Caption + '*.state')))

        @dataclass
        class CellT:
            Level: int  # xml.attr v_cell->'Level'
            Strong: int  # xml.attr v_cell->'Strong'

        @dataclass
        class LinkT:
            Level: int  # xml.attr v_link->'Level'
            Utility: int  # xml.attr v_link->'Utility'

        @dataclass
        class ActionT:
            Level: int  # xml.attr v_action->'Level'
            Strong: int  # xml.attr v_action->'Strong'

        @dataclass
        class LearnT:
            MaxLength: int  # xml.attr v_learn->'MaxLength'
            WaitRewardLevel: int  # xml.attr v_learn->'WaitRewardLevel' | c_action_level * 9

        State: StateT

        Cell: CellT
        Link: LinkT
        Action: ActionT
        Learn: LearnT

        # i_params.Analyse['InnerGroupCount'] # xml.attr 'InnerGroupCount'
        InnerGroupCount: int
        # i_params.Analyse['CellCount'] # xml.attr 'Count'
        CellCount: int
        NoneActionLevel: int

    @dataclass
    class InOutSettingT:
        # emb.CLineSize.get_size() = math.prod(self.ArrayShape) * self.ThreadCount
        # for ItemLines[emb.LineType.Input]['']
        InCount: int
        # self._OutputCount # sum itCount.ThreadCount for ItemLines[emb.LineType.Output].items()
        ActionCount: int

    GroupMas: typing.List[CCellGroup]
    Param_InCount: int
    Param_Link: SettingT.LinkT
    Param_Cell: SettingT.CellT

    def __init__(self, i_param: SettingT, i_in_out_param: InOutSettingT):
        self.Param_InCount = i_in_out_param.InCount
        self.Param_Link = i_param.Link
        self.Param_Cell = i_param.Cell

        c_link_strong_range = 8  # TODO: i_param['Link']['Strong']
        c_link_utility = i_param.Link.Utility
        c_inner_group_count = i_param.InnerGroupCount

        self.GroupMas = []
        v_size = 2 + c_inner_group_count

        self.GroupMas.append(CCellGroup(
            i_caption=self.group_idx_to_caption(len(self.GroupMas), v_size),
            i_max_chain_length=0,
            i_wait_reward_level=0,
            i_none_action_level=0,
            i_cell_count=self.Param_InCount,
            i_cell_param=CCellParam(
                CompetitionTime=1,
                RelaxTime=0,
                StrongRange=1
            ),
            i_transfer_param=CTransferParam(
                Level=1,
                ActivationAndInhibition=c_link_utility,
                Strong=c_link_strong_range
            )
        ))

        for _ in range(c_inner_group_count):
            self.GroupMas.append(CCellGroup(
                i_caption=self.group_idx_to_caption(len(self.GroupMas), v_size),
                i_max_chain_length=i_param.Learn.MaxLength,
                i_wait_reward_level=0,
                i_none_action_level=i_param.NoneActionLevel,
                i_cell_count=i_param.CellCount,
                i_cell_param=CCellParam(
                    CompetitionTime=i_param.Cell.Level,
                    RelaxTime=0,
                    StrongRange=i_param.Cell.Strong
                ),
                i_transfer_param=CTransferParam(
                    Level=0,  # TODO: введен запрет обучения
                    ActivationAndInhibition=c_link_utility,
                    Strong=c_link_strong_range
                )
            ))

        self.GroupMas.append(CCellGroup(
            i_caption=self.group_idx_to_caption(len(self.GroupMas), v_size),
            i_max_chain_length=1,
            i_wait_reward_level=i_param.Learn.WaitRewardLevel,
            i_none_action_level=-1 if (c_inner_group_count <= 0) else 0,
            i_cell_count=i_in_out_param.ActionCount,
            i_cell_param=CCellParam(
                CompetitionTime=2,
                RelaxTime=i_param.Action.Level,
                StrongRange=i_param.Action.Strong
            ),
            i_transfer_param=CTransferParam(
                Level=i_param.Link.Level,
                ActivationAndInhibition=c_link_utility,
                Strong=c_link_strong_range
            )
        ))

        v_state_list = i_param.State.state_list()
        if v_state_list:
            print('\r\n'.join(v_state_list))
            self.load(v_state_list[0])

    # ==============================
    @staticmethod
    def group_idx_to_caption(i_idx: int, i_size: int):
        assert 1 < i_size
        assert 0 <= i_idx < i_size
        if i_idx == 0:
            return 'Input'
        elif i_idx == i_size - 1:
            return 'Output'
        else:
            if i_size == 3:
                return 'Inner'
            else:
                return 'Inner' + str(i_idx)

    # ==============================
    def load(self, i_state_filename):
        if i_state_filename is None:
            return

        v_xml_tree = xmlET.parse(i_state_filename)

        v_wn = v_xml_tree.getroot()
        v_version = v_wn.attrib['Version']
        c_current_version = '1.0'
        if v_version != c_current_version:
            raise Exception('Bad version')

        v_groups = v_wn.find('GroupMas')

        v_groups_state = {}
        for s in v_groups.findall('Group'):
            v_group_name = s.attrib['Caption']
            v_groups_state[v_group_name] = s

        for it_idx, it_item in enumerate(self.GroupMas):
            v_group_name = self.group_idx_to_caption(it_idx, len(self.GroupMas))
            v_group_state = None
            if v_group_name in v_groups_state:
                v_group_state = v_groups_state[v_group_name]

            it_item.load(v_group_state)

        print(f'Load state from {i_state_filename}')

    def save(self, i_state_filename: str):
        v_wn = xmlET.Element('LayerState')
        c_current_version = '1.0'
        v_wn.attrib['Version'] = c_current_version

        v_groups = xmlET.Element('GroupMas')
        v_wn.append(v_groups)

        for it_idx, it_item in enumerate(self.GroupMas):
            v_group_name = self.group_idx_to_caption(it_idx, len(self.GroupMas))

            v_group = xmlET.Element('Group')
            v_group.attrib['Caption'] = v_group_name
            v_groups.append(v_group)

            it_item.save(v_group)

        doc = xmlET.ElementTree(v_wn)
        if sys.version_info >= (3, 9):
            xmlET.indent(doc, space="\t", level=0)
            doc.write(i_state_filename, encoding='utf-8', xml_declaration=True)
        else:
            from xml.dom import minidom
            xml_str = minidom.parseString(xmlET.tostring(v_wn)).toprettyxml()
            with open(i_state_filename, "w") as f:
                f.write(xml_str)

        print(f'Save state to {i_state_filename}')

    # ==============================
    def step(self, i_input_state, i_reward, i_stick):

        for it_idx, it_item in enumerate(self.GroupMas):
            it_item.step()

        for it_idx, it_item in enumerate(self.GroupMas):
            it_item.reward(i_reward)
            it_item.stick(i_stick)

        self.GroupMas[0].set_state(i_input_state)

        for it_cur, it_next in zip(self.GroupMas, self.GroupMas[1:]):
            it_next.dispatch_input(it_cur)

        return self.GroupMas[-1].get_output()

    # ==============================
    def create_new_input(self):
        return CState(self.Param_InCount, 1)

    # ==============================
    def __repr__(self):
        return '\r\n'.join([str(it) for it in self.GroupMas])


# ==============================
class TestAnalyse(unittest.TestCase):
    def test(self):
        v_a = CAnalyse(
            CAnalyse.SettingT(
                CAnalyse.SettingT.StateT('', ''),
                CAnalyse.SettingT.CellT(Level=10, Strong=8),
                CAnalyse.SettingT.LinkT(Level=15, Utility=10),
                CAnalyse.SettingT.ActionT(Level=10, Strong=8),
                CAnalyse.SettingT.LearnT(MaxLength=4, WaitRewardLevel=10*9),
                InnerGroupCount=1,
                CellCount=9,
                NoneActionLevel=0,
            ),
            CAnalyse.InOutSettingT(
                InCount=10,
                ActionCount=3
            )
        )
        # v_a.load(v_state)

        # noinspection PyProtectedMember
        def step(i_input=None):
            if i_input is not None:
                print(i_input)
                v_a.step(i_input, None, None)

            print(v_a)

        step()
        v_input = v_a.create_new_input()
        step(v_input)
        v_input.Mas[1].set_max()
        v_input.Mas[2].set_max()
        step(v_input)
