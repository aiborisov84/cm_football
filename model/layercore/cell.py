from __future__ import annotations
import typing
from dataclasses import dataclass
import unittest
import xml.etree.ElementTree as xmlET

from model.layercore import transfer
from model.layercore import group
from model import xml_utils
from model.layercore.state import CRangeValue


# ==============================
@dataclass
class CCellParam:
    CompetitionTime: int
    RelaxTime: int
    StrongRange: int


# ==============================
class CCellMemState:
    Level: int
    CONST_RepeatBegin = -1
    CONST_RepeatEnd = -2

    def __init__(self, i_level: int):
        self.Level = i_level

    def __repr__(self):
        if self.Level == CCellMemState.CONST_RepeatBegin:
            return "RB"
        elif self.Level == CCellMemState.CONST_RepeatEnd:
            return "RE"
        else:
            return str(self.Level)

    def __eq__(self, other):
        return self.Level == other.Level


# ==============================
class CCell:
    # время соревнования внутри слоя (CompetitionTime)
    _CompetitionTime: CRangeValue
    # время отдыха (RelaxTime)
    _RelaxTime: CRangeValue
    # Количество повторов
    _RepeatCount: CRangeValue
    # количество использований ячейки TODO: спорный параметр
    # используется только для выбора последовательности первого использования ячейки
    Strong: CRangeValue
    Active: bool

    def __init__(self, i_param: CCellParam):
        self._CompetitionTime = CRangeValue(i_param.CompetitionTime)
        # TODO: отдельная настройка на повторение
        self._RepeatCount = CRangeValue(i_param.CompetitionTime)
        self._RelaxTime = CRangeValue(i_param.RelaxTime)
        self.Strong = CRangeValue(i_param.StrongRange)
        self.Active = False

    def __repr__(self):
        v_state = '[{} T:{} R:{} S:{}{}]'.format(
            '#' if self.Active else ' ',
            self._CompetitionTime,
            self._RelaxTime,
            self.Strong,
            f' Repeat{self._RepeatCount}' if self._RepeatCount.is_set() else ''
        )

        return v_state

    def load(self, i_state):
        if i_state is None:
            return

        self._CompetitionTime.Value = i_state[0]
        self._RelaxTime.Value = i_state[1]
        self.Strong.Value = i_state[2]
        self.Active = i_state[3]

    def work__set(self, i_is_active):
        if self.Active:
            self._RepeatCount.inc()
        else:
            self.Active = i_is_active
            self.Strong.inc()

        self._CompetitionTime.set_max()

    def work__transfer(self):
        if self._RepeatCount.is_max_near():
            self._RepeatCount.inc()
        else:
            if self._CompetitionTime.is_min_next():
                self._RepeatCount.reset()
            self.Active = False

    def work__is_run(self):
        return self._CompetitionTime.is_set() or self._RelaxTime.is_set()

    def work__is_current(self):
        return self._CompetitionTime.is_max()

    def work__can_repeat(self):
        return self._CompetitionTime.is_max_near()

    def work__is_ready(self):
        return self._CompetitionTime.is_min_next() or self._RepeatCount.is_max_near()

    def strong__inc(self):
        self.Strong.inc()

    def time__step(self):
        if self._CompetitionTime.is_min_next():
            self._RelaxTime.set_max()

        self._CompetitionTime.dec()
        self._RelaxTime.dec()

        if not self._CompetitionTime.is_set() and not self._RelaxTime.is_set():
            self._RepeatCount.reset()
            self.Active = False

    def is_active(self):
        return self.Active

    def is_repeat(self):
        return self._RepeatCount.is_set()

    def competition__get_time(self) -> CCellMemState:
        if self._RepeatCount.is_max_near() and self._CompetitionTime.is_max():
            return CCellMemState(CCellMemState.CONST_RepeatBegin)
        if self._RepeatCount.is_max() and self._CompetitionTime.is_min_next():
            return CCellMemState(CCellMemState.CONST_RepeatEnd)
        return CCellMemState(self._CompetitionTime.Value)


# ==============================
class CAnalyseCell(CCell):
    _TransferList: typing.List[transfer.CTransfer]
    _Competition: typing.Optional[int]
    _CompetitionLevel: int
    _Caption: str

    def __init__(self, i_param: CCellParam):
        super().__init__(i_param)
        self._TransferList = []
        self._Competition = None
        self._CompetitionLevel = 0
        self._Caption = ''

    def header_str(self):
        v_competition_str = ''
        if self._Competition is not None:
            v_competition_str = f' Comp({self._Competition})'

        return super().__repr__() + v_competition_str

    def __repr__(self):
        v_result = [self.header_str()]

        for it in self._TransferList:
            v_result.append(str(it))

        return '\r\n'.join(v_result)

    def transfer_verbose_string(self, i_cell_group: group.CCellGroup, i_done_cell_idx: typing.List[int]):
        v_result = []

        for it in self._TransferList:
            v_result.append(it.to_verbose_string(i_cell_group, i_done_cell_idx))

        if v_result:
            return v_result
        else:
            return 'No transfer'

    def step(self):
        self.time__step()

        for it in self._TransferList:
            it.step()

    def reward(self, i_reward: typing.Optional[bool]):
        if i_reward is None:
            return

        if not i_reward:
            return

        for it in self._TransferList:
            it.reward()

    def stick(self, i_stick: typing.Optional[bool]):
        if i_stick is None:
            return

        if not i_stick:
            return

        for it in self._TransferList:
            it.stick()

    def get_mem_active_range(self):
        if not self._TransferList:
            return 0

        return max(it.get_active_range() for it in self._TransferList)

    def add_transfer(self, i_transfer: transfer.CTransfer) -> bool:
        # если ячейка задавлена - необходимо выбрать другую для активации
        if self._Competition is not None:
            if self._Competition <= self._CompetitionLevel:
                return False

        for it in self._TransferList:
            if it.is_same(i_transfer):
                return False

        self._TransferList.append(i_transfer)
        return True

    def competition_run(
            self,
            i_input_cell_group: group.CCellGroup,
            i_inner_cell_group: group.CCellGroup) -> typing.List[transfer.CTransfer]:

        v_is_work = False
        v_result = []
        v_competition_sum = 0

        for it in self._TransferList:
            # Проверка применимости
            if not it.catch_state(i_input_cell_group, i_inner_cell_group):
                continue

            v_competition_voice = it.get_competition_voice()
            # todo: condition transfer with check double link in creation
            if v_competition_voice > 0:
                v_result.append(it)

            v_competition_sum += v_competition_voice

            v_is_work = True

        if v_is_work:
            self._Competition = v_competition_sum

        return v_result

    def competition_reset(self):
        self._Competition = None
        self._CompetitionLevel = 0

        for it in self._TransferList:
            it.work_prepare()

    def competition_set_level(self, i_competition_level):
        self._CompetitionLevel = i_competition_level

    def competition_get_result(self) -> bool:
        if self._Competition is None:
            return False

        if self._Competition <= self._CompetitionLevel:
            return False
        else:
            return True

    def transfer_optimization(self):
        if self._CompetitionTime.is_set():
            return

        self._TransferList = [it for it in self._TransferList if not it.is_useless()]

    def load(self, i_xml_state: xmlET.Element):
        if i_xml_state is None:
            return

        self._Caption = xml_utils.attr_get(i_xml_state, 'Caption', '')

        # self.Level.Value = i_state[0]
        # self.Strong.Value = i_state[1]
        # self.Active = i_state[2]

        super().load(None)

    def save(self, i_xml_state: xmlET.Element):
        if self._Caption:
            i_xml_state.attrib['Caption'] = self._Caption

    def get_caption(self):
        if self._Caption:
            return f'«{self._Caption}»'
        else:
            return ''

    def set_caption(self, i_caption):
        self._Caption = i_caption

    def set_transfer_caption(
            self,
            i_input_cell_group: group.CCellGroup,
            i_inner_cell_group: group.CCellGroup
    ):
        for it in self._TransferList:
            it.set_caption(i_input_cell_group, i_inner_cell_group)


# ==============================
class TestCellMethods(unittest.TestCase):
    def test_upper(self):
        v_param = CCellParam(1, 15, 8)
        v = CCell(v_param)
        v.work__set(True)
        v.strong__inc()
        print(v)


# ==============================
if __name__ == '__main__':
    unittest.main()
