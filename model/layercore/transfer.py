from __future__ import annotations
import typing
from dataclasses import dataclass

from . import group, cell
from .state import CRangeValue


# ==============================
class CActiveCellsState:
    # CellIdx -> Caption
    _CellCaptionMap: typing.Dict[int, str]
    # CellIdx -> Level
    CellLevelMap: typing.Dict[int, cell.CCellMemState]
    # Признак сработавшего NoneLevel
    IsNone: bool
    # Количество узлов, формирующих цепочку истории (текущий и предшествующие узлы)
    Range: int

    def is_none(self):
        return not self.CellLevelMap

    def __init__(self, i_state_map: typing.Dict[int, cell.CCellMemState], i_range, i_is_none=False):
        self._CellCaptionMap = {}
        self.CellLevelMap = i_state_map
        self.Range = i_range
        self.IsNone = i_is_none

    def __repr__(self):
        if not self.CellLevelMap and not self.IsNone:
            return ''

        v_str_mas = []
        if self.IsNone:
            v_str_mas.append('NL')

        for it_cell_idx, it_level in self.CellLevelMap.items():
            v_item_caption = ''
            if it_cell_idx in self._CellCaptionMap:
                v_item_caption = self._CellCaptionMap[it_cell_idx]
            v_str_mas.append(f'{it_cell_idx}{v_item_caption}^{it_level}')

        return '{}{{{}}}'.format(
            f'{self.Range}' if self.Range > 1 else '',
            '; '.join(v_str_mas)
        )

    def to_verbose_string(self, i_cell_group: group.CCellGroup, i_done_cell_idx: typing.List[int]):
        if not self.CellLevelMap and not self.IsNone:
            return ''

        v_str_mas = []

        if self.IsNone:
            v_str_mas.append('GroupNoneLevel')

        for it_cell_idx, it_level in self.CellLevelMap.items():
            v_cell = i_cell_group.CellMas[it_cell_idx]
            v_str_mas.append(f'C{{{i_cell_group.get_cell_info(it_cell_idx)}^{it_level}}} '+ v_cell.header_str())
            if it_cell_idx in i_done_cell_idx:
                v_str_mas.append(['==> CycledTransfer'])
            else:
                v_str_mas.append(v_cell.transfer_verbose_string(i_cell_group, [*i_done_cell_idx, it_cell_idx]))

        return v_str_mas

    def is_same(self, i_state: CActiveCellsState):
        return self.CellLevelMap == i_state.CellLevelMap and self.IsNone == i_state.IsNone

    def set_caption(self, i_cell_map: typing.Mapping[int, cell.CAnalyseCell]):
        self._CellCaptionMap = {}
        for it_cell_idx in self.CellLevelMap.keys():
            if it_cell_idx in i_cell_map:
                vl_link_cell = i_cell_map[it_cell_idx]
                self._CellCaptionMap[it_cell_idx] = vl_link_cell.get_caption()


# ==============================
@dataclass
class CTransferParam:
    Level: int
    ActivationAndInhibition: int
    Strong: int

    def is_reward_learnable(self):
        return self.Level > 0


# ==============================
@dataclass
class CTransferState:
    IsNew: bool
    IsCatch: bool
    IsWork: bool


# ==============================
class CTransfer:
    _Input: CActiveCellsState
    _Inner: CActiveCellsState

    _Learn: CRangeValue
    _Time: CRangeValue
    _Activation: CRangeValue
    _Inhibition: CRangeValue
    _State: CTransferState
    _Caption: str

    def __init__(
            self,
            i_input: CActiveCellsState,
            i_inner: CActiveCellsState,
            i_param: CTransferParam):

        self._Input = i_input
        self._Inner = i_inner
        self._Activation = CRangeValue(i_param.ActivationAndInhibition)
        self._Inhibition = CRangeValue(i_param.ActivationAndInhibition)
        if i_param.Level > 0:
            self._Learn = CRangeValue(i_param.Level)
            self._Time = CRangeValue(i_param.Level)
            self._Learn.set_max()
        else:
            self._Learn = CRangeValue(0)
            self._Time = CRangeValue(1)
            self._Activation.inc()

        self._Time.set_max()

        self._Strong = CRangeValue(i_param.Strong)
        self._State = CTransferState(
            IsNew=True,
            IsWork=True,
            IsCatch=True
        )

    def _get_work_done_str(self):
        v_res = ''
        v_res += '*' if self._State.IsCatch else '-'
        v_res += "+" if self._State.IsNew else '-'

        v_competition = self.get_competition_voice()
        if v_competition > 0:
            v_res += '>'
        elif v_competition == 0:
            v_res += '?'
        else:
            v_res += 'x'

        return v_res

    def _header_str(self):
        v_learn_str = ''
        if self.is_learnable():
            v_learn_str = ' A{} I{} L{}'.format(
                self._Activation,
                self._Inhibition,
                self._Learn
            )

        return '{}{} T{} S{}'.format(
            self._get_work_done_str(),
            v_learn_str,
            self._Time,
            self._Strong
        )

    def __repr__(self):
        return '{} (I{} C{})'.format(
            self._header_str(),
            self._Input,
            self._Inner
        )

    def to_verbose_string(self, i_cell_group: group.CCellGroup, i_done_cell_idx: typing.List[int]):
        v_result = [
            self._header_str(),
            [
                f'I{self._Input}',
                *self._Inner.to_verbose_string(i_cell_group, i_done_cell_idx)
            ]
        ]
        return v_result

    def get_competition_voice(self):
        return self._Activation.Value - self._Inhibition.Value

    def step(self):
        self._Learn.dec()
        self._Time.dec()

    def catch_state(
            self,
            i_input_cell_group: group.CCellGroup,
            i_inner_cell_group: group.CCellGroup):

        if self._Inner.IsNone:
            if not i_inner_cell_group.NoneLevel.is_max():
                return False

        for it_cell_idx, it_cell_mem_time in self._Input.CellLevelMap.items():
            c_cell_time = i_input_cell_group.CellMas[it_cell_idx].competition__get_time()
            if c_cell_time != it_cell_mem_time:
                return False

        for it_cell_idx, it_cell_mem_time in self._Inner.CellLevelMap.items():
            c_cell_time = i_inner_cell_group.CellMas[it_cell_idx].competition__get_time()
            if c_cell_time != it_cell_mem_time:
                return False

        self._State.IsCatch = True

        self._Time.set_max()
        return True

    def work_prepare(self):
        self._State.IsWork = False
        self._State.IsCatch = False
        self._State.IsNew = False

    def work_transfer(
            self,
            i_input_cell_group: typing.Optional[group.CCellGroup],
            i_inner_cell_group: group.CCellGroup):

        if i_input_cell_group is not None:
            for it_cell_idx in self._Input.CellLevelMap.keys():
                vl_link_cell = i_input_cell_group.CellMas[it_cell_idx]
                vl_link_cell.work__transfer()
                vl_link_cell.strong__inc()

        for it_cell_idx in self._Inner.CellLevelMap.keys():
            vl_link_cell = i_inner_cell_group.CellMas[it_cell_idx]
            vl_link_cell.work__transfer()
            vl_link_cell.strong__inc()

        if self._Inner.IsNone:
            i_inner_cell_group.NoneLevel.reset()


        # TODO: Убрал автоматическую полезность новых
        # if self._State.IsNew:
        #    self._Activation.inc()

        # TODO: Убрал возобновление обучения
        # self._Level.set_max()
        self._State.IsWork = True
        self._Strong.inc()

    def get_active_range(self):
        return self._Inner.Range

    def reward(self):
        if self._Learn.is_set():
            self._Activation.inc()

    def stick(self):
        if self._Learn.is_set():
            self._Inhibition.inc()

    def set_caption(
            self,
            i_input_cell_group: group.CCellGroup,
            i_inner_cell_group: group.CCellGroup
    ):
        self._Input.set_caption(i_input_cell_group.CellMas)
        self._Inner.set_caption(i_inner_cell_group.CellMas)

    def is_learnable(self):
        return self._Learn.is_init()

    def is_useless(self):
        if self.get_competition_voice() != 0:
            return False

        if not self.is_learnable():
            return False

        if self._Learn.is_set():
            return False

        return True

    def is_same(self, i_transfer: CTransfer):
        return (
                self._Inner.is_same(i_transfer._Inner) and
                self._Input.is_same(i_transfer._Input)
        )

    def is_same_input(self, i_input: CActiveCellsState):
        return self._Input.is_same(i_input)

    def get_inner_range(self):
        return self._Inner.Range

