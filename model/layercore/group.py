from __future__ import annotations
import xml.etree.ElementTree as xmlET

import copy
import typing

from model.layercore import cell
from model.layercore import transfer
from model import xml_utils
from model import emb
from model.layercore.state import CState, CRangeValue


# ==============================
# TODO: View group state filtered by [active, selected] cells

# ==============================
class CCellChain:
    _CellIdxMas: typing.List[int]

    def __init__(self, i_max_series_length):
        self.MaxSeriesLength = i_max_series_length
        self._CellIdxMas = []

    def load(self, i_state):
        if i_state is None:
            self._CellIdxMas = []
        else:
            self._CellIdxMas = i_state

    def get_last_cell_idx(self):
        if len(self._CellIdxMas) == 0:
            return None
        else:
            return self._CellIdxMas[-1]

    def reset(self):
        self._CellIdxMas = []

    def add_cell_idx(self, i_cell_idx):
        if len(self._CellIdxMas) < self.MaxSeriesLength:
            self._CellIdxMas.append(i_cell_idx)

    def is_can_learn(self):
        return len(self._CellIdxMas) < self.MaxSeriesLength

    def is_none(self):
        return len(self._CellIdxMas) == 0

    def is_contains(self, i_cell_idx):
        return i_cell_idx in self._CellIdxMas


# ==============================
class CCellGroup:
    CellMas: typing.Mapping[int, cell.CAnalyseCell]
    CurrentChain: CCellChain
    # действия на долгое отсутствие входного сигнала
    NoneLevel: typing.Optional[CRangeValue]
    # действия на долгое пребывание группы без поощрения
    WaitRewardLevel: CRangeValue
    _Caption: str
    _TransferParam: transfer.CTransferParam
    _InitCurrentCellIdx: typing.Optional[int]
    _StickInLastStep: bool
    _RewardInLastStep: bool

    def __init__(
            self,
            i_caption: str,
            i_max_chain_length: int,
            i_wait_reward_level: int,
            i_none_action_level: int,
            i_cell_count: int,
            i_cell_param: cell.CCellParam,
            i_transfer_param: transfer.CTransferParam):

        self._TransferParam = i_transfer_param
        self._Caption = i_caption
        self._StickInLastStep = False
        self._RewardInLastStep = False
        self.CellMas = {
            it: cell.CAnalyseCell(i_cell_param)
            for it in range(i_cell_count)
        }
        self.WaitRewardLevel = CRangeValue(i_wait_reward_level)

        self._InitCurrentCellIdx = None
        self.CurrentChain = CCellChain(
            i_max_series_length=i_max_chain_length
        )

        if i_none_action_level < 0:
            self.NoneLevel = CRangeValue(2 * (i_cell_param.CompetitionTime + i_cell_param.RelaxTime))
        elif i_none_action_level == 0:
            self.NoneLevel = None
        else:
            self.NoneLevel = CRangeValue(i_none_action_level)

    # ==============================
    def _get_active_cell_idx(self) -> typing.Optional[int]:
        v_result_cell_idx = self.CurrentChain.get_last_cell_idx()
        if v_result_cell_idx is None:
            v_result_cell_idx = self._InitCurrentCellIdx

        return v_result_cell_idx

    # ==============================
    def _get_min_strong_cell_idx(self) -> typing.Optional[int]:
        """Получение индекса самой слабой ячейки"""
        v_result = [None]
        for it, itCell in self.CellMas.items():
            v_strong = itCell.Strong.Value
            if v_result[0] is None or v_result[0] > v_strong:
                v_result = [v_strong, it]

        return v_result[1] if v_result[0] is not None else None

    # ==============================
    def _get_rest_active(self) -> transfer.CActiveCellsState:
        v_result = {}
        v_result_range = 1

        for it_cell_idx, it_cell in self.CellMas.items():
            if not it_cell.work__is_ready():
                continue

            if not it_cell.is_active():
                continue

            v_result[it_cell_idx] = it_cell.competition__get_time()
            v_result_range = max(
                v_result_range,
                it_cell.get_mem_active_range() + 1
            )

        return transfer.CActiveCellsState(
            v_result,
            v_result_range,
            self.NoneLevel.is_max() if self.NoneLevel else False
        )

    # ==============================
    def _get_chain_active(self) -> transfer.CActiveCellsState:
        """Получает массив индексов и уровней текущих активных ячеек и
        глубину предшествующей истории
        """
        v_result = {}
        v_result_range = 1
        c_last_cell_idx = self._get_active_cell_idx()

        def try_add_cell(i_cell_idx: int):
            v_cell = self.CellMas[i_cell_idx]

            if not v_cell.work__is_run():
                return
            if not v_cell.is_active():
                return
            if not v_cell.is_repeat():
                if v_cell.work__is_current():
                    return

            nonlocal v_result
            nonlocal v_result_range

            v_result[i_cell_idx] = v_cell.competition__get_time()
            v_result_range = max(
                v_result_range,
                v_cell.get_mem_active_range() + 1
            )

        # Добавляем в опору все ячейки максимального уровня (сработавшие на предыдущем шаге)
        # Необходимо заменить это на добавление ячеек, сработавших последними в своих цепочках
        # (отвязаться от требования на предыдущий шаг)
        for it_cell_idx in self.CellMas:
            if c_last_cell_idx == it_cell_idx:
                continue

            try_add_cell(it_cell_idx)

        # последней в опору добавляем текущую ячейку
        if c_last_cell_idx is not None:
            try_add_cell(c_last_cell_idx)

        return transfer.CActiveCellsState(
            v_result,
            v_result_range,
            self.NoneLevel.is_max() if self.NoneLevel else False
        )

    # ==============================
    def set_state(self, i_input_state: CState):
        assert(i_input_state.get_size() == len(self.CellMas))

        for it_cell_idx, it_cell in self.CellMas.items():
            if i_input_state.is_set_item(it_cell_idx):
                it_cell.work__set(True)

    # ==============================
    def step(self):
        if self._TransferParam.is_reward_learnable():
            self.WaitRewardLevel.inc()
        self._StickInLastStep = False
        self._RewardInLastStep = False

        if self.NoneLevel:
            self.NoneLevel.inc()

        for itCell in self.CellMas.values():
            itCell.step()

            if self.NoneLevel:
                if itCell.is_active() and not itCell.is_repeat():
                    self.NoneLevel.reset()

        if self._InitCurrentCellIdx is not None:
            vl_active_cell = self.CellMas[self._InitCurrentCellIdx]
            if not vl_active_cell.work__is_run():
                self._InitCurrentCellIdx = None

        c_last_cell = self.CurrentChain.get_last_cell_idx()
        if c_last_cell is not None:
            vl_active_cell = self.CellMas[c_last_cell]
            if not vl_active_cell.work__is_run():
                self.CurrentChain.reset()

    def get_cell_info(self, i_cell_idx):
        v_cell = self.CellMas[i_cell_idx]
        return '{}{}{}'.format(
            i_cell_idx,
            ('#' if v_cell.work__is_current() else ' ') +
            ('+' if i_cell_idx == self.CurrentChain.get_last_cell_idx() else ' ') +
            ('*' if i_cell_idx == self._InitCurrentCellIdx else ' '),
            v_cell.get_caption()
        )

    # ==============================
    def get_status(self):
        v_status_mas = [
            f'InnerActive: {self._get_chain_active()}',
            f'OutRestActive: {self._get_rest_active()}'
        ]
        if self.CurrentChain.MaxSeriesLength > 0:
            v_learn_param = emb.data_to_text([
                    f'MaxSeriesLength={self.CurrentChain.MaxSeriesLength}',
                    f'IsRewardLearnable={self._TransferParam.is_reward_learnable()}'
                ])
            v_status_mas.append(f'LearnParam: {v_learn_param}')
            if self._TransferParam.is_reward_learnable():
                v_learn_status = emb.data_to_text([
                    f'Reward={self._RewardInLastStep}',
                    f'Stick={self._StickInLastStep}',
                    f'WaitRewardLevel: {self.WaitRewardLevel}'
                ])
                v_status_mas.append(f'Learn: {v_learn_status}')

        return emb.line_mas_to_text(v_status_mas)

    # ==============================
    def __repr__(self):
        v_cell_mas = []

        if self.NoneLevel is not None:
            v_cell_mas.append('NoneLevel: {}'.format(self.NoneLevel.Value))

        for it_cell_idx, it_cell in self.CellMas.items():
            v_cell_mas.append('{}:\n{}'.format(
                self.get_cell_info(it_cell_idx),
                emb.data_to_text(it_cell, emb.GIndentSize)
            ))

        return emb.line_mas_to_text([
            self._Caption + ' [',
            *v_cell_mas,
            ']',
            self.get_status()
        ])

    # ==============================
    def _dispatch_input_chains_work(
            self,
            i_input_cell_group: CCellGroup,
            i_start_input_active: transfer.CActiveCellsState
    ):

        for it_cell in self.CellMas.values():
            it_cell.competition_reset()

        v_inner_active = self._get_chain_active()

        v_workable_cells = []
        for it_cell_idx, it_cell in self.CellMas.items():
            #  для активированной в предыдущем шаге ячейки возможен перенос-повторение
            if not it_cell.work__can_repeat():
                if it_cell.work__is_run():
                    continue

                if self.CurrentChain.is_contains(it_cell_idx):
                    continue

            # Перенос активности ячейки со входных и внутренних в соревнование текущей ячейки
            v_work_transfer_list = it_cell.competition_run(i_input_cell_group, self)

            v_workable_cells.append((it_cell_idx, v_work_transfer_list))

        v_max_strong = None

        v_full_catch = None
        if not i_start_input_active.is_none():
            class _CFullCatch:
                CatchAll = False
                Transfer = transfer.CTransfer(
                    i_start_input_active,
                    v_inner_active,
                    self._TransferParam
                )

                def get_transfer(self):
                    return copy.deepcopy(self.Transfer)

            v_full_catch = _CFullCatch()

        for it_work in v_workable_cells:
            it_cell_idx = it_work[0]
            it_cell = self.CellMas[it_cell_idx]

            if self.WaitRewardLevel.is_max():
                it_cell.competition_set_level(1)

            if not it_cell.competition_get_result():
                # todo: may be inhibition work (try copy link to other cell)
                continue

            it_work_transfer_list = it_work[1]
            for it_transfer in it_work_transfer_list:
                it_transfer.work_transfer(i_input_cell_group, self)
                if v_full_catch:
                    if it_transfer.is_same_input(i_start_input_active):
                        v_full_catch.CatchAll = True

            it_cell.work__set(True)

            if v_full_catch:
                if v_full_catch.Transfer.is_learnable():
                    it_cell.add_transfer(v_full_catch.get_transfer())

            v_strong = it_cell.Strong.Value
            if v_max_strong is None or v_max_strong[0] < v_strong:
                v_max_strong = [v_strong, it_cell_idx]

        if v_full_catch:
            #  Если еще нет ячейки полностью разбирающей текущий вход
            if not v_full_catch.CatchAll:
                self._use_new_cell(i_input_cell_group, v_full_catch.get_transfer())

        # Самая сильная активированная ячейка
        # становится текущей
        if v_max_strong is not None:
            self._InitCurrentCellIdx = v_max_strong[1]

    # ==============================
    def _dispatch_input_chain_learn(self, i_input_cell_group: CCellGroup):
        if not self.CurrentChain.is_can_learn():
            return

        v_input_active = i_input_cell_group._get_rest_active()
        if v_input_active.is_none():
            if self.NoneLevel is None:
                return
            if not self.NoneLevel.is_max():
                return

        v_inner_active = self._get_chain_active()

        # Если входная маска еще не вся разобрана, то
        # на эту маску устанавливается обработка

        v_transfer = transfer.CTransfer(
            v_input_active,
            v_inner_active,
            self._TransferParam
        )

        self._use_new_cell(i_input_cell_group, v_transfer)

    # ==============================
    def _use_new_cell(
            self,
            i_input_cell_group: typing.Optional[CCellGroup],
            i_transfer: transfer.CTransfer
    ):

        if self.CurrentChain.MaxSeriesLength < i_transfer.get_inner_range():
            # текущая цепочка уже достигла максимально разрешен длину
            return

        # ищется слабая ячейка
        # и устанавливается обработка

        for it_idx, it_cell in sorted(self.CellMas.items(), key=lambda item: item[1].Strong.Value):
            if not it_cell.add_transfer(i_transfer):
                continue

            i_transfer.work_transfer(i_input_cell_group, self)
            it_cell.work__set(True)

            self.CurrentChain.add_cell_idx(it_idx)
            break

    # ==============================
    def _dispatch_inner_chain_learn(self, i_start_input_active: transfer.CActiveCellsState):
        """Если вход разобран, и формируется цепочка, и сработал старт других цепочек,
        То прописываем эти старты в обучаемые трансферы к формирующейся цепочке"""

        if i_start_input_active.is_none():
            return

        if self.CurrentChain.is_none():
            return

        v_last_chain_cell_idx = self.CurrentChain.get_last_cell_idx()

        vl_last_chain_cell = self.CellMas[v_last_chain_cell_idx]

        if vl_last_chain_cell.work__is_current():
            return

        v_current_active = transfer.CActiveCellsState(
            {v_last_chain_cell_idx: vl_last_chain_cell.competition__get_time()},
            vl_last_chain_cell.get_mem_active_range() + 1
        )

        if self.CurrentChain.MaxSeriesLength < v_current_active.Range:
            return

        v_transfer = transfer.CTransfer(
            i_start_input_active,
            v_current_active,
            self._TransferParam
        )

        if v_transfer.is_learnable():
            for it_cell_idx, it_cell in self.CellMas.items():
                if self.CurrentChain.is_contains(it_cell_idx):
                    continue
                # Первая ячейка в цепочке
                if it_cell.get_mem_active_range() != 1:
                    continue
                # Активная в текущий момент
                if not it_cell.is_active():
                    continue
                if not it_cell.work__is_current():
                    continue

                it_cell.add_transfer(v_transfer)
                v_transfer.work_transfer(None, self)
        else:
            # !!! Добавить новую ячейку в цепь
            self._use_new_cell(None, v_transfer)

    # ==============================
    def _dispatch_input_rest(self, i_input_cell_group: CCellGroup):
        # оптимизация отработавших ?-связей
        for it_cell in self.CellMas.values():
            it_cell.transfer_optimization()

        # формирование ?-связей по оставшемуся неразобранному входу
        v_input_active = i_input_cell_group._get_rest_active()
        if v_input_active.is_none():
            return

        # чистый вход
        v_inner_active = transfer.CActiveCellsState(
            {},
            1,
            self.NoneLevel.is_max() if self.NoneLevel else False
        )

        for it_cell in self.CellMas.values():
            if not it_cell.work__is_current():
                continue

            v_transfer = transfer.CTransfer(
                v_input_active,
                v_inner_active,
                self._TransferParam
            )

            it_cell.add_transfer(v_transfer)

    # ==============================
    def dispatch_input(self, i_input_cell_group: CCellGroup):
        v_start_input_active = i_input_cell_group._get_rest_active()
        self._dispatch_input_chains_work(i_input_cell_group, v_start_input_active)
        self._dispatch_input_chain_learn(i_input_cell_group)
        self._dispatch_inner_chain_learn(v_start_input_active)
        self._dispatch_input_rest(i_input_cell_group)

        for it_cell in self.CellMas.values():
            it_cell.set_transfer_caption(i_input_cell_group, self)

    def reward(self, i_reward: typing.Optional[bool]):
        if not self._TransferParam.is_reward_learnable():
            return

        if i_reward:
            self.WaitRewardLevel.reset()
            self._RewardInLastStep = True

        for itCell in self.CellMas.values():
            itCell.reward(i_reward)

    def stick(self, i_stick: typing.Optional[bool]):
        if i_stick:
            self._StickInLastStep = True

        for itCell in self.CellMas.values():
            itCell.stick(i_stick)

    def get_output(self) -> CState:
        v_out_state = CState(len(self.CellMas), 1)

        for it_cell_idx, it_cell in self.CellMas.items():
            if it_cell.work__is_ready():
                v_out_state.set_item_max(it_cell_idx)

        return v_out_state

    def load(self, i_xml_state: xmlET.Element):
        if i_xml_state is None:
            return

        v_cells = i_xml_state.find('Cells')
        v_cell_list = v_cells.findall('Cell')

        v_cells_state = {}
        for s in v_cell_list:
            v_cell_idx = xml_utils.attr_get_int(s, 'Idx')
            v_cells_state[v_cell_idx] = s

        for it_cell_idx, it_cell in self.CellMas.items():
            v_cell_state = None
            if it_cell_idx in v_cells_state:
                v_cell_state = v_cells_state[it_cell_idx]

            it_cell.load(v_cell_state)

        # self.CurrentChain.load(i_state['Series'])
        # self.InitCurrentCell = i_state['Active']

    def save(self, i_xml_state: xmlET.Element):
        vl_cells = xmlET.SubElement(i_xml_state, 'Cells')
        for it_cell_idx, it_cell in self.CellMas.items():
            vl_cell_state = xmlET.SubElement(vl_cells, 'Cell')
            vl_cell_state.attrib['Idx'] = str(it_cell_idx)

            it_cell.save(vl_cell_state)
