import unittest


# ==============================
class CRangeValue:
    Max: int
    Value: int

    def __init__(self, i_max, i_value=0):
        self.Max = i_max
        self.Value = i_value

    def is_init(self):
        return 0 < self.Max
        
    def inc(self):
        self.Value = min(self.Value+1, self.Max)
        
    def dec(self):
        self.Value = max(self.Value-1, 0)
        
    def set_max(self):
        self.Value = self.Max
        
    def reset(self):
        self.Value = 0
        
    def is_set(self):
        return 0 < self.Value
        
    def is_max(self):
        if not self.is_set():
            return False

        return self.Max == self.Value
        
    def is_equal(self, i_range):
        return (
            self.Value == i_range.Value
            and
            self.Max == i_range.Max
        )

    def is_max_near(self):
        return self.is_set() and self.Value + 1 == self.Max

    def is_min_next(self):
        return self.Value == 1
        
    def __repr__(self):
        if self.Max == 0:
            return '<>'
        if self.Max == 1:
            return '<{}>'.format(
                '*' if self.is_max() else '0'
            )
        else:
            return '<{:>{align}}_{:>{align}}>'.format(
                '*' if self.is_max() else self.Value,
                self.Max,
                align=len(str(self.Max))
            )
        
        
# ==============================
class CCellStateLink:
    def __init__(self, i_cell, i_level):
        self.CellLink = i_cell
        self.Level = i_level
        

# ==============================
class CMask:
    def __init__(self, i_size, i_def=False):
        self.Mas = [i_def] * i_size
        
    def is_null(self):
        for it in self.Mas:
            if it:
                return False
        return True
        
    def is_set_item(self, i_idx):
        return self.Mas[i_idx]
        
    def get_size(self):
        return len(self.Mas)
        
    def fill(self, i_value, i_state):
        if self.get_size() != i_state.get_size():
            raise Exception('Dif size')
            
        for it, _ in enumerate(self.Mas):
            if i_state.is_set_item(it):
                self.Mas[it] = i_value
                
    def augment(self, i_state):
        self.fill(True, i_state)
        
    def subtract(self, i_state):
        self.fill(False, i_state)
        
    def __repr__(self):
        return '<{}>'.format(self.Mas)
        

# ==============================
class CState:
    def __init__(self, i_size, i_range):
        self.Mas = []
        for _ in range(i_size):
            self.Mas.append(CRangeValue(i_range))
            
    def load(self, i_state):
        if i_state is None:
            return
            
        for it, itValue in enumerate(self.Mas):
            itValue.Value = i_state[it]
            
    def create_mask(self):
        v_result = CMask(self.get_size())
        v_result.augment(self)
        return v_result
        
    def get_size(self):
        return len(self.Mas)
        
    def is_set_item(self, i_idx):
        return self.Mas[i_idx].is_set()
        
    def is_set(self):
        for it in self.Mas:
            if it.is_set():
                return True
        return False
        
    def set_item_max(self, i_idx):
        return self.Mas[i_idx].set_max()
        
    def auto_dec(self):
        for it in self.Mas:
            it.dec()
        
    def is_same_state(self, i_state):
        if self.get_size() != i_state.get_size():
            raise Exception('Dif size')
            
        for itMy, itOut in zip(self.Mas, i_state.Mas):
            if itMy.is_set():
                if not itMy.is_equal(itOut):
                    return False
                    
        return True
        
    def get_active_count(self):
        v_result = 0
        for it in self.Mas:
            if it.is_set():
                v_result = v_result + 1
        return v_result
        
    def apply_mask(self, i_mask):
        if self.get_size() != i_mask.get_size():
            raise Exception('Dif size')
        
        for it, itState in enumerate(self.Mas):
            if not i_mask.is_set_item(it):
                itState.reset()
        
    def __repr__(self):
        v_result = []
        for it in self.Mas:
            v_result.append(str(it))
            
        return '['+', '.join(v_result)+']'
               

# ==============================
class TestState(unittest.TestCase):
    def test_range(self):
        v = CRangeValue(5)
        print(v)

        v_m_1 = CMask(5)
        v_m_1.Mas[0] = True
        v_m_2 = CMask(5)
        v_m_2.Mas[3] = True

        print(v_m_1)
        print(v_m_2)
        v_m_1.augment(v_m_2)
        print(v_m_1)
        v_m_1.subtract(v_m_2)
        print(v_m_1)

        v_s_1 = CState(5, 6)
        v_s_2 = CState(5, 6)
        v_s_1.set_item_max(3)
        print(v_s_1.is_same_state(v_s_2))
        print(v_s_1.is_same_state(v_s_1))
        print(v_s_1)
        v_s_1.auto_dec()
        print(v_s_1)
        print(v_s_1.get_active_count())
