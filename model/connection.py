import math
import typing
import xml.etree.ElementTree as xmlET
from dataclasses import dataclass

from model.item import CItem, CBlock, LineAddressT, LinkT
from model import xml_utils
        

# ==============================
class CConnection(CItem):
    LinkMas: typing.List[LinkT]
    From: CBlock
    To: CBlock

    @dataclass
    class SettingT:
        Item: CItem.SettingT
        From: CBlock
        To: CBlock
        LinkMas: typing.List[LinkT]

    def __init__(self, i_params: SettingT):
        super().__init__(i_params.Item)
        self.LinkMas = i_params.LinkMas
        self.From = i_params.From
        self.To = i_params.To

        self._GatherSize = self.To.get_gather_size()
        self._RepeatStep = self.To.get_repeat_step()

        c_gather_size = math.prod(self._GatherSize)
        c_repeat_step = math.prod(self._RepeatStep)

        if 1 < c_repeat_step or 1 < c_gather_size:
            c_first_line_count = None
            for itLink in self.LinkMas:
                c_line_count = self.From.get_output_count(itLink.From, itLink.Count, self._GatherSize, self._RepeatStep)
                if c_first_line_count is None:
                    c_first_line_count = c_line_count
                else:
                    if c_first_line_count != c_line_count:
                        raise Exception('All lines need to be same sizes')

            self.To.set_repeat_count(c_first_line_count)
        else:
            self._RepeatCount = self.From.get_repeat_count()
            c_from_repeat_count = math.prod(self._RepeatCount)
            if 1 < c_from_repeat_count:
                for itLink in self.LinkMas:
                    self.To.set_in_line_shape(itLink.To, itLink.Count, self._RepeatCount)
    
    # ==============================
    @classmethod
    def create_from_xml(cls, i_xml_item: xmlET.Element, i_block_getter):
        v_item_params: CItem.SettingT = super().param_from_xml(i_xml_item)
        v_version = i_xml_item.attrib['Version']

        c_known_versions = ['1.0', '2.0']
        if v_version not in c_known_versions:
            raise Exception(f'Connection bad version {v_version}')
        
        v_data = i_xml_item.find('Data')

        if 'FromID' in v_data.attrib:
            v_block_id = v_data
        else:
            v_block_id = i_xml_item

        v_from_id = v_block_id.attrib['FromID']
        v_to_id = v_block_id.attrib['ToID']
        
        v_from_block = i_block_getter(v_from_id)
        v_to_block = i_block_getter(v_to_id)
        
        v_link_list = v_data.findall('Link')
        
        v_link_mas = []
        for itLink in v_link_list:
            v_new_link = LinkT(
                i_from=LineAddressT(
                    i_name=xml_utils.attr_get(itLink, 'FromLineName', ''),
                    i_start_idx=xml_utils.attr_get_int(itLink, 'FromLineIdx', 0)
                ),
                i_to=LineAddressT(
                    i_name=xml_utils.attr_get(itLink, 'ToLineName', ''),
                    i_start_idx=xml_utils.attr_get_int(itLink, 'ToLineIdx', 0)
                ),
                i_count=xml_utils.attr_get_int(itLink, 'LineCount', 1)
            )
            v_link_mas.append(v_new_link)
          
        return cls(CConnection.SettingT(v_item_params, v_from_block, v_to_block, v_link_mas))
    
    # ==============================    
    def save(self, i_parent_node):
        pass
        
    # ==============================
    def do_transmit(self):
        for itLink in self.LinkMas:
            v_data = self.From.get_output(itLink.From, itLink.Count)
            self.To.set_input(itLink.To, itLink.Count, v_data)
