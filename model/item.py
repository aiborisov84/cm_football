import math
import typing
from abc import ABC, abstractmethod
import xml.etree.ElementTree as xmlET
from dataclasses import dataclass

import numpy

from model import emb


# ==============================
class LineAddressT:
    def __init__(self, i_name, i_start_idx):
        self.Name = i_name
        self.StartIdx = i_start_idx


# ==============================
class LinkT:
    From: LineAddressT
    To: LineAddressT
    Count: int

    def __init__(self, i_from, i_to, i_count):
        self.From = i_from
        self.To = i_to
        self.Count = i_count


# ==============================
class CItem(ABC):

    @dataclass
    class SettingT:
        Caption: str

    def __init__(self, i_params: SettingT):
        self._DrawItem = None
        self.Position = None
        self.Caption = i_params.Caption
        
    @classmethod
    def param_from_xml(cls, i_xml_item: xmlET.Element) -> SettingT:
        v_caption = i_xml_item.attrib['Caption']
        return CItem.SettingT(v_caption)
        
    @abstractmethod
    def save(self, i_parent_node):
        pass


# ==============================
class CBlock(CItem):
    _State: emb.TFullLineState
    IsLog: bool

    @dataclass
    class SettingT:
        Item: CItem.SettingT
        BlockId: str

    def __init__(self, i_params: SettingT, i_item_lines: emb.TFullLineParam, i_is_log: bool):
        super().__init__(i_params.Item)
        self.IsLog = i_is_log
        self.BlockId = i_params.BlockId
        
        v_state = {}

        for itType, itLineSet in i_item_lines.items():
            v_line_set_state = {}
            for itName, itSize in itLineSet.items():

                v_size_param = None
                if isinstance(itSize, emb.CLineSize):
                    v_size_param = itSize
                elif isinstance(itSize, list):
                    v_size_param = emb.CLineSize(i_array_shape=itSize)
                elif isinstance(itSize, int):
                    v_size_param = emb.CLineSize(i_thread_count=itSize)

                v_line_set_state[itName] = v_size_param.data_create()

            v_state[itType] = v_line_set_state
            
        self._State = v_state
        
    # ==============================
    @classmethod
    def param_from_xml(cls, i_xml_item: xmlET.Element) -> SettingT:
        v_item_params: CItem.SettingT = super().param_from_xml(i_xml_item)

        v_blocks = i_xml_item.findall('Block')
        if len(v_blocks):
            v_block_id = v_blocks[0].attrib['ID']
        else:
            v_block_id = i_xml_item.attrib['ID']

        return CBlock.SettingT(v_item_params, v_block_id)
        
    # ==============================
    def save(self, i_parent_node):
        pass

    # ==============================
    def set_repeat_count(self, i_repeat_count):
        if 1 < math.prod(i_repeat_count):
            raise Exception(f"Can't set repeat count {i_repeat_count} in this block {self.BlockId}")

    # ==============================
    def set_in_line_shape(self, i_line: LineAddressT, i_count: int, i_shape):
        vl_in = self._State[emb.LineType.Input]
        vl_it_in_line = vl_in[i_line.Name]
        for it in range(i_line.StartIdx, i_line.StartIdx + i_count):
            vl_it_in_line[it] = numpy.zeros(i_shape, dtype=int)

    # ==============================
    def set_input(self, i_to_line_idx: LineAddressT, i_line_count, i_line_data):
        vl_input_set = self._State[emb.LineType.Input]
        vl_input = vl_input_set[i_to_line_idx.Name]
        for itIdx in range(i_line_count):
            vl_input[i_to_line_idx.StartIdx + itIdx] = i_line_data[itIdx]

    # ==============================
    def get_output_count(
            self,
            i_from_line_idx: LineAddressT,
            i_line_count,
            i_gather_size: typing.List[int],
            i_repeat_step: typing.List[int]
    ):
        if math.prod(i_gather_size) == 1:
            return [1]

        v_output_set = self._State[emb.LineType.Output]
        v_output = v_output_set[i_from_line_idx.Name]
        v_result = None
        for itIdx in range(i_line_count):
            v_output_thread = v_output[i_from_line_idx.StartIdx + itIdx]
            c_shape = v_output_thread.shape
            if len(c_shape) != len(i_gather_size):
                raise Exception(f'No same shape in output and gather {c_shape}!={i_gather_size}')
            if len(c_shape) != len(i_repeat_step):
                raise Exception(f'No same shape in output and repeat {c_shape}!={i_repeat_step}')

            v_thread_result = []
            for it_size, it_gather, it_step in zip(c_shape, i_gather_size, i_repeat_step):
                if it_step < 1:
                    v_thread_result.append(1)
                else:
                    v_count = 0
                    for it in range(0, it_size, it_step):
                        if it + it_gather <= it_size:
                            v_count += 1
                        else:
                            break

                    c_count = 1 + (it_size - it_gather) // it_step

                    assert c_count == v_count

                    v_thread_result.append(c_count)

            if len(v_thread_result) == 0:
                raise Exception(f'Wrong shape={c_shape} gather={i_gather_size} step={i_repeat_step}')

            if v_result is None:
                v_result = v_thread_result
            else:
                # select minimal count to fit for all thread
                for it_idx, it_thread in enumerate(v_thread_result):
                    if it_thread < v_result[it_idx]:
                        v_result[it_idx] = it_thread

        if v_result is None:
            raise Exception(f'Wrong line_count={i_line_count}')

        return v_result

    # ==============================
    def get_output(
            self,
            i_from_line_idx: LineAddressT,
            i_line_count
    ):
        v_out_set = self._State[emb.LineType.Output]
        v_out = v_out_set[i_from_line_idx.Name]

        if isinstance(v_out, numpy.ndarray):
            if i_line_count == 1:
                v_result = [v_out]  # one line result
            else:
                v_result = []
                for it in range(i_from_line_idx.StartIdx, i_from_line_idx.StartIdx + i_line_count):
                    v_result.append(v_out[numpy.ix_([it])])
        else:
            v_result = []
            for it_idx in range(i_line_count):
                c_out_thread_idx = i_from_line_idx.StartIdx + it_idx
                v_out_thread = v_out[c_out_thread_idx]

                v_result.append(v_out_thread)

        return v_result

    # ==============================
    @abstractmethod
    def do_tick(self):
        pass

    # ==============================
    @abstractmethod
    def get_gather_size(self):
        pass

    # ==============================
    @abstractmethod
    def get_repeat_step(self):
        pass

    # ==============================
    @abstractmethod
    def get_repeat_count(self):
        pass

    # ==============================
    @abstractmethod
    def get_component(self):
        pass
