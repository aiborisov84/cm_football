import enum
from abc import ABC, abstractmethod
from itertools import chain
import platform
import math
import typing
import numpy
import unittest

from io import StringIO
import textwrap

from PySide6.QtGui import QFont

GIsDroid = platform.machine().startswith('arm') or platform.machine().startswith('aarch')


def get_appl_font():
    if GIsDroid:
        return QFont('Droid Sans Mono', 16)
    else:
        return QFont('Courier New', 16)


def get_log_font():
    if GIsDroid:
        return QFont('Droid Sans Mono', 6)
    else:
        return QFont('Courier New', 8)


# ==============================
class LineType(enum.Enum):
    Input = 0
    Inner = 1
    Output = 2


def line_type_to_str(i_type: LineType):
    if i_type == LineType.Input:
        return 'In'
    elif i_type == LineType.Output:
        return 'Out'
    elif i_type == LineType.Inner:
        return 'Self'

    return ''


# ==============================
class CLineSize:
    ThreadCount: int
    ArrayShape: typing.List[int]
    Level: int
    UserControl: bool

    def __init__(self, i_thread_count: int = 1, i_array_shape=None, i_level=1, i_user_control=False):
        if i_array_shape is None:
            i_array_shape = [1]

        self.ThreadCount = i_thread_count
        self.ArrayShape = i_array_shape
        self.Level = i_level
        self.UserControl = i_user_control

    def get_dimension(self):
        o_dimension = 0

        if math.prod(self.ArrayShape) > 1:
            o_dimension += len(self.ArrayShape)

        if 1 < self.ThreadCount:
            o_dimension += 1

        if o_dimension < 1:
            o_dimension = 1

        return o_dimension

    def get_shape(self):
        o_shape = []

        if math.prod(self.ArrayShape) > 1:
            o_shape.extend(self.ArrayShape)

        if 1 < self.ThreadCount:
            o_shape.append(self.ThreadCount)

        if not o_shape:
            o_shape = [1]

        return o_shape

    def get_size(self):
        return math.prod(self.ArrayShape) * self.ThreadCount

    def data_create(self):
        return [
            numpy.zeros(self.ArrayShape, dtype=int)
            for _ in range(self.ThreadCount)
        ]

    def __repr__(self):
        return f'CLineSize({self.ThreadCount}x{self.ArrayShape})'


TLineDict = typing.Dict[str, typing.List[numpy.ndarray]]
TLineSizeUnion = typing.Union[CLineSize, typing.List[int], int]
TLineParamDict = typing.Dict[str, CLineSize]
TFullLineParam = typing.Dict[LineType, TLineParamDict]
TFullLineState = typing.Dict[LineType, TLineDict]


# ==============================
def line_param_map_to_line_map(i_line_param_map: TLineParamDict) -> TLineDict:
    return {
            it: [
                numpy.zeros(it_line_size.ArrayShape, dtype=int)
                for _ in range(it_line_size.ThreadCount)
            ]
            for it, it_line_size in i_line_param_map.items()
        }


# ==============================
def line_map_fill(io_line_map: TLineDict, i_line_name: str, i_flat_value):
    def get_value(i_simple_value):
        if isinstance(i_simple_value, int):
            return i_simple_value
        elif isinstance(i_simple_value, bool):
            return 1 if i_simple_value else 0
        else:
            raise Exception(f'Line "{i_line_name}" can\'t fill by {type(i_simple_value)}')

    if isinstance(i_flat_value, list) or isinstance(i_flat_value, tuple):
        it_in_item = iter(i_flat_value)
        v_thread_list = io_line_map[i_line_name]
        for it_ndarray in v_thread_list:
            vl_flat_mas = it_ndarray.flat
            for it_flat_idx in range(len(vl_flat_mas)):
                c_value = next(it_in_item)
                vl_flat_mas[it_flat_idx] = get_value(c_value)
    else:
        io_line_map[i_line_name][0][0] = get_value(i_flat_value)


# ==============================
# ==============================
GIndentSize = 4
GIndent: str = ' '*GIndentSize  # '\t'


# ==============================
def do_format(i_data, i_indent: typing.Optional[int] = None):
    c_indent = ' '*i_indent if i_indent is not None else GIndent
    if isinstance(i_data, dict):
        def dict_format(i_key, ii_data):
            if i_key == "":
                return str(ii_data)
            else:
                return f"{i_key}: {ii_data}"

        return ('\r\n'+c_indent).join(chain([''], [dict_format(k, v) for k, v in i_data.items()]))
    else:
        return ('\r\n'+c_indent).join(chain([''], map(str, i_data)))


# ==============================
def line_mas_to_text(
        i_str_list: typing.List[str],
        i_indent_size: int = 0
):
    s = '\r\n'.join(i_str_list)

    if 0 < i_indent_size:
        s = textwrap.indent(s, ' ' * i_indent_size)

    return s


# ==============================
def verbose_string_to_text(i_verbose_str, i_indent='  '):
    def list_to_text(i_verbose_str_inner, i_level: int):
        if isinstance(i_verbose_str_inner, list):
            v_result = []
            for it in i_verbose_str_inner:
                s = list_to_text(it, i_level+1)
                v_result.append(s)
            s = '\n'.join(v_result)
            if 0 < i_level:  # and 1 < len(i_verbose_str_inner):
                s = textwrap.indent(s, i_indent)
            return s
        else:
            s = str(i_verbose_str_inner)
            return s

    return list_to_text(i_verbose_str, 0)


# ==============================
def data_to_text(
        i_data,
        i_indent_size: int = 0,
        i_sub_indent_size: int = GIndentSize
):
    if isinstance(i_data, dict):
        def dict_format(i_key, ii_data):
            if i_key == "":
                return str(ii_data)
            else:
                return f"{i_key}: {ii_data}"

        v_line_mas = [dict_format(k, v) for k, v in i_data.items()]
    elif isinstance(i_data, list):
        v_line_mas = [data_to_text(it, i_sub_indent_size, i_sub_indent_size) for it in i_data]
        v_line_mas.insert(0, '[')
        v_line_mas.append(']')
    else:
        v_line_mas = [str(i_data)]

    return line_mas_to_text(v_line_mas, i_indent_size)


# ==============================
def line_map_to_text(
        i_lines_map: TLineDict,
        i_indent_size: int = 0,
        i_sub_indent_size: int = GIndentSize
):
    c_sub_indent = ' '*i_sub_indent_size
    c_2_sub_indent = c_sub_indent + c_sub_indent
    v_lines_mas = []
    for it_name, it_line in i_lines_map.items():
        v_s_out = StringIO()
        print(f'Line<{it_name}>: [', file=v_s_out)
        for it_idx, it_thread in enumerate(it_line):
            if isinstance(it_thread, numpy.ndarray):
                print(c_sub_indent + f'{it_name}_{it_idx}: ', file=v_s_out, end=('' if it_thread.ndim < 2 else '\n'))
                s = str(it_thread)
                if 1 < it_thread.ndim:
                    s = textwrap.indent(str(it_thread), c_2_sub_indent)
                print(s, file=v_s_out)
            else:
                print(data_to_text(it_thread, i_sub_indent_size), file=v_s_out)
        print(']', file=v_s_out, end='')
        v_lines_mas.append(v_s_out.getvalue())

    return line_mas_to_text(v_lines_mas, i_indent_size)


# ==============================
def info_block_to_text(i_caption: str, i_info: str, i_indent: int = 0):
    s = textwrap.indent(i_info, ' ' * 4)

    s = f"[== {i_caption}\r\n{s}\r\n]== {i_caption}"

    if i_indent:
        s = textwrap.indent(s, ' ' * i_indent)

    return s


# ==============================
class IDynamic(ABC):
    """Динамический блок"""

    @abstractmethod
    def do_step(self):
        pass


# ==============================
class IDetector(IDynamic):
    """Динамический детектор"""

    @abstractmethod
    def check_and_start(self, i_tick_idx, i_input):
        pass


# ==============================
class ISampleDetector(IDetector):
    """Динамический образец со стартом по детектору"""

    @abstractmethod
    def get_current(self):
        pass


# ==============================
class ISample(IDynamic):
    """Динамический образец с ручным стартом"""

    @abstractmethod
    def set_start(self):
        pass


# ==============================
class IModule(ABC):
    """Интерфейс модуля среды. Должен быть реализован в классе с именем CModule"""

    Param: TFullLineParam

    def __init__(self, i_env_param: TFullLineParam, i_net_module_param: TFullLineParam):
        if i_net_module_param:
            IModule.verify_param(i_env_param, i_net_module_param)
        self.Param = i_env_param

    @staticmethod
    def verify_param(i_env_param: TFullLineParam, i_net_module_param: TFullLineParam):
        for it_state in [LineType.Input, LineType.Output]:
            v_net_param = i_net_module_param[it_state]
            v_env_param = i_env_param[it_state]
            for it_net_name, it_net_line_size in v_net_param.items():
                if it_net_name not in v_env_param:
                    raise Exception(f'Line "{it_net_name}" is not exists in lines')

                c_env_line_size = v_env_param[it_net_name]
                for it in c_env_line_size.__dict__.keys():
                    v_net_value = it_net_line_size.__dict__[it]
                    v_env_value = c_env_line_size.__dict__[it]
                    if v_net_value != v_env_value:
                        raise Exception(f'Line "{it_net_name}" has different {it}: {v_net_value} != {v_env_value}')

    def get_param(self) -> TFullLineParam:
        return self.Param

    @abstractmethod
    def step(self, i_tick_idx, i_input):
        pass

    @abstractmethod
    def get_component(self):
        pass

    @abstractmethod
    def get_info(self):
        pass

    @abstractmethod
    def get_caption(self):
        pass


# ==============================
# ==============================
class CDetectItem:
    def __init__(self, *i_arg_mas):
        self.Mas = [*i_arg_mas]
        while len(self.Mas) > 0 and self.Mas[-1] is None:
            self.Mas.pop()

    def is_same(self, i_input):
        if len(i_input) < len(self.Mas):
            return False

        for itDetect, itInput in zip(self.Mas, i_input):
            if itDetect is None:
                continue
            if itDetect != itInput:
                return False

        return True


# ==============================
class CDetector(IDetector):
    def __init__(
            self,
            i_detect_mas,
            i_is_work: typing.Callable = None,
            i_on_detect: typing.Callable = None,
            i_on_step_detect: typing.Callable = None
    ):
        self.Idx = None
        self._DetectMas = i_detect_mas
        self._DetectInLast = False
        self._IsWork = i_is_work
        self._OnDetect = i_on_detect
        self._OnStepDetect = i_on_step_detect

    def check_and_start(self, i_tick_idx, i_input):
        if self._IsWork is not None:
            if not self._IsWork():
                self.Idx = None
                return False

        if self.Idx is None:
            self.Idx = 0

        if not self._DetectMas[self.Idx].is_same(i_input):
            self.Idx = None
            return False

        if self._OnStepDetect is not None:
            self._OnStepDetect()

        if len(self._DetectMas) != self.Idx+1:
            return False

        self.Idx = None
        self._DetectInLast = True
        if self._OnDetect is not None:
            self._OnDetect()
        return True

    def do_step(self):
        if self.Idx is not None:
            self.Idx = self.Idx + 1

        self._DetectInLast = False

    @property
    def is_detect_in_last(self):
        return self._DetectInLast


# ==============================
# ==============================
class CSample(ISample):
    def __init__(
            self,
            i_out_mas,
            i_repeat_count: int = 1,
            i_silent_steps_before_cycle: typing.Optional[int] = None,
            i_caption: str = ''
    ):
        self.Idx: typing.Optional[int] = None
        self._DataSteps = i_repeat_count * len(i_out_mas)
        self._SilentStepsBeforeCycle = i_silent_steps_before_cycle
        self._OutMas = i_out_mas
        self.Caption = i_caption

    def is_just_started(self):
        return self.Idx == 0

    def state(self):
        if self.Idx is None:
            return '-'
        else:
            if self._SilentStepsBeforeCycle:
                c_sum = self._DataSteps + self._SilentStepsBeforeCycle
                c_prefix = 'd' if self.Idx < self._DataSteps else 's'
                return f'{c_prefix}{self.Idx}({c_sum}=d{self._DataSteps}+s{self._SilentStepsBeforeCycle})'
            else:
                return f'{self.Idx}({self._DataSteps})'

    def is_before_ended(self):
        if self.Idx:
            if self._DataSteps == self.Idx + 1:
                return True
        return False

    def do_step(self):
        if self.Idx is not None:
            self.Idx = self.Idx + 1
            if self._SilentStepsBeforeCycle:
                if self._DataSteps + self._SilentStepsBeforeCycle <= self.Idx:
                    self.Idx = 0
            else:
                if self._DataSteps <= self.Idx:
                    self.Idx = None

    def get_current(self):
        if self.Idx is None:
            return []

        if self.Idx < self._DataSteps:
            return self._OutMas[self.Idx % len(self._OutMas)]
        else:
            return []

    def get_length(self):
        return self._DataSteps

    def set_start(self):
        self.Idx = 0

    def set_stop(self):
        self.Idx = None


# ==============================
class CConditionalSample(ISampleDetector):
    def __init__(
            self,
            i_out_mas,
            i_out_none,
            i_is_start: typing.Callable,
            i_length: typing.Optional[int] = None,
            i_on_end: typing.Callable = None
    ):
        self.Idx = None
        self._OutMas = i_out_mas
        self._OutNone = i_out_none
        self._IsStart = i_is_start
        if i_length is None:
            self._Length = len(self._OutMas)
        else:
            self._Length = max(i_length, len(self._OutMas))
        self._OnEnd = i_on_end

    def check_and_start(self, i_tick_idx, i_input):
        if self._IsStart(i_tick_idx, i_input) and self.Idx is None:
            self.Idx = 0
            return True
        else:
            return False

    def do_step(self):
        if self.Idx is None:
            return

        self.Idx = self.Idx + 1

        if self.Idx < self._Length:
            return

        self.Idx = None
        if self._OnEnd is not None:
            self._OnEnd()

    def get_current(self):
        if self.Idx is not None and self.Idx < len(self._OutMas):
            return self._OutMas[self.Idx]
        else:
            return self._OutNone


# ==============================
class CHistory:
    Mas: typing.List
    MaxHistoryLength: int

    def __init__(self, i_max_history_length=10):
        self.Mas = []
        self.MaxHistoryLength = i_max_history_length

    def push(self, i_value):
        self.Mas.insert(0, i_value)
        while self.MaxHistoryLength < len(self.Mas):
            self.Mas.pop()

    def state(self):
        if self.Mas:
            v_hist_list = '; '.join(map(str, self.Mas[1:]))
            return f'{self.Mas[0]} [{v_hist_list}]'
        else:
            return ''


# ==============================
class TestCellMethods(unittest.TestCase):
    def test_verbose_str(self):
        v = [1, [2, 3, [4], [5, 6]]]
        print(verbose_string_to_text(v))
