import QtQuick.Controls
import QtQuick
import QtQuick.Shapes
import QtQuick.Layouts

Item {
    property string blockCaption : "Env"
    property Item envCustomItem: null
    property Item envFormLine: null

    id: root

    anchors.fill: parent

    Page {
        anchors.fill: parent

        header: Page {

            height: header.height + (cbInfoVisible.checked ? cmptTxt.height : 0)

            header: ToolBar {
                id: captionHeader
                height: cbInfoVisible.height

                RowLayout
                {
                    anchors.fill: parent
                    Label {
                        id: envLabel
                        leftPadding: 5
                        elide: Label.ElideRight
                        Layout.fillWidth: true
                        verticalAlignment: Text.AlignVCenter
                        text: applicationData.getTextCaption()
                        visible: text.length > 0
                    }
                    CheckBox {
                        id: cbInfoVisible
                        text: qsTr("Info")
                        checked: true
                    }
                }
            }

            Flickable {
                id: flicArea
                anchors.top: parent.top
                width: parent.width
                height: cmptTxt.height
                clip: true
                visible: cbInfoVisible.checked
                TextArea.flickable: TextArea {
                    id: cmptTxt

                    font: applicationData.getLogFont()
                    readOnly: true
                    //selectByMouse: true
                    textFormat: TextEdit.MarkdownText
                    wrapMode: TextEdit.NoWrap
                    text: applicationData.textInfo
                    onLinkActivated: (link)=> Qt.openUrlExternally(link)

                    HoverHandler {
                        enabled: parent.hoveredLink
                        cursorShape: Qt.PointingHandCursor
                    }
                }

                Keys.onUpPressed: {scrollBarV.decrease(); }
                Keys.onDownPressed: {scrollBarV.increase();}

                ScrollBar.vertical: ScrollBar {id: scrollBarV; active: true}
                ScrollBar.horizontal: ScrollBar {active: true}
            }
        }

        StackLayout {
            id: swipeViewType
            anchors.fill: parent

            SplitView {
                id: splitCustom
                orientation: Qt.Vertical

                handle: Rectangle {
                     implicitWidth: 4
                     implicitHeight: 4
                     //color: SplitHandle.pressed ? "#81e889"
                     //    : (SplitHandle.hovered ? Qt.lighter("#c2f4c6", 1.1) : "#c2f4c6")

                     color: SplitHandle.pressed ? splitCustom.palette.mid
                         : (SplitHandle.hovered ? splitCustom.palette.light : splitCustom.palette.midlight)

                 }

                ItemTextView {
                    SplitView.preferredWidth: viewEnvCustom.visible ? parent.width*0.4 : parent.width
                    SplitView.preferredHeight: viewEnvCustom.visible ? parent.height*0.4 : parent.height
                    textFont: applicationData.getLogFont()
                    contentText: applicationData.textEnv
                }

                Page {
                    id: viewEnvCustom
                    SplitView.fillWidth: true
                    SplitView.fillHeight: true
                }
            }

            Page {
                id: viewFormLine
            }
        }

        footer: ToolBar {
            id: selectView
            visible: false

            RowLayout {
                anchors.fill: parent

                TabButton {
                    text: "Log"
                    checked: true
                    width: implicitWidth
                    onClicked: swipeViewType.currentIndex = 0
                }

                TabButton {
                    text: "Lines"
                    width: implicitWidth
                    onClicked: swipeViewType.currentIndex = 1
                }
                CheckBox {
                    id: chVertHor
                    visible: viewEnvCustom.visible
                    text: qsTr("Vert|Hor")
                    checked: true
                    onToggled: {
                        if (checkState === Qt.Checked)
                            splitCustom.orientation = Qt.Vertical
                        else
                            splitCustom.orientation = Qt.Horizontal
                    }
                }
                Item {Layout.fillWidth: true}
            }
        }
    }

    function setCustom(iComponent)
    {
        if(envCustomItem)
        {
            console.log("Environment Ready custom destroy")
            viewEnvCustom.visible = false
            envCustomItem.destroy()
            envCustomItem = null
        }

        if(iComponent)
        {
            console.log("Environment Ready custom insert")
            envCustomItem = iComponent.createObject(viewEnvCustom, {"x": 0, "y": 0});
            viewEnvCustom.visible = true
        }
        else
        {
            viewEnvCustom.visible = false
        }
    }

    function setFormLineView(iFormLineView)
    {
        if(envFormLine)
        {
            console.log("Environment Ready FormLine destroy")
            envFormLine.destroy()
            envFormLine = null
        }

        if(iFormLineView)
        {
            console.log("Environment Ready FormLine insert")
            envFormLine = iFormLineView.createObject(viewFormLine, {"x": 0, "y": 0});
            selectView.visible = true
        }
        else
        {
            selectView.visible = false
        }
    }

    Component.onCompleted: {
        blockCaption = applicationData.getTextCaption()

        console.log("Environment Completed! ["+blockCaption+"]")

        applicationData.setRoot(this)
    }

    Component.onDestruction: {
        console.log("Environment Destruction! ["+blockCaption+"]")

        setCustom(null)
        applicationData.setRoot(null)
    }
}