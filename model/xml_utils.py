# ==============================
import typing
import xml.etree.ElementTree as xmlET


def attr_get_int(i_xml_node: xmlET.Element, i_name: str, i_def: typing.Optional[int] = None) -> typing.Optional[int]:
    if i_name in i_xml_node.attrib:
        return int(i_xml_node.attrib[i_name])
    else:
        return i_def


# ==============================
def attr_get_int_def(i_xml_node: xmlET.Element, i_name: str, i_def: int) -> int:
    return attr_get_int(i_xml_node, i_name, i_def)


# ==============================
def attr_get(i_xml_node: xmlET.Element, i_name: str, i_def=None):
    if i_name in i_xml_node.attrib:
        return i_xml_node.attrib[i_name]
    else:
        return i_def
