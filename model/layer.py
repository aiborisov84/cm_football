import typing
import xml.etree.ElementTree as xmlET
from dataclasses import dataclass

from datetime import datetime
import logging
import glob
import math
import numpy
import os.path as op

from PySide6.QtGui import QFont
from PySide6.QtQuick import QQuickItem
from PySide6.QtQml import QQmlComponent, QQmlEngine
from PySide6.QtCore import QObject, Slot, Property, Signal, QUrl, QCoreApplication  # , QTimer

from model import emb, xml_utils
from model.item import CBlock
from model.layercore.analyse import CAnalyse
from model.layercore.group import CCellGroup

from model.FormLayerGroupView import Control as mFormLayerGroupView
from model.FormLineMapView import Control as mFormLineMapView

# ==============================
# TODO: Учет частичных активированных переходов и использование их подсказок
#  при выборе новой активации для не полностью разобранных входных сигналов
#
# TODO: Cell transfer saving and loading by user command
#
# TODO: MultipleSign - add node to separate transfer and competition
#  (Если на каждый сигнал уже есть действие, то на сочетание возможно новое действие или запрет уже привязанных)


# ==============================
class PyApplicationData(QObject):
    onCaptionChanged = Signal()
    onInputChanged = Signal()
    _text_inner: typing.List[str]
    _analyse_link: CAnalyse
    _FormLayerGroupViewControlMas: typing.List[mFormLayerGroupView.CControl]

    def __init__(
            self,
            i_caption: str,
            i_analyse_link: CAnalyse,
            i_lines_param: emb.TFullLineParam,
            i_state_dir_file: typing.Tuple[str, str]
    ):
        QObject.__init__(self)

        self._Root = None
        self._status_text = ""
        self._text_input = ""
        self._caption = i_caption
        self._state_dir_file = i_state_dir_file
        self._state_list = []

        self._analyse_link = i_analyse_link
        v_cell_group_mas: typing.List[CCellGroup] = i_analyse_link.GroupMas

        self._FormLayerGroupViewControlMas = []
        for itIdx, itGroup in enumerate(v_cell_group_mas):
            if itIdx == 0:
                v_caption = "In"
            elif itIdx == len(v_cell_group_mas)-1:
                v_caption = "Out"
            else:
                v_caption = f"Inner{itIdx}"
            self._FormLayerGroupViewControlMas.append(
                mFormLayerGroupView.CControl(
                    itGroup,
                    v_caption
                )
            )

        self._FormLineMapViewControl = mFormLineMapView.CControl(
            i_lines_param,
            self._caption if self._caption else "LayerBlock"
        )
        self._FormLineMapView = self._FormLineMapViewControl.get_component()

    def __del__(self):
        if self._Root:
            self._Root.setFormLayerGroupViewIn(None)
            self._Root.setFormLayerGroupViewInner([])
            self._Root.setFormLayerGroupViewOut(None)
            self._Root.setFormLineMapView(None)

        del self._FormLayerGroupViewControlMas

        self._FormLineMapView = None
        self._FormLineMapViewControl = None

    @Slot(QQuickItem)
    def setRoot(self, i_root):
        self._Root = i_root
        if self._Root:
            v_comp_mas = [it.get_component() for it in self._FormLayerGroupViewControlMas]
            self._Root.setFormLayerGroupViewIn(v_comp_mas[0])
            self._Root.setFormLayerGroupViewInner(v_comp_mas[1:-1])
            self._Root.setFormLayerGroupViewOut(v_comp_mas[-1])

            self._Root.setFormLineMapView(self._FormLineMapView)

    @Property(str, notify=onCaptionChanged)
    def textCaption(self):
        return self._caption

    @Property(str, notify=onInputChanged)
    def textInput(self):
        return self._text_input

    @Slot(int, result=str)
    def getTextInner(self, i_inner_idx):
        if i_inner_idx < len(self._text_inner):
            return self._text_inner[i_inner_idx]
        else:
            return ''

    @Slot()
    def saveState(self):
        v_suffix = datetime.now().strftime("_%Y%m%d_%H%M%S")
        v_filename = self._state_dir_file[1] + v_suffix + '.state'
        v_filepath = op.join(self._state_dir_file[0], v_filename)

        self._analyse_link.save(v_filepath)

    @Slot()
    def stateSelectDialog(self):
        if self._Root:
            self._state_list = sorted(glob.glob(op.join(self._state_dir_file[0], self._state_dir_file[1] + '*.state')))

            v_short_state_list = [op.basename(it) for it in self._state_list]

            self._Root.selectStateFile(v_short_state_list)

    @Slot(int)
    def loadStateFile(self, i_state_file_index: int):
        if len(self._state_list) <= i_state_file_index:
            return

        v_file_name = self._state_list[i_state_file_index]
        if not op.exists(v_file_name):
            return

        self._analyse_link.load(v_file_name)

    @Slot(result=QFont)
    def getLogFont(self):
        return emb.get_log_font()

    def set_status_text(
            self,
            i_input_text: str
    ):
        for itGroup in self._FormLayerGroupViewControlMas:
            itGroup.fill_log_text()

        self._text_input = i_input_text
        # noinspection PyUnresolvedReferences
        self.onInputChanged.emit()

    def fill_state(self, i_full_line_state: emb.TFullLineState):
        self._FormLineMapViewControl.fill_state(i_full_line_state)


# ==============================
class CLayer(CBlock):
    _qml_data: typing.Optional[PyApplicationData]
    Param: emb.TFullLineParam

    @dataclass
    class SettingT:
        Block: CBlock.SettingT
        ItemLines: emb.TFullLineParam
        GatherSize: typing.List[int]  # xml.attr['GatherSize'] | [1]
        Analyse: CAnalyse.SettingT

    def __init__(self, i_params: SettingT, i_is_log: bool, i_do_qml_component=True):
        super().__init__(i_params.Block, i_params.ItemLines, i_is_log)
        self._InnerIdx = 0
        self._CreateParams = i_params
        self.Param = i_params.ItemLines

        self._GatherSize = i_params.GatherSize

        v_input_count: emb.CLineSize = self.Param[emb.LineType.Input]['']
        self._OutputCount = 0
        self._OutputMap = []
        for itName, itCount in self.Param[emb.LineType.Output].items():
            self._OutputCount += itCount.ThreadCount
            for itIdx in range(itCount.ThreadCount):
                self._OutputMap.append((itName, itIdx))

        self.Analyse = CAnalyse(
            i_params.Analyse,
            CAnalyse.InOutSettingT(
                InCount=v_input_count.get_size(),
                ActionCount=self._OutputCount,
            )
        )

        self._qml_data = None

        if i_do_qml_component:
            if QCoreApplication.instance():
                self._qml_engine = QQmlEngine()
                self._qml_data = PyApplicationData(
                    self.Caption,
                    self.Analyse,
                    self.Param,
                    (i_params.Analyse.State.Dir, i_params.Analyse.State.Caption)
                )
                self._qml_engine.rootContext().setContextProperty(
                    "applicationData", self._qml_data)

                v_dir = op.dirname(__file__)
                self._qml_engine.setBaseUrl(QUrl("file:///" + v_dir + "/"))

                self._component = QQmlComponent(self._qml_engine)
                self._component.loadUrl(QUrl("Layer.qml"))
                if self._component.status() != QQmlComponent.Ready:
                    self._component = None

    def __del__(self):
        if QCoreApplication.instance():
            self._component = None
            self._qml_engine = None
            self._qml_data = None

    # ==============================
    @classmethod
    def create_from_xml(cls, i_xml_item: xmlET.Element, i_dir: str, i_is_log: bool):
        v_block_params: CBlock.SettingT = super().param_from_xml(i_xml_item)
        v_version = i_xml_item.attrib['Version']

        c_current_version = '3.0'
        if v_version != c_current_version:
            raise Exception('Bad version')

        v_caption = i_xml_item.attrib['Caption']

        v_gather_size = [1]
        if 'GatherSize' in i_xml_item.attrib:
            v_gather_size = list(map(int, i_xml_item.attrib['GatherSize'].split(':')))

        c_input_gather_multiple = math.prod(v_gather_size)

        v_inputs = i_xml_item.findall('Input')
        v_outputs = i_xml_item.findall('Output')
        v_data = i_xml_item.find('Data')

        v_item_lines: emb.TFullLineParam = {emb.LineType.Inner: {'': emb.CLineSize(1)}}

        v_input_sizes: emb.TLineParamDict = {}
        for itInput in v_inputs:
            v_in_name = itInput.attrib['Name']
            v_in_size = xml_utils.attr_get_int(itInput, 'Size')
            v_input_sizes[v_in_name] = emb.CLineSize(v_in_size * c_input_gather_multiple)

        v_item_lines[emb.LineType.Input] = v_input_sizes

        v_output_sizes: emb.TLineParamDict = {}
        for itOutput in v_outputs:
            v_out_name = itOutput.attrib['Name']
            v_out_size = xml_utils.attr_get_int(itOutput, 'Size')
            v_output_sizes[v_out_name] = emb.CLineSize(v_out_size)

        v_item_lines[emb.LineType.Output] = v_output_sizes

        v_cell = v_data.find('Cell')
        v_link = v_data.find('Link')
        v_action = v_data.find('Action')
        c_action_level = xml_utils.attr_get_int(v_action, 'Level')
        v_learn = v_data.find('Learn')

        v_analyse_param = CAnalyse.SettingT(
            State=CAnalyse.SettingT.StateT(Caption=v_caption, Dir=i_dir),
            Cell=CAnalyse.SettingT.CellT(
                Level=xml_utils.attr_get_int(v_cell, 'Level'),
                Strong=xml_utils.attr_get_int(v_cell, 'Strong')
            ),
            Link=CAnalyse.SettingT.LinkT(
                Level=xml_utils.attr_get_int(v_link, 'Level'),
                Utility=xml_utils.attr_get_int(v_link, 'Utility')
            ),
            Action=CAnalyse.SettingT.ActionT(
                Level=c_action_level,
                Strong=xml_utils.attr_get_int(v_action, 'Strong')
            ),
            Learn=CAnalyse.SettingT.LearnT(
                MaxLength=xml_utils.attr_get_int(v_learn, 'MaxLength'),
                WaitRewardLevel=xml_utils.attr_get_int_def(v_learn, 'WaitRewardLevel', c_action_level * 9)
            ),
            InnerGroupCount=xml_utils.attr_get_int_def(v_cell, 'InnerGroupCount', 1),
            CellCount=xml_utils.attr_get_int(v_cell, 'Count'),
            NoneActionLevel=xml_utils.attr_get_int(v_cell, 'NoneActionLevel', -1),
        )

        v_params = CLayer.SettingT(
            Block=v_block_params,
            ItemLines=v_item_lines,
            GatherSize=v_gather_size,
            Analyse=v_analyse_param
        )
        # v2 v_params['Data'] = v_data.firstChild.nodeValue

        return cls(v_params, i_is_log)

    # ==============================
    def do_tick(self):
        self._InnerIdx = self._InnerIdx + 1

        v_input = self.Analyse.create_new_input()

        c_in_lines = self._State[emb.LineType.Input]

        c_in_data = c_in_lines['']

        def get_special_input(i_name):
            v_res = None
            if i_name in c_in_lines:
                c_in_special = c_in_lines[i_name]
                if len(c_in_special) > 0:
                    if c_in_special[0] > 0:
                        v_res = True
                    else:
                        v_res = False
                        
            return v_res

        c_reward = get_special_input('Reward')
        c_stick = get_special_input('Stick')

        v_flatten_input = []
        for it_data in c_in_data:
            if not isinstance(it_data, numpy.ndarray):
                v_flatten_input.append(it_data)
            else:
                c_line_data = list(map(int, numpy.nditer(it_data)))
                v_flatten_input.extend(c_line_data)

        for it, itData in enumerate(v_flatten_input):
            if itData > 0:
                v_input.Mas[it].set_max()

        v_in_str = f'{emb.line_map_to_text(c_in_lines)}\r\nAnalyseInput={v_input}'

        v_output = self.Analyse.step(v_input, c_reward, c_stick)
        v_analyse_str = str(self.Analyse)

        vl_out = self._State[emb.LineType.Output]
        for itIdx, itOutLink in enumerate(self._OutputMap):
            v_out_name = itOutLink[0]
            v_out_idx = itOutLink[1]
            vl_out[v_out_name][v_out_idx][0] = 1 if v_output.Mas[itIdx].is_set() else 0

        if self._qml_data:
            self._qml_data.fill_state(self._State)
            self._qml_data.set_status_text(
                f"Tick: {self._InnerIdx}" +
                (" Reward" if c_reward else "") +
                (" Stick" if c_stick else "")
            )

        if self.IsLog:
            logging.info(
                '\r\n'.join([
                    f'[====== Layer [{self.Caption}]',
                    emb.info_block_to_text('In', v_in_str),
                    emb.info_block_to_text('Analyse', v_analyse_str),
                    emb.info_block_to_text('Out', emb.line_map_to_text(vl_out)),
                    ']====== Layer'
                ])
            )

    # ==============================
    def get_gather_size(self):
        return self._GatherSize

    # ==============================
    def get_repeat_step(self):
        return [0] * len(self._GatherSize)

    # ==============================
    def get_repeat_count(self):
        return [0] * len(self._GatherSize)

    # ==============================
    def get_component(self):
        return self._component
