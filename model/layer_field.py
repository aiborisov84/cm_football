import typing
import xml.etree.ElementTree as xmlET

import logging
from dataclasses import dataclass

import numpy
import glob
import os.path as op

from PySide6.QtGui import QFont
from PySide6.QtQuick import QQuickItem
from PySide6.QtQml import QQmlComponent, QQmlEngine
from PySide6.QtCore import QObject, Slot, Property, Signal, QUrl, QCoreApplication  # , QTimer

from model import emb
from model import xml_utils
from model.item import CBlock
from model.layercore.analyse import CAnalyse
from model.layer import CLayer


class PyApplicationData(QObject):
    onCaptionChanged = Signal()
    onInputChanged = Signal()
    onInnerChanged = Signal()
    onOutputChanged = Signal()
    _inner_group_count: int

    def __init__(self, i_caption):
        QObject.__init__(self)

        self._Root = None
        self._inner_group_count = 0
        self._status_text = ""
        self._text_input = ""
        self._text_inner = ""
        self._text_output = ""
        self._caption = i_caption

    @Slot(QQuickItem)
    def setRoot(self, i_root):
        self._Root = i_root

    @Property(str, notify=onCaptionChanged)
    def textCaption(self):
        return self._caption

    @Property(str, notify=onInputChanged)
    def textInput(self):
        return self._text_input

    @Property(str, notify=onInnerChanged)
    def textInner(self):
        return self._text_inner

    @Property(str, notify=onOutputChanged)
    def textOutput(self):
        return self._text_output

    @Slot(result=QFont)
    def getLogFont(self):
        return emb.get_log_font()

    @Slot(result=int)
    def getInnerCount(self):
        return self._inner_group_count

    def set_inner_status(self, i_status: typing.Dict[str, str]):
        self._inner_group_count = len(i_status)

        if self._Root:
            self._Root.setRefreshInner(i_status)

    def set_status_text(
            self,
            i_input_text: str,
            i_inner_text: str,
            i_output_text: str
    ):
        self._text_input = i_input_text
        # noinspection PyUnresolvedReferences
        self.onInputChanged.emit()

        self._text_inner = i_inner_text
        # noinspection PyUnresolvedReferences
        self.onInnerChanged.emit()

        self._text_output = i_output_text
        # noinspection PyUnresolvedReferences
        self.onOutputChanged.emit()


# ==============================
class CLayerField(CBlock):
    MainLayer: CLayer
    Param: emb.TFullLineParam

    @dataclass
    class SettingT:
        Block: CBlock.SettingT
        ItemLines: emb.TFullLineParam
        GatherSize: typing.List[int]  # xml.attr['GatherSize'] | [1]
        RepeatStep: typing.List[int]  # xml.attr['RepeatStep'] | [1]*len(GatherSize)
        Analyse: CAnalyse.SettingT

    def __init__(self, i_params: SettingT, i_is_log: bool):
        super().__init__(i_params.Block, i_params.ItemLines, i_is_log)

        self._LayerCreateParams = i_params

        self.MainLayer = CLayer(i_params, i_is_log)
        self._InnerIdx = 0
        self.Param = i_params.ItemLines

        self._GatherSize = i_params.GatherSize
        self._RepeatStep = i_params.RepeatStep
        self._RepeatCount = [1] * len(self._RepeatStep)
        c_default_repeat_multi_index = tuple(0 for _ in range(len(self._RepeatStep)))
        self._RepeatMap = {(0,): (self.MainLayer, c_default_repeat_multi_index)}

        v_input_count: emb.CLineSize = self.Param[emb.LineType.Input]['']
        self._OutputCount = 0
        self._OutputMap = []
        for itName, itCount in self.Param[emb.LineType.Output].items():
            self._OutputCount += itCount.ThreadCount
            for itIdx in range(itCount.ThreadCount):
                self._OutputMap.append((itName, itIdx))

        self.Analyse = CAnalyse(
            i_params.Analyse,
            CAnalyse.InOutSettingT(
                InCount=v_input_count.get_size(),
                ActionCount=self._OutputCount,
            )
        )

        if QCoreApplication.instance():
            self._qml_engine = QQmlEngine()
            self._qml_data = PyApplicationData(self.Caption)
            self._qml_engine.rootContext().setContextProperty(
                "applicationData", self._qml_data)

            v_dir = op.dirname(__file__)
            self._qml_engine.setBaseUrl(QUrl("file:///" + v_dir + "/"))

            self._component = QQmlComponent(self._qml_engine)
            self._component.loadUrl(QUrl("LayerField.qml"))
            if self._component.status() != QQmlComponent.Ready:
                self._component = None

    def __del__(self):
        if QCoreApplication.instance():
            self._component = None
            self._qml_engine = None
            self._qml_data = None

    # ==============================
    @classmethod
    def create_from_xml(cls, i_xml_item: xmlET.Element, i_dir: str, i_is_log: bool):
        v_block_params: CBlock.SettingT = super().param_from_xml(i_xml_item)
        v_version = i_xml_item.attrib['Version']

        c_current_version = '3.0'
        if v_version != c_current_version:
            raise Exception('Bad version')

        v_caption = i_xml_item.attrib['Caption']

        v_gather_size = [1]
        if 'GatherSize' in i_xml_item.attrib:
            v_gather_size = list(map(int, i_xml_item.attrib['GatherSize'].split(':')))

        v_repeat_step = [1] * len(v_gather_size)
        if 'RepeatStep' in i_xml_item.attrib:
            v_repeat_step = list(map(int, i_xml_item.attrib['RepeatStep'].split(':')))

        v_inputs = i_xml_item.findall('Input')
        v_outputs = i_xml_item.findall('Output')
        v_data = i_xml_item.find('Data')

        v_item_lines = {emb.LineType.Inner: {'': emb.CLineSize(1)}}

        v_input_sizes = {}
        for itInput in v_inputs:
            v_in_name = itInput.attrib['Name']
            v_in_thread_count = xml_utils.attr_get_int(itInput, 'Size')
            v_input_sizes[v_in_name] = emb.CLineSize(v_in_thread_count, v_gather_size)

        v_item_lines[emb.LineType.Input] = v_input_sizes

        v_output_sizes = {}
        for itOutput in v_outputs:
            v_out_name = itOutput.attrib['Name']
            v_out_size = xml_utils.attr_get_int(itOutput, 'Size')
            v_output_sizes[v_out_name] = emb.CLineSize(v_out_size)

        v_item_lines[emb.LineType.Output] = v_output_sizes

        v_cell = v_data.find('Cell')
        v_link = v_data.find('Link')
        v_action = v_data.find('Action')
        c_action_level = xml_utils.attr_get_int(v_action, 'Level')
        v_learn = v_data.find('Learn')

        v_analyse_param = CAnalyse.SettingT(
            State=CAnalyse.SettingT.StateT(Caption=v_caption, Dir=i_dir),
            Cell=CAnalyse.SettingT.CellT(
                Level=xml_utils.attr_get_int(v_cell, 'Level'),
                Strong=xml_utils.attr_get_int(v_cell, 'Strong')
            ),
            Link=CAnalyse.SettingT.LinkT(
                Level=xml_utils.attr_get_int(v_link, 'Level'),
                Utility=xml_utils.attr_get_int(v_link, 'Utility')
            ),
            Action=CAnalyse.SettingT.ActionT(
                Level=c_action_level,
                Strong=xml_utils.attr_get_int(v_action, 'Strong')
            ),
            Learn=CAnalyse.SettingT.LearnT(
                MaxLength=xml_utils.attr_get_int(v_learn, 'MaxLength'),
                WaitRewardLevel=xml_utils.attr_get_int_def(v_learn, 'WaitRewardLevel', c_action_level * 9)
            ),
            InnerGroupCount=xml_utils.attr_get_int_def(v_cell, 'InnerGroupCount', 1),
            CellCount=xml_utils.attr_get_int(v_cell, 'Count'),
            NoneActionLevel=xml_utils.attr_get_int(v_cell, 'NoneActionLevel', -1),
        )

        v_params = CLayerField.SettingT(
            Block=v_block_params,
            ItemLines=v_item_lines,
            GatherSize=v_gather_size,
            RepeatStep=v_repeat_step,
            Analyse=v_analyse_param
        )

        return cls(v_params, i_is_log)

    # ==============================
    def do_tick(self):
        self._InnerIdx = self._InnerIdx + 1

        c_delegate_realisation = True

        if c_delegate_realisation:
            v_in_str_mas = [emb.line_map_to_text(self._State[emb.LineType.Input])]
            v_inner_status = {}
            vl_full_in = self._State[emb.LineType.Input]
            vl_full_out = self._State[emb.LineType.Output]
            for it_repeat_idx, it_link in self._RepeatMap.items():
                vl_one_layer = it_link[0]
                c_input_idx = it_link[1]

                vl_one_in = vl_one_layer._State[emb.LineType.Input]
                vl_one_out = vl_one_layer._State[emb.LineType.Output]

                for it_name, it_full_in_line in vl_full_in.items():
                    vl_one_line = vl_one_in[it_name]

                    if len(vl_one_line) != len(it_full_in_line):
                        raise Exception(f'Thread count must be same')

                    for it_idx, it_thread in enumerate(it_full_in_line):
                        if not isinstance(it_thread, numpy.ndarray):
                            raise Exception("Bad inner layer thread. No ndarray")

                        c_gather_dimension = len(self._GatherSize)
                        if it_thread.ndim != c_gather_dimension:
                            raise Exception(f'Bad dimension gather {c_gather_dimension}!={it_thread.ndim} (out)')

                        if it_thread.ndim != len(c_input_idx):
                            raise Exception(f'Bad dimension in c_input_idx {c_input_idx}!={it_thread.ndim} (out)')

                        v_select_idx = []
                        for it_d in range(it_thread.ndim):
                            c_d_select_idx = list(
                                range(c_input_idx[it_d], c_input_idx[it_d] + self._GatherSize[it_d]))
                            v_select_idx.append(numpy.array(c_d_select_idx, dtype=numpy.intp))

                        vl_one_line[it_idx] = it_thread[numpy.ix_(*v_select_idx)]  # one line result

                v_inner_status.setdefault(f'ri{it_repeat_idx}', emb.line_map_to_text(vl_one_in, 4))

                vl_one_layer.do_tick()

                for it_name, it_full_out_line in vl_full_out.items():
                    vl_one_line = vl_one_out[it_name]
                    for it_idx, it_thread in enumerate(vl_one_line):
                        it_full_out_line[it_idx][it_repeat_idx] = it_thread[0]

            v_out_text = emb.line_map_to_text(vl_full_out)
            self._qml_data.set_status_text(
                ("Tick: {}\r\n".format(self._InnerIdx)) +
                emb.info_block_to_text('In', '\r\n'.join(v_in_str_mas)),
                emb.info_block_to_text('Inner', ''),
                emb.info_block_to_text('Out', v_out_text)
            )
            self._qml_data.set_inner_status(v_inner_status)

            if self.IsLog:
                logging.info(
                    '\r\n'.join([
                        f'[====== LayerField [{self.Caption}]',
                        emb.info_block_to_text('Out', v_out_text),
                        ']====== LayerFiled'
                    ])
                )
        else:
            v_input = self.Analyse.create_new_input()

            c_in_lines = self._State[emb.LineType.Input]

            c_in_data = c_in_lines['']

            def get_special_input(i_name):
                v_res = None
                if i_name in c_in_lines:
                    c_in_special = c_in_lines[i_name]
                    if len(c_in_special) > 0:
                        if c_in_special[0] > 0:
                            v_res = True
                        else:
                            v_res = False

                return v_res

            c_reward = get_special_input('Reward')
            c_stick = get_special_input('Stick')

            v_flatten_input = []
            for it_data in c_in_data:
                if not isinstance(it_data, numpy.ndarray):
                    v_flatten_input.append(it_data)
                else:
                    c_line_data = list(map(int, numpy.nditer(it_data)))
                    v_flatten_input.extend(c_line_data)

            for it, itData in enumerate(v_flatten_input):
                if itData > 0:
                    v_input.Mas[it].set_max()

            v_in_str = f'In: {v_input} Reward={c_reward} Stick={c_stick}'

            v_output = self.Analyse.step(v_input, c_reward, c_stick)
            v_analyse_str = str(self.Analyse)

            vl_out = self._State[emb.LineType.Output]
            for itIdx, itOutLink in enumerate(self._OutputMap):
                v_out_name = itOutLink[0]
                v_out_idx = itOutLink[1]
                vl_out[v_out_name][v_out_idx][0] = 1 if v_output.Mas[itIdx].is_set() else 0

            self._qml_data.set_status_text(
                ("Tick: {}\r\n".format(self._InnerIdx)) +
                str(self.Analyse.GroupMas[0]) +
                ("\r\nReward" if c_reward else "") +
                ("\r\nStick" if c_stick else ""),
                str(self.Analyse.GroupMas[1]) if len(self.Analyse.GroupMas) > 2 else "",
                str(self.Analyse.GroupMas[-1])
            )

            v_out_text = emb.line_map_to_text(vl_out)

            if self.IsLog:
                logging.info(
                    '\r\n'.join([
                        f'[====== LayerField [{self.Caption}]',
                        v_in_str,
                        v_analyse_str,
                        "[== Out\r\n{}\r\n]== Out".format(v_out_text),
                        ']====== LayerFiled'
                    ])
                )

    # ==============================
    def get_gather_size(self):
        return self._GatherSize

    # ==============================
    def get_repeat_step(self):
        return self._RepeatStep

    # ==============================
    def get_repeat_count(self):
        return self._RepeatCount

    # ==============================
    def set_repeat_count(self, i_repeat_count):
        if i_repeat_count == 1:
            return

        self._RepeatCount = i_repeat_count

        if len(self._RepeatStep) != len(i_repeat_count):
            raise Exception(f"Different size in repeat count {i_repeat_count} and repeat step {self._RepeatStep}")

        c_repeat_array = numpy.zeros(i_repeat_count, dtype=int)
        v_nd_iter = numpy.nditer(c_repeat_array, flags=['multi_index'])
        self._RepeatMap = {}
        for _ in v_nd_iter:
            c_repeat_multi_idx = v_nd_iter.multi_index
            # {(0,): (self.MainLayer, (0, 0))}

            v_repeat_input_multi_idx = []
            for it_repeat_idx, it_repeat_step in zip(c_repeat_multi_idx, self._RepeatStep):
                v_repeat_input_multi_idx.append(it_repeat_idx * it_repeat_step)
            c_repeat_input_multi_idx = tuple(v_repeat_input_multi_idx)

            if sum(c_repeat_multi_idx) == 0:
                vl_new_layer = self.MainLayer
            else:
                vl_new_layer = CLayer(self._LayerCreateParams, False, i_do_qml_component=False)

            self._RepeatMap.setdefault(v_nd_iter.multi_index, (vl_new_layer, c_repeat_input_multi_idx))

        vl_in = self._State[emb.LineType.Input]
        for it_name, it_in_line in vl_in.items():
            for it in range(len(it_in_line)):
                v_shape = []
                for it_gather, it_step, it_count in zip(self._GatherSize, self._RepeatStep, i_repeat_count):
                    if it_step < 1:
                        v_shape.append(1)
                    else:
                        c_size = (it_count - 1) * it_step + it_gather

                        v_shape.append(c_size)

                it_in_line[it] = numpy.zeros(v_shape, dtype=int)

        vl_out = self._State[emb.LineType.Output]
        for it_name, it_out_line in vl_out.items():
            for it in range(len(it_out_line)):
                it_out_line[it] = numpy.zeros(i_repeat_count, dtype=int)

    # ==============================
    def get_component(self):
        return self._component
