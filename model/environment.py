import os.path as op
import importlib.util
import logging
import sys
import xml.etree.ElementTree as xmlET
from dataclasses import dataclass

from PySide6.QtGui import QFont
from PySide6.QtQuick import QQuickItem
from PySide6.QtQml import QQmlComponent, QQmlApplicationEngine
from PySide6.QtCore import QObject, Slot, Property, Signal, QUrl, QCoreApplication

from model import emb
from model.item import CBlock
from model.FormLineMapView import Control as mFormLineMapView

# ==============================
# TODO: auto uncheck in line activated by user


# ==============================
class PyApplicationData(QObject):
    onEnvChanged = Signal()
    onInfoChanged = Signal()

    def __init__(self, i_module: emb.IModule):
        QObject.__init__(self)

        self._Root = None
        self._status_text = ''
        self._text_env = ''
        self._info = ''

        self._custom_component = None
        self._caption = ""
        self._FormLineMapViewControl = None
        self._FormLineMapView = None

        if i_module:
            self._custom_component = i_module.get_component()
            self._caption = i_module.get_caption()
            self._info = i_module.get_info()
            # noinspection PyUnresolvedReferences
            self.onInfoChanged.emit()

            self._FormLineMapViewControl = mFormLineMapView.CControl(
                i_module.get_param(),
                self._caption if self._caption else "ModuleBlock"
            )
            self._FormLineMapView = self._FormLineMapViewControl.get_component()

    def __del__(self):
        if self._Root:
            self._Root.setCustom(None)
            self._Root.setFormLineView(None)

        self._custom_component = None
        self._FormLineMapView = None
        self._FormLineMapViewControl = None

    @Slot(QQuickItem)
    def setRoot(self, i_root):
        if self._Root and not i_root:
            self._Root.setCustom(None)
            self._Root.setFormLineView(None)
            self._custom_component = None
            self._FormLineMapView = None
            self._FormLineMapViewControl = None

        self._Root = i_root
        if self._Root:
            self._Root.setCustom(self._custom_component)
            self._Root.setFormLineView(self._FormLineMapView)

    @Property(str, notify=onEnvChanged)
    def textEnv(self):
        return self._text_env

    @Property(str, notify=onInfoChanged)
    def textInfo(self):
        return self._info

    @Slot(result=QFont)
    def getLogFont(self):
        return emb.get_log_font()

    # @Slot(result=QQmlComponent)
    # def getCustom(self):
    #     return self._custom_component

    @Slot(result=str)
    def getTextCaption(self):
        return self._caption

    def set_status_text(self, i_env_text: str):
        self._text_env = i_env_text
        # noinspection PyUnresolvedReferences
        self.onEnvChanged.emit()

    def fill_state(self, i_full_line_state: emb.TFullLineState):
        self._FormLineMapViewControl.fill_state(i_full_line_state)


# ==============================
class CEnvironment(CBlock):
    _Module: emb.IModule

    @dataclass
    class SettingT:
        Block: CBlock.SettingT
        ItemLines: emb.TFullLineParam
        EnvModule: emb.IModule

    def __init__(self, i_params: SettingT, i_is_log: bool):
        super().__init__(i_params.Block, i_params.ItemLines, i_is_log)
        
        self.CurrentTick = 0
        self._Module = i_params.EnvModule

        if QCoreApplication.instance():
            self._qml_data = PyApplicationData(self._Module)
            self._qml_engine = QQmlApplicationEngine()
            self._qml_engine.rootContext().setContextProperty(
                "applicationData", self._qml_data)

            v_dir = op.dirname(__file__)
            self._qml_engine.setBaseUrl(QUrl("file:///" + v_dir + "/"))

            self._component = QQmlComponent(self._qml_engine)
            self._component.loadUrl(QUrl("Environment.qml"))

            assert self._component.status() == QQmlComponent.Ready
        else:
            self._qml_data = None

    def __del__(self):
        if QCoreApplication.instance():
            self._component = None
            self._qml_engine = None
            self._qml_data = None

    # ==============================
    @classmethod
    def create_from_xml(cls, i_xml_item: xmlET.Element, i_dir: str, i_is_log: bool):
        v_block_params: CBlock.SettingT = super().param_from_xml(i_xml_item)
        v_version = i_xml_item.attrib['Version']
        
        c_known_versions = ['2.0', '3.0']
        if v_version not in c_known_versions:
            raise Exception(f'Environment bad version {v_version}')
        
        v_data = i_xml_item.find('Data')
        v_script_file = v_data.attrib['ScriptFile']
        v_inputs = i_xml_item.findall('Input')
        v_outputs = i_xml_item.findall('Output')

        v_item_lines = {}

        def parse_line(i_line_xml_item: xmlET.Element, io_param_dict):
            v_in_name = i_line_xml_item.attrib['Name']

            v_thread_count = 1
            v_shape = None

            v_user_control = i_line_xml_item.attrib.get('UserControl', 'false').lower() in ['true', '1']

            if 'Size' in i_line_xml_item.attrib:
                c_shape = list(map(int, i_line_xml_item.attrib['Size'].split(':')))
                if len(c_shape) == 1:
                    v_thread_count = c_shape[0]
                else:
                    v_shape = c_shape

            if 'Level' in i_line_xml_item.attrib:
                c_level = int(i_line_xml_item.attrib['Level'])
            else:
                c_level = 1

            io_param_dict[v_in_name] = emb.CLineSize(
                v_thread_count,
                v_shape,
                i_level=c_level,
                i_user_control=v_user_control
            )

        v_input_sizes = {}
        for itInput in v_inputs:
            parse_line(itInput, v_input_sizes)

        v_item_lines[emb.LineType.Input] = v_input_sizes

        v_output_sizes = {}
        for itOutput in v_outputs:
            parse_line(itOutput, v_output_sizes)

        v_item_lines[emb.LineType.Output] = v_output_sizes
        
        v_file = op.join(i_dir, v_script_file)
        v_module_name = op.basename(i_dir)
        v_sub_module_name, v_file_ext = op.splitext(v_script_file)

        if v_file_ext:
            if i_dir not in sys.path:
                sys.path.insert(0, i_dir)
            v_spec = importlib.util.spec_from_file_location(
                v_module_name,
                v_file,
                submodule_search_locations=[i_dir]
            )
            v_py_module = importlib.util.module_from_spec(v_spec)
            # noinspection PyUnresolvedReferences
            v_spec.loader.exec_module(v_py_module)
            # noinspection PyUnresolvedReferences
            v_module = v_py_module.CModule(v_item_lines)
        else:
            c_import_dir = op.dirname(i_dir)
            if c_import_dir not in sys.path:
                sys.path.insert(0, c_import_dir)
            if not v_sub_module_name:
                v_mod = importlib.import_module(v_module_name)
            else:
                v_mod = importlib.import_module(
                    "." + v_sub_module_name,
                    v_module_name
                )

            v_module = v_mod.CModule(v_item_lines)

        assert isinstance(v_module, emb.IModule)

        v_item_lines_param = v_module.get_param()
             
        return cls(CEnvironment.SettingT(v_block_params, v_item_lines_param, v_module), i_is_log)
    
    # ==============================
    def do_tick(self):
        v_input = self._State[emb.LineType.Input]
        v_inner, v_out = self._Module.step(
            self.CurrentTick, 
            v_input
        )
        self._State[emb.LineType.Inner] = v_inner
        self._State[emb.LineType.Output] = v_out

        if self._qml_data:
            self._qml_data.fill_state(self._State)

        v_env_str = '\r\n'.join([
            'Tick: {}'.format(self.CurrentTick),
            emb.info_block_to_text('In', emb.line_map_to_text(v_input)),
            emb.info_block_to_text('Inner', emb.line_map_to_text(v_inner)),
            emb.info_block_to_text('Out', emb.line_map_to_text(v_out))
        ])

        if self._qml_data:
            self._qml_data.set_status_text(v_env_str)

        if self.IsLog:
            logging.info('\r\n'.join([
                f'[====== Environment [{self.Caption}]',
                v_env_str,
                ']====== Environment'
            ]))

        self.CurrentTick = self.CurrentTick+1

    # ==============================
    def get_gather_size(self):
        return [1]

    # ==============================
    def get_repeat_step(self):
        return [0]

    # ==============================
    def get_repeat_count(self):
        return [0]

    # ==============================
    def get_component(self):
        return self._component
