import QtQuick.Controls
import QtQuick
import QtQuick.Shapes
import QtQuick.Layouts

Item {
    property string blockCaption: applicationData.textCaption
    property font logTextFont: applicationData.getLogFont()
    anchors.fill: parent

    property Item layFormGroupsIn: null
    property Item layFormGroupsOut: null
    property Item envFormLine: null

    ListModel {
        id: innerGroupList
    }

    Component {
        id: viewGroupDelegateTab

        Page {
            title: index+1
            property Item blockItem: null
        }
    }

    SystemPalette { id: myPalette; }

    Page {
        anchors.fill: parent

        header: ColumnLayout {
            spacing: 3
            ToolBar {
                Layout.preferredHeight: cbHeight.height
                Layout.fillWidth: true

                CheckBox {id: cbHeight; visible: false}

                RowLayout {
                    anchors.fill: parent
                    Label {
                        leftPadding: 5
                        rightPadding: 5
                        verticalAlignment: Text.AlignVCenter
                        text: applicationData.textCaption
                        //color: myPalette.light
                        visible: text.length > 0
                    }
                    Label {
                        id: envLabel
                        leftPadding: 5
                        rightPadding: 5
                        Layout.fillWidth: true
                        verticalAlignment: Text.AlignVCenter
                        text: "["+applicationData.textInput+"]"
                        //color: myPalette.light
                        visible: applicationData.textInput.length > 0
                    }
                }
            }
            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: selectView.height

                Layout.leftMargin: 3
                Layout.rightMargin: 3

                RowLayout {
                    anchors.fill: parent
                    Button {
                        id: layerActionsMenu
                        text: "Actions..."
                        onClicked: menu.open()

                        Menu {
                            id: menu
                            y: layerActionsMenu.height

                            MenuItem {
                                text: "Save state..."
                                onTriggered: applicationData.saveState()
                            }
                            MenuItem {
                                text: "Load state..."
                                onTriggered: applicationData.stateSelectDialog()
                            }
                        }
                    }
                    Item {Layout.fillWidth: true}
                    TabBar {
                        id: selectView

                        width: contentWidth

                        visible: false

                        TabButton {
                            text: "Log"
                            checked: true
                            width: implicitWidth
                        }

                        TabButton {
                            text: "Lines"
                            width: implicitWidth
                        }
                    }
                }
             }
        }

        StackLayout {
            id: swipeViewType
            currentIndex: selectView.currentIndex
            anchors.fill: parent
            anchors.margins: 3

            SplitView {
                //anchors.fill: parent
                orientation: Qt.Horizontal

                handle: Rectangle {
                     implicitWidth: 4
                     implicitHeight: 4

                     color: SplitHandle.pressed ? parent.palette.mid
                         : (SplitHandle.hovered ? parent.palette.light : parent.palette.midlight)

                }

                Item
                {
                    id: itemGroupsIn
                    property int defaultCollapseWidth: 20
                    property double defaultParentPercent: 0.2
                    SplitView.preferredWidth: parent.width * itemGroupsIn.defaultParentPercent
                    RowLayout {
                        anchors.fill: parent 
                        ToolBar {
                            id: collapsedBar
                            visible: false
                            Layout.preferredWidth: itemGroupsIn.defaultCollapseWidth
                            Layout.fillHeight: true
                            
                            Label {
                                text: layFormGroupsIn ? layFormGroupsIn.blockCaption : ""
                                width: parent.height
                                height: parent.width
                                y: parent.height
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                transformOrigin: Item.TopLeft
                                rotation: -90
                                MouseArea {
                                    id: collapseButton
                                    anchors.fill: parent
                                    onClicked: layFormGroupsIn.collapsed = !layFormGroupsIn.collapsed
                                }
                            }
                        }
                        Page {
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            id: viewFormGroupsIn
                        }
                    }

                    states: [
                        State {
                            name: ""
                            when: !layFormGroupsIn.collapsed
                            PropertyChanges { target: viewFormGroupsIn; visible: true }
                            PropertyChanges { target: collapsedBar; visible: false }
                            PropertyChanges { target: itemGroupsIn; SplitView.preferredWidth: itemGroupsIn.parent.width * itemGroupsIn.defaultParentPercent }
                        },
                        State {
                            name: "collapsed"
                            when: layFormGroupsIn.collapsed
                            PropertyChanges { target: viewFormGroupsIn; visible: false }
                            PropertyChanges { target: collapsedBar; visible: true }
                            PropertyChanges { target: itemGroupsIn; SplitView.preferredWidth: itemGroupsIn.defaultCollapseWidth }
                        }
                    ]
                }

                Page {
                    id: viewGroupMasTab
                    visible: false
                    SplitView.preferredWidth: parent.width*0.45

                    StackLayout {
                        id: swipeLayerMasView
                        anchors.fill: parent
                        currentIndex: tabBarLayerMas.currentIndex

                        Repeater {
                            id: repeatGroupMasView
                            model: innerGroupList
                            delegate: viewGroupDelegateTab
                        }
                    }

                    footer: TabBar {
                        id: tabBarLayerMas

                        Repeater {
                            model: innerGroupList
                            TabButton {
                                text: item_title
                            }
                        }
                    }
                }

                Page {
                    SplitView.preferredWidth: parent.width*0.35
                    id: viewFormGroupsOut
                }
            }

            Page {
                id: viewFormLine
            }
        }
    }

    function setFormLayerGroupViewIn(iFormLayerGroupView)
    {
        if(layFormGroupsIn)
        {
            console.log("Layer Ready FormGroup In destroy")
            layFormGroupsIn.destroy()
            layFormGroupsIn = null
        }

        if(iFormLayerGroupView)
        {
            console.log("Layer Ready FormGroup In insert")
            layFormGroupsIn = iFormLayerGroupView.createObject(viewFormGroupsIn, {"x": 0, "y": 0});
        }
    }

    function setFormLayerGroupViewInner(iFormLayerGroupViewMas)
    {
        for(var j = 0; j < innerGroupList.count; j++)
        {
            var vl_clean_page = repeatGroupMasView.itemAt(j)
            console.log("== App Prepare block free: "+vl_clean_page.blockItem.blockCaption)
            vl_clean_page.blockItem.destroy()
            vl_clean_page.blockItem = null
        }

        innerGroupList.clear()
        for(var i in iFormLayerGroupViewMas)
        {
            var vl_component = iFormLayerGroupViewMas[i]
            innerGroupList.append(
            {
                "content": (i).toString(),
                "item_title": (i).toString()
            })

            var vl_page = repeatGroupMasView.itemAt(i)
            vl_page.blockItem = vl_component.createObject(vl_page, {"x": 0, "y": 0})
            innerGroupList.setProperty(i, "item_title", vl_page.blockItem.blockCaption)
        }

        viewGroupMasTab.visible = (innerGroupList.count > 0)
    }

    function setFormLayerGroupViewOut(iFormLayerGroupView)
    {
        if(layFormGroupsOut)
        {
            console.log("Layer Ready FormGroup Out destroy")
            layFormGroupsOut.destroy()
            layFormGroupsOut = null
        }

        if(iFormLayerGroupView)
        {
            console.log("Layer Ready FormGroup Out insert")
            layFormGroupsOut = iFormLayerGroupView.createObject(viewFormGroupsOut, {"x": 0, "y": 0});
        }
    }

    function setFormLineMapView(iFormLineView)
    {
        if(envFormLine)
        {
            console.log("Layer Ready FormLine destroy")
            envFormLine.destroy()
            envFormLine = null
        }

        if(iFormLineView)
        {
            console.log("Layer Ready FormLine insert")
            envFormLine = iFormLineView.createObject(viewFormLine, {"x": 0, "y": 0});
            selectView.visible = true
        }
        else
        {
            selectView.visible = false
        }
    }

    //========================================================================
    Dialog {
        id: stateDialog
        title: "Please choose state file"
        standardButtons: Dialog.Ok | Dialog.Cancel
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        width: parent.width * 0.8
        height: parent.height * 0.8
        focus: true

        ListView {
            anchors.fill: parent
            id: stateListView

            focus: true
            keyNavigationEnabled: true

            model: ListModel {}

            highlightMoveDuration: 100
            highlightMoveVelocity: -1
            highlight: Rectangle {
                color: "lightsteelblue"
                radius: 5
            }

            Keys.onPressed: {
                if (event.key === Qt.Key_Enter || event.key === Qt.Key_Return)
                    stateDialog.accept()
            }

            delegate: ItemDelegate {
                focus: true
                text: "№" + (index+1).toString() + " " + stateFileName
                onClicked: stateListView.currentIndex = index
            }
        }

        onAccepted: applicationData.loadStateFile(stateListView.currentIndex)
        onRejected: console.log("State select canceled")
    }

    function selectStateFile(iFileList)
    {
        stateListView.model.clear()
        for(var i in iFileList)
        {
            var vl_file = iFileList[i]
            stateListView.model.append({"stateFileName": vl_file});
        }
        stateListView.currentIndex = 0

        stateDialog.open()
    }

    //========================================================================


    Component.onCompleted: {
        console.log("Layer Completed! ["+blockCaption+"]")

        applicationData.setRoot(this)
    }

    Component.onDestruction: {
        console.log("Layer Destruction! ["+blockCaption+"]")
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
