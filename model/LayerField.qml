import QtQuick.Controls 2.13
import QtQuick 2.11
import QtQuick.Shapes 1.11
import QtQuick.Layouts 1.11

Item {
    property string blockCaption: applicationData.textCaption
    property font logTextFont: applicationData.getLogFont()
    anchors.fill: parent

    ListModel {
        id: innerList
    }

    SplitView {
        anchors.fill: parent
        orientation: Qt.Horizontal

        handle: Rectangle {
             implicitWidth: 4
             implicitHeight: 4

             color: SplitHandle.pressed ? parent.palette.mid
                 : (SplitHandle.hovered ? parent.palette.light : parent.palette.midlight)

         }

        ItemTextView {
            SplitView.preferredWidth: parent.width*0.2
            textFont: logTextFont
            contentText: applicationData.textInput
        }

        StackLayout {
            id: swipeViewInner
            SplitView.preferredWidth: parent.width*0.45

            Page {
                id: viewInnerTab

                StackLayout {
                    id: swipeView
                    anchors.fill: parent
                    currentIndex: comboBoxInner.currentIndex

                    Repeater {
                        model: innerList
                        delegate: ItemTextView {
                            width: parent.width*0.45
                            height: parent.height
                            textFont: logTextFont
                            contentText: content
                        }
                    }
                }

                footer: ComboBox {
                    id: comboBoxInner
                    textRole: "text"
                    //displayText: currentText
                    model: innerList
                }
            }
        }

        ItemTextView {
            width: parent.width*0.35
            height: parent.height
            textFont: logTextFont
            contentText: applicationData.textOutput
        }
    }

    Component.onCompleted: {
        console.log("Layer Completed! ["+blockCaption+"]")

        applicationData.setRoot(this)

        swipeViewInner.visible = (innerList.count > 0)
    }

    Component.onDestruction: {
        console.log("Layer Destruction! ["+blockCaption+"]")
    }

    function setRefreshInner(iInnerStatusMap)
    {
        //console.log("inner")
        innerList.clear()
        for(var key in iInnerStatusMap){
            var value = iInnerStatusMap[key]
            innerList.append(
            {
               text: key,
               content: value
            })
            // console.log(key)
        }
        if(innerList.count > 0)
            comboBoxInner.currentIndex = 0
        swipeViewInner.visible = (innerList.count > 0)
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
