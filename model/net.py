import os.path as op
import typing
import logging
import unittest
import xml.etree.ElementTree as xmlET

from model.item import CBlock
from model.environment import CEnvironment
from model.layer import CLayer
from model.layer_field import CLayerField
from model.connection import CConnection


# ==============================
class CNet:
    IsLog: bool
    BlockMap: typing.Mapping[int, CBlock]
    LayerMas: typing.List[CLayer]
    EnvMas: typing.List[CEnvironment]
    ConnectionMas: typing.List[CConnection]

    def __init__(
            self,
            i_is_log: bool,
            i_block_map: typing.Mapping[int, CBlock],
            i_connection_mas: typing.List[CConnection],
            i_env_mas: typing.List[CEnvironment],
            i_layer_mas: typing.List[CLayer]):

        self.IsLog = i_is_log
        self.BlockMap = i_block_map
        self.ConnectionMas = i_connection_mas
        self.EnvMas = i_env_mas
        self.LayerMas = i_layer_mas
        self._AutoBlockID = 1
        v_keys = self.BlockMap.keys()
        while True:
            c_block_id = f"BlockAuto{self._AutoBlockID}"
            if c_block_id not in v_keys:
                self._NextID = c_block_id
                break
            self._AutoBlockID += 1
        self.CurrentTime = 0
    
    # ==============================    
    @classmethod
    def create_from_xml(cls, i_xml_wn: xmlET.Element, i_dir):
        v_version = i_xml_wn.attrib['Version']
        c_support_version_list = ['1.0', '2.0']
        if v_version not in c_support_version_list:
            raise Exception('Bad version')

        v_is_log = i_xml_wn.attrib.get('IsLog', 'true').lower() in ['true', '1']

        v_block_map = {}
        v_connection_mas = []
        v_env_mas = []
        v_layer_mas = []

        v_blocks = i_xml_wn.find('Blocks')
        if v_blocks:
            v_environments = v_blocks.find('Environments')
            if v_environments:
                v_environment_list = v_environments.findall('Environment')
            else:
                v_environment_list = v_blocks.findall('Environment')
            v_layers = v_blocks.find('Layers')
            if v_layers:
                v_layer_list = v_layers.findall('Layer')
                v_layer_field_list = v_layers.findall('LayerField')
            else:
                v_layer_list = v_blocks.findall('Layer')
                v_layer_field_list = v_blocks.findall('LayerField')

            v_connections = i_xml_wn.find('Connections')
            v_connection_list = v_connections.findall('Connection')

            for s in v_environment_list:
                v_environment = CEnvironment.create_from_xml(s, i_dir, v_is_log)
                v_block_map[v_environment.BlockId] = v_environment
                v_env_mas.append(v_environment)

            for s in v_layer_list:
                v_layer = CLayer.create_from_xml(s, i_dir, v_is_log)
                v_block_map[v_layer.BlockId] = v_layer
                v_layer_mas.append(v_layer)

            for s in v_layer_field_list:
                v_layer_field = CLayerField.create_from_xml(s, i_dir, v_is_log)
                v_block_map[v_layer_field.BlockId] = v_layer_field
                v_layer_mas.append(v_layer_field)
        else:
            v_block_connection_list = i_xml_wn.findall('*')

            for s in v_block_connection_list:
                if s.tag == 'Environment':
                    v_block = CEnvironment.create_from_xml(s, i_dir, v_is_log)
                    v_env_mas.append(v_block)
                elif s.tag == 'Layer':
                    v_block = CLayer.create_from_xml(s, i_dir, v_is_log)
                    v_layer_mas.append(v_block)
                elif s.tag == 'LayerField':
                    v_block = CLayerField.create_from_xml(s, i_dir, v_is_log)
                    v_layer_mas.append(v_block)
                else:
                    continue

                v_block_map[v_block.BlockId] = v_block

            v_connection_list = i_xml_wn.findall('Connection')

        for s in v_connection_list:
            v_connection = CConnection.create_from_xml(
                s,
                lambda i_block_id: v_block_map[i_block_id]
            )
            v_connection_mas.append(v_connection)

        return cls(v_is_log, v_block_map, v_connection_mas, v_env_mas, v_layer_mas)
   
    # ==============================    
    @classmethod
    def create_from_file(cls, i_file):
        v_dir = op.dirname(i_file)
        v_xml_tree = xmlET.parse(i_file)
        v_xml_wn = v_xml_tree.getroot()
        return cls.create_from_xml(v_xml_wn, v_dir)
        
    # ==============================    
    def do_tick(self):
        self.CurrentTime = self.CurrentTime + 1

        for _, itItem in self.BlockMap.items():
            try:
                itItem.do_tick()
            except Exception as e:
                logging.debug(f'---------------------- !!! Net.do_tick.Block[{_}]')
                raise e

        for itItem in self.ConnectionMas:
            try:
                itItem.do_transmit()
            except Exception as e:
                logging.debug(f'---------------------- !!! Net.do_tick.Connection[{itItem.Caption}]')
                raise e

    # ==============================
    def get_components(self):
        v_main_env = self.EnvMas[0] if len(self.EnvMas) == 1 else None
        v_main_layer = self.LayerMas[0] if len(self.LayerMas) == 1 else None

        v_components = []
        for it in self.BlockMap.values():
            if it != v_main_env and it != v_main_layer:
                v_component = it.get_component()
                if v_component:
                    v_components.append(v_component)

        return (
            v_main_env.get_component() if v_main_env else None,
            v_main_layer.get_component() if v_main_layer else None,
            v_components
        )


# ==============================
class TestNet(unittest.TestCase):
    def test(self):
        logging.basicConfig(level=logging.DEBUG, format='%(message)s')

        v_file = op.abspath('../!data/Env3_SignPatternToAction/Test.work_net')

        v_net = CNet.create_from_file(v_file)
        for _ in range(30):
            v_net.do_tick()
