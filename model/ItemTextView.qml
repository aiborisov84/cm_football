import QtQuick.Controls
import QtQuick
import QtQuick.Shapes
import QtQuick.Layouts


Page {
    property string contentText
    property int historyLength: 5
    property font textFont

    ListModel {
        id: historyList
    }

    onContentTextChanged: {
        //console.info(contentText)
        cmptTxt.text = contentText
        historyList.insert(0, {text: contentText})
        if (historyLength < historyList.count)
            historyList.remove(historyList.count-1)
    }

    onTextFontChanged: {
        cmptTxt.font = textFont
        historyLabel.font = textFont
        historyItem.font = textFont
    }

    header: ToolBar {
        RowLayout {
            anchors.fill: parent
            Label {
                id: historyLabel
                leftPadding: 5
                height: historyItem.height
                verticalAlignment: Qt.AlignVCenter
                text: "Hist."
            }
            
            SpinBox {
                id: historyItem
                value: 0
                from: 0
                to: historyLength-1

                onValueChanged: {
                    if(value < historyList.count)
                        cmptTxt.text = historyList.get(value).text
                    else
                        cmptTxt.text = ""
                }
            }
            Item {Layout.fillWidth: true}
        }
    }

    Flickable {
        property alias textFont: cmptTxt.font

        property real lastY: 0
        property bool userChage: false

        anchors.fill: parent

        id: flicArea
        clip: true
        TextArea.flickable: TextArea {
            id: cmptTxt
            readOnly: true
            //selectByMouse: true
            textFormat: TextEdit.PlainText
            wrapMode: TextEdit.NoWrap
        }

        Keys.onUpPressed: {userChage = true; scrollBarV.decrease(); }
        Keys.onDownPressed: {userChage = true; scrollBarV.increase();}

        ScrollBar.vertical: ScrollBar {id: scrollBarV; active: true}
        ScrollBar.horizontal: ScrollBar {active: true}

        onContentYChanged: {
            if(!userChage)
            {
                if(contentY!=lastY)
                    contentY=lastY
            }
            else
            {
                lastY = contentY
                userChage = false
            }
        }

        onMovementStarted: {
            userChage = true
        }

        onMovementEnded: {
            userChage = false
            lastY = contentY
        }
    }
}
