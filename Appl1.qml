import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Window
import QtQml
import QtQuick.Dialogs
// import QtQuick.Controls.Material
 
ApplicationWindow {
    id: fmMain
    visible: true
    width: 1200
    height: 800//1920
    title: qsTr("ai Net")
    property font textFont: pyconnect.getLogFont()
    property Item envItem: null
    property Item layerItem: null
    
    ListModel {
        id: viewList
    }

    ListModel {
        id: layerList
    }

    Component {
        id: viewDelegateTab

        Page {
            title: index+1
            Flickable {
                id: flick
                property int lastY: 0
                property bool userChage: false
                anchors.fill: parent
                clip: true
                TextArea.flickable: TextArea {
                    id: messageArea
                    font: textFont
                    readOnly: true
                    //selectByMouse: true
                    textFormat: TextEdit.PlainText
                    wrapMode: TextEdit.NoWrap
                    text: content
                }
                ScrollBar.vertical: ScrollBar {}
                ScrollBar.horizontal: ScrollBar {}
                onContentYChanged: {
                    if(!userChage)
                        if(contentY!=lastY)
                            contentY=lastY
                }
                onMovementStarted: {
                    userChage = true
                }
                onMovementEnded: {
                    userChage = false
                    lastY = contentY
                }
            }
        }
    }

    Component {
        id: viewLayerDelegateTab

        Page {
            title: index+1
            property Item blockItem: null
        }
    }

    Page {
        anchors.fill: parent

        StackLayout {
            id: swipeViewType
            anchors.fill: parent

            Page {
                id: viewLogTab

                StackLayout {
                    id: swipeView
                    anchors.fill: parent
                    currentIndex: tabBar.currentIndex

                    Repeater {
                        model: viewList
                        delegate: viewDelegateTab
                    }
                }

                footer: Page {
                    RowLayout {
                        anchors.fill: parent

                        TabBar {
                            id: tabBar
                            position: TabBar.Footer

                            Repeater {
                                model: viewList
                                TabButton {
                                    text: item_title
                                }
                            }
                        }
                        Item {Layout.fillWidth: true}
                    }
                }
            }

            SplitView {
                id: splitEnvLayer
                orientation: Qt.Horizontal

                Page {
                    SplitView.preferredWidth: parent.width*0.3
                    id: viewEnvTab
                }

                Page {
                    id: viewLayerTab
                }
            }

            Page {
                id: viewLayerMasTab

                StackLayout {
                    id: swipeLayerMasView
                    anchors.fill: parent
                    currentIndex: tabBarLayerMas.currentIndex

                    Repeater {
                        id: repeatLayerMasView
                        model: layerList
                        delegate: viewLayerDelegateTab
                    }
                }

                footer: Page {
                    RowLayout {
                        anchors.fill: parent

                        TabBar {
                            id: tabBarLayerMas
                            position: TabBar.Footer

                            Repeater {
                                model: layerList
                                TabButton {
                                    text: item_title
                                    width: implicitWidth
                                }
                            }
                        }
                        Item {Layout.fillWidth: true}
                    }
                }
            }
        }
    }

    footer: Page {
        id: tb1

        Flow {
            anchors.fill: parent
            spacing: 5

            TabBar {
                position: TabBar.Footer
                TabButton {
                    id: btBlockMasTab
                    text: "BlockMas[b]"
                    onClicked: swipeViewType.currentIndex = 2
                    width: implicitWidth
                }
                TabButton {
                    id: btHistoryTab
                    text: "LogHistory[1-5]"
                    onClicked: swipeViewType.currentIndex = 0
                    width: implicitWidth
                }
                TabButton {
                    id: btEnvLayerTab
                    text: "Env[e] + Layer[l]"
                    onClicked: swipeViewType.currentIndex = 1
                    width: implicitWidth
                }
            }
        
            //Item {Layout.fillWidth: true}
            Button {
                text: "select[s]"
                onClicked: selectModel()
            }
            Button {
                text: "exit"
                onClicked: {
                    Qt.quit()
                }
            }
            Button {
                text: "restart[0]"
                onClicked: restart()
            }
            Row {
                spacing: 2
                Button {
                    text: "step[9]"
                    onClicked: step()
                }
                SpinBox {
                    id: stepCount
                    value: 1
                    from: 1
                }
            }
        }
    }
    
    function step() {
        for(var i=0; i<viewList.count-1; i++)
        {
            viewList.setProperty(i, "content", 
                viewList.get(i+1).content
            )
        }
        viewList.setProperty(viewList.count-1, "content", pyconnect.step(stepCount.value))
    }

    function restartIdx(idx) {
        pyconnect.restart(idx)
        for(var i=0; i<viewList.count; i++)
        {
            viewList.setProperty(i, "content", viewList.get(i).item_title)
        }
    }
    
    function restart() {
        restartIdx(-1)
    }

    function setMainEnv(iComponent)
    {
        if(envItem)
        {
            console.log("== Appl Prepare main environment free")
            viewEnvTab.visible = false
            //envItem.destroy()
            envItem = null
            console.log("== Appl Prepare main environment free Done")
        }

        if(iComponent)
        {
            console.log("== Appl Ready main environment insert")
            envItem = iComponent.createObject(viewEnvTab, {"x": 0, "y": 0});
            viewEnvTab.visible = true
        }
        else
        {
            viewEnvTab.visible = false
        }

        var vMainLayerEnv = layerItem || envItem
        btEnvLayerTab.enabled = vMainLayerEnv

        if(vMainLayerEnv)
            selectImportant()
    }

    function setMainLayer(iComponent)
    {
        if(layerItem)
        {
            console.log("== Appl Prepare main layer free")
            viewLayerTab.visible = false
            //layerItem.destroy()
            layerItem = null
        }

        if(iComponent)
        {
            console.log("== App Ready main layer insert")
            layerItem = iComponent.createObject(viewLayerTab, {"x": 0, "y": 0});
            viewLayerTab.visible = true
        }
        else
        {
            viewLayerTab.visible = false
        }

        var vMainLayerEnv = layerItem || envItem
        btEnvLayerTab.enabled = vMainLayerEnv

        if(vMainLayerEnv)
            selectImportant()
    }

    function setComponentMas(iComponentMas)
    {
        for(var j = 0; j < layerList.count; j++)
        {
            var vlCleanPage = repeatLayerMasView.itemAt(j)
            console.log("== App Prepare block free: "+vlCleanPage.blockItem.blockCaption)
            //vlCleanPage.blockItem.destroy()
            vlCleanPage.blockItem = null
        }
        btBlockMasTab.text = "BlockMas[b]"
        layerList.clear()
        for(var i in iComponentMas)
        {
            var vlComponent = iComponentMas[i]
            layerList.append(
            {
                "content": (i).toString(),
                "item_title": (i).toString()
            })

            var vlPage = repeatLayerMasView.itemAt(i)
            vlPage.blockItem = vlComponent.createObject(vlPage, {"x": 0, "y": 0})
            layerList.setProperty(i, "item_title", vlPage.blockItem.blockCaption)
            // console.log("Appl insert block: " + vlPage.blockItem.blockCaption)
        }
        btBlockMasTab.text = "BlockMas" + (layerList.count).toString()+"[b]"

        var vBlockMasShow = (0 < layerList.count)
        btBlockMasTab.enabled = vBlockMasShow

        if(vBlockMasShow)
            selectBlockMas()
    }

    function addApplException(iExceptionStr)
    {
        messageDialog.text = iExceptionStr
        messageDialog.visible = true
    }

    function selectModel()
    {
        dlgSelectModel.visible = true
    }

    Component.onCompleted: {
        console.log("== Appl Completed!")
        if(!pyconnect.isAndroid())
        {
            fmMain.height = Screen.desktopAvailableHeight * 0.8;
            fmMain.width = Screen.desktopAvailableWidth * 0.8;
            fmMain.y = Screen.desktopAvailableHeight * 0.1;
            fmMain.x = Screen.desktopAvailableWidth * 0.1;
        }

        for(var i=5; i>0; i--)
        {
            viewList.append(
            {
                "content": (i).toString(),
                "item_title": (i).toString()
            })
        }
        tabBar.currentIndex = viewList.count-1

        btEnvLayerTab.enabled = false

        pyconnect.setRoot(this)
    }

    Component.onDestruction: {
        console.log("== Appl Destruction!")
        setMainEnv(null)
        setMainLayer(null)
        setComponentMas(null)

        pyconnect.setRoot(null)
    }

    function selectHistory(iIndex)
    {
        tabBar.currentIndex = viewList.count-iIndex
        btHistoryTab.clicked()
        btHistoryTab.checked = true
    }

    function selectEnvLayer()
    {
        if(!btEnvLayerTab.enabled)
            return false

        btEnvLayerTab.clicked()
        btEnvLayerTab.checked = true
        return true
    }

    function selectBlockMas()
    {
        if(!btBlockMasTab.enabled)
            return false

        btBlockMasTab.clicked()
        btBlockMasTab.checked = true
        return true
    }

    function selectImportant()
    {
        if(selectEnvLayer())
            return
        selectHistory(1)
    }

    Shortcut {
        sequence: "1"
        onActivated: selectHistory(1)
    }
    Shortcut {
        sequence: "2"
        onActivated: selectHistory(2)
    }
    Shortcut {
        sequence: "3"
        onActivated: selectHistory(3)
    }
    Shortcut {
        sequence: "4"
        onActivated: selectHistory(4)
    }
    Shortcut {
        sequence: "5"
        onActivated: selectHistory(5)
    }
    Shortcut {
        sequence: "e"
        onActivated: selectEnvLayer()
    }
    Shortcut {
        sequence: "l"
        onActivated: selectEnvLayer()
    }
    Shortcut {
        sequence: "b"
        onActivated: selectBlockMas()
    }
    Shortcut {
        sequence: "9"
        onActivated: step()
    }
    Shortcut {
        sequence: "0"
        onActivated: restart()
    }
    Shortcut {
        sequence: "s"
        onActivated: selectModel()
    }

    MessageDialog {
        id: messageDialog
        title: "Exception"
        text: ""
        onAccepted: {
            visible = false
        }
    }
    
    Dialog {
        id: dlgSelectModel

        title: "Choose model"
        standardButtons: Dialog.Ok

        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        width: parent.width * 0.8
        height: parent.height * 0.8

        focus: true
        visible: false
    
        onAccepted: {
            restartIdx(listView.currentIndex)
        }

        Shortcut {
            sequence: "1"
            enabled: dlgSelectModel.visible && (0 < listView.count)
            context: Qt.ApplicationShortcut
            onActivated: {
                listView.currentIndex = 0
                dlgSelectModel.accept()
            }
        }

        Shortcut {
            sequence: "2"
            enabled: dlgSelectModel.visible && (1 < listView.count)
            context: Qt.ApplicationShortcut
            onActivated: {
                listView.currentIndex = 1
                dlgSelectModel.accept()
            }
        }

        Shortcut {
            sequence: "3"
            enabled: dlgSelectModel.visible && (2 < listView.count)
            context: Qt.ApplicationShortcut
            onActivated: {
                listView.currentIndex = 2
                dlgSelectModel.accept()
            }
        }

        Shortcut {
            sequence: "4"
            enabled: dlgSelectModel.visible && (3 < listView.count)
            context: Qt.ApplicationShortcut
            onActivated: {
                listView.currentIndex = 3
                dlgSelectModel.accept()
            }
        }

        Shortcut {
            sequence: "5"
            enabled: dlgSelectModel.visible && (4 < listView.count)
            context: Qt.ApplicationShortcut
            onActivated: {
                listView.currentIndex = 4
                dlgSelectModel.accept()
            }
        }

        Shortcut {
            sequence: "6"
            enabled: dlgSelectModel.visible && (5 < listView.count)
            context: Qt.ApplicationShortcut
            onActivated: {
                listView.currentIndex = 5
                dlgSelectModel.accept()
            }
        }

        Shortcut {
            sequence: "7"
            enabled: dlgSelectModel.visible && (6 < listView.count)
            context: Qt.ApplicationShortcut
            onActivated: {
                listView.currentIndex = 6
                dlgSelectModel.accept()
            }
        }

        ListView {
            anchors.fill: parent
            id: listView

            focus: true
            keyNavigationEnabled: true

            model: ListModel {
                id: cbItems
            }
            clip: true
            highlight: Rectangle {
                color: "lightsteelblue"
                radius: 5
            }
            highlightMoveDuration: 100
            highlightMoveVelocity: -1
            delegate: ItemDelegate {
                text: "№" + (index+1).toString() + " " + modelName
                //highlighted: ListView.isCurrentItem
                onClicked: {
                    listView.currentIndex = index
                    dlgSelectModel.accept()
                }
            }

            Keys.onPressed: {
                if (event.key === Qt.Key_Enter || event.key === Qt.Key_Return)
                    dlgSelectModel.accept()
            }

            ScrollBar.horizontal: ScrollBar {
                active: true
            }
        }

        Component.onCompleted: {
            if(!pyconnect.isAndroid())
            {
                dlgSelectModel.width = fmMain.width * 0.8
                dlgSelectModel.height = fmMain.height * 0.8
            }

            var vCount = pyconnect.modelCount();
            for(var i=0; i<vCount; i++) 
            {
                var vItem = pyconnect.modelStr(i);
                listView.model.append({"modelName": vItem});
            }   
            listView.currentIndex = pyconnect.modelIdx()
        }
    }
}
